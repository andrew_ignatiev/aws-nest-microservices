/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const slsw = require('serverless-webpack');
const TerserPlugin = require('terser-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = (async () => {
  const isLocal = slsw.lib.webpack.isLocal;

  return {
    mode: isLocal ? 'development' : 'production',
    entry: slsw.lib.entries,
    devtool: isLocal ? 'eval-cheap-module-source-map' : 'source-map',
    optimization: {
      minimize: true,
      minimizer: [
        new TerserPlugin({
          terserOptions: {
            keep_classnames: true,
            compress: {
              passes: 2,
            },
          },
        }),
      ],
      splitChunks: {
        chunks: 'all',
        cacheGroups: {
          default: false,
          defaultVendors: false,
          lambda: {
            // https://docs.aws.amazon.com/lambda/latest/dg/gettingstarted-limits.html
            minSize: 300000,
            maxSize: 300000,
            name: (_, chunks) => chunks[0].runtime + '-chunk',
          },
        },
      },
    },
    resolve: {
      extensions: ['.ts', '.js'],
      cacheWithContext: false,
      plugins: [
        new TsconfigPathsPlugin({
          configFile: 'tsconfig.json',
        }),
      ],
    },
    output: {
      libraryTarget: 'commonjs2',
      path: path.join(__dirname, '../../.webpack'),
      filename: '[name].js',
    },
    target: 'node',
    externals: [nodeExternals()],
    module: {
      rules: [
        // all files with a `.ts` extension will be handled by `ts-loader`
        {
          test: /\.ts$/,
          loader: 'ts-loader',
          exclude: /node_modules/,
        },
      ],
    },
  };
})();
