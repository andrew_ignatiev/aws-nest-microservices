import { INestApplication, Injectable } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { DocumentBuilderOptions } from '@app-lib/serverless/interfaces';
import { PromiseResult } from 'aws-sdk/lib/request';
import * as swaggerUi from 'swagger-ui-express';
import * as aws from 'aws-sdk';

@Injectable()
export class ServerlessService {
  private ssm: aws.SSM;

  getSSM() {
    if (this.ssm == null) {
      this.ssm = new aws.SSM({ region: process.env.AWS_REGION ?? '' });
    }

    return this.ssm;
  }

  isServerless() {
    return process.env.SLS_STAGE != null;
  }

  getStage() {
    if (this.isServerless()) {
      return process.env.SLS_STAGE;
    }

    return null;
  }

  createAppDocument(
    app: INestApplication,
    docPath: string,
    options: DocumentBuilder,
  ): INestApplication;
  createAppDocument(
    app: INestApplication,
    docPath: string,
    options: DocumentBuilderOptions,
  ): INestApplication;
  createAppDocument(
    app: INestApplication,
    docPath: string,
    options: any,
  ): INestApplication {
    if (this.getStage() === 'production') {
      return app;
    }

    let config: ReturnType<InstanceType<typeof DocumentBuilder>['build']>;

    if (options instanceof DocumentBuilder) {
      config = options.build();
    } else {
      const builder = new DocumentBuilder()
        .setTitle(options.title)
        .setDescription(options.description)
        .setVersion(options.version);

      if (options.bearerAuth?.options != null) {
        builder.addBearerAuth(
          options.bearerAuth.options,
          options.bearerAuth.name,
        );
      }

      config = builder.build();
      config.servers.push({
        url: `/${this.getStage() ?? ''}`,
        description: 'API Server',
      });
    }

    app
      .getHttpAdapter()
      .getInstance()
      .use(
        docPath,
        swaggerUi.serveWithOptions({ redirect: false }),
        swaggerUi.setup(SwaggerModule.createDocument(app, config)),
      );

    return app;
  }

  async getParameterValueFromSMM(
    name: string,
    withDecryption: boolean,
    defaultValue?: string,
  ): Promise<string> {
    let value;

    if (this.getStage() != null) {
      value = (await this.getParameterFromSMM(name, withDecryption)).Parameter
        ?.Value;
    }

    return value ?? defaultValue ?? '';
  }

  async getParameterFromSMM(
    name: string,
    withDecryption: boolean,
  ): Promise<PromiseResult<aws.SSM.GetParameterResult, aws.AWSError>> {
    const options = {
      Name: name,
      WithDecryption: withDecryption,
    };

    return this.getSSM().getParameter(options).promise();
  }
}
