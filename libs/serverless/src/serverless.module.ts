import { Module } from '@nestjs/common';
import { ServerlessService } from './serverless.service';

@Module({
  providers: [ServerlessService],
  exports: [ServerlessService],
})
export class ServerlessModule {}
