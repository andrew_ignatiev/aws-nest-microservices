import { Test, TestingModule } from '@nestjs/testing';
import { ServerlessService } from './serverless.service';

describe('ServerlessService', () => {
  let service: ServerlessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ServerlessService],
    }).compile();

    service = module.get<ServerlessService>(ServerlessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
