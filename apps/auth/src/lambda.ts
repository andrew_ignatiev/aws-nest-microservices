import { createApp } from './app.factory';
import * as serverless from 'serverless-http';
import {
  Context,
  Callback,
  Handler,
  APIGatewayRequestAuthorizerEvent,
} from 'aws-lambda';
import { HttpStatus } from '@nestjs/common';

let cachedHandler: Handler;

async function bootstrap(): Promise<Handler> {
  if (!cachedHandler) {
    const nestApp = await createApp();
    const httpApp = nestApp.getHttpAdapter().getInstance();

    nestApp.enableCors();
    await nestApp.init();

    cachedHandler = serverless(httpApp);
  }

  return cachedHandler;
}

export async function handler(
  event: APIGatewayRequestAuthorizerEvent,
  context: Context,
  callback: Callback,
): Promise<void> {
  delete event.headers['Content-Length'];
  delete event.multiValueHeaders['Content-Length'];

  const res = await (await bootstrap())(event, context, callback);
  const body = JSON.parse(res.body);

  if (res.statusCode !== HttpStatus.OK) {
    return callback(body?.message);
  }

  return callback(null, body);
}
