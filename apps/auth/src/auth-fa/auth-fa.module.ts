import { Module } from '@nestjs/common';
import { AuthFaService } from './auth-fa.service';
import { AuthFaSettings } from './interfaces';
import { ASYNC_AUTH_FA_SETTINGS } from './constants';

const authFaOptionsFactory = {
  provide: ASYNC_AUTH_FA_SETTINGS,
  useFactory: async (authFaService: AuthFaService): Promise<AuthFaSettings> => {
    return { publicKey: (await authFaService.getPublicKey()) as string };
  },
  inject: [AuthFaService],
};

@Module({
  providers: [AuthFaService, authFaOptionsFactory],
  exports: [AuthFaService, authFaOptionsFactory],
})
export class AuthFaModule {}
