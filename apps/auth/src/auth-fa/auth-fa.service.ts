import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

const key = '';

@Injectable()
export class AuthFaService {
  private publicKey: Promise<string>;

  constructor(private configService: ConfigService) {}

  getPublicKey(): Promise<string> {
    if (this.publicKey == null) {
      this.publicKey = new Promise((resolve) => {
        setTimeout(() => {
          resolve(key);
        }, 500);
      });
    }

    return this.publicKey;
  }
}
