export interface AuthFaSettings {
  /**
   * FusionAuth JWT Public Key
   * @see https://fusionauth.io/docs/v1/tech/apis/jwt/
   */
  publicKey: string | Buffer;
}
