import { Context, APIGatewayRequestAuthorizerEvent } from 'aws-lambda';
import { IncomingMessage } from 'http';

export interface ServerlessRequest extends IncomingMessage {
  requestContext: Record<string, unknown>;
  apiGateway: {
    event: APIGatewayRequestAuthorizerEvent;
    context: Context;
  };
}
