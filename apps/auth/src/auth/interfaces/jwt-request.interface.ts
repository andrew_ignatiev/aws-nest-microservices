import { JwtUser } from './jwt-user.interface';
import { ServerlessRequest } from './serverless-request.interface';

export interface JwtRequest extends ServerlessRequest {
  user: JwtUser;
}
