export interface JwtUserPayload {
  sub: number | string;
  roles: Array<string>;
  storeId: number | string;
}

export interface JwtUser extends Pick<JwtUserPayload, 'roles' | 'storeId'> {
  userId: number | string;
}
