import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { JwtModule, JwtModuleOptions } from '@nestjs/jwt';
import { AuthFaModule } from '../auth-fa/auth-fa.module';
import { AuthStrategy } from './auth.strategy';
import { AuthFaSettings } from '../auth-fa/interfaces';
import { ASYNC_AUTH_FA_SETTINGS } from '../auth-fa/constants';

@Module({
  imports: [
    AuthFaModule,
    JwtModule.registerAsync({
      imports: [AuthFaModule],
      useFactory: async (
        settings: AuthFaSettings,
      ): Promise<JwtModuleOptions> => ({ publicKey: settings.publicKey }),
      inject: [ASYNC_AUTH_FA_SETTINGS],
    }),
  ],
  providers: [AuthService, AuthStrategy],
  controllers: [AuthController],
})
export class AuthModule {}
