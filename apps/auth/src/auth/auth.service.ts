import { Injectable } from '@nestjs/common';
import { JwtRequest } from './interfaces';
import { ApiOptions, AuthPolicy } from 'aws-api-auth-policy';
import * as fetch from 'node-fetch';

@Injectable()
export class AuthService {
  /**
   * AWS API Gateway Lambda Authorizer policy creator
   * @param request
   */
  async createAwsAuthPolicy(request: JwtRequest) {
    const user = request.user;
    const chunks = request.apiGateway.event.methodArn.split(':');
    const awsAccountId = chunks[4];
    const apiGatewayArn = chunks[5].split('/');

    const apiOptions: ApiOptions = {
      stage: apiGatewayArn[1],
      region: chunks[3],
      restApiId: apiGatewayArn[0],
    };

    const authPolicy = new AuthPolicy(
      user.userId?.toString(),
      awsAccountId,
      apiOptions,
    );

    if (user.userId == null) {
      authPolicy.denyAllMethods();
    } else {
      authPolicy.allowAllMethods();
    }

    const policy = authPolicy.build();

    policy.context = {
      userId: user.userId,
      storeId: user.storeId,
      roles: user.roles?.join(',') ?? '',
    };

    return policy;
  }
}
