import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, Inject } from '@nestjs/common';
import { AuthFaSettings } from '../auth-fa/interfaces';
import { ASYNC_AUTH_FA_SETTINGS } from '../auth-fa/constants';
import { JwtUserPayload, JwtUser } from './interfaces';

@Injectable()
export class AuthStrategy extends PassportStrategy(Strategy) {
  constructor(
    @Inject(ASYNC_AUTH_FA_SETTINGS)
    private readonly settings: AuthFaSettings,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: settings.publicKey,
    });
  }

  async validate(payload: JwtUserPayload): Promise<JwtUser> {
    return {
      userId: payload.sub,
      roles: payload.roles,
      storeId: payload.storeId,
    };
  }
}
