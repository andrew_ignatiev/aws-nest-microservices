import { Controller, All, Request, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';
import { JwtRequest } from './interfaces';

@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(AuthGuard)
  @All('*')
  auth(@Request() request: JwtRequest) {
    return this.authService.createAwsAuthPolicy(request);
  }
}
