import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { FacebookConfig } from './config.interface';

/**
 * Service dealing with facebook configuration based operations.
 *
 * @class
 */
@Injectable()
export class FacebookConfigService {
  constructor(private configService: ConfigService<FacebookConfig>) {}

  get adsPixelId(): FacebookConfig['facebook.adsPixelId'] {
    return this.configService.get('facebook.adsPixelId');
  }

  get adsPixelAccessToken(): FacebookConfig['facebook.adsPixelAccessToken'] {
    return this.configService.get('facebook.adsPixelAccessToken');
  }
}
