import { registerAs } from '@nestjs/config';

export default registerAs('facebook', async () => {
  return {
    adsPixelId: process.env['ADS_PIXEL_ID'],
    adsPixelAccessToken: process.env['ADS_PIXEL_ACCESS_TOKEN'],
  };
});
