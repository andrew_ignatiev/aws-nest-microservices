export interface FacebookConfig {
  'facebook.adsPixelId': string;
  'facebook.adsPixelAccessToken': string;
}
