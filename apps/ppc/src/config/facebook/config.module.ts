import config from './config';
import { Module } from '@nestjs/common';
import { FacebookConfigService } from './config.service';
import { ConfigModule, ConfigService } from '@nestjs/config';

/**
 * Import and provide facebook configuration related classes.
 *
 * @module
 */
@Module({
  imports: [ConfigModule.forRoot({ load: [config] })],
  providers: [ConfigService, FacebookConfigService],
  exports: [ConfigService, FacebookConfigService],
})
export class FacebookConfigModule {}
