import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
} from '@nestjs/common';

@Catch(HttpException)
export class ApiExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();

    const res = exception.getResponse();
    const json = typeof res === 'string' ? { message: res } : res;

    response.status(exception.getStatus()).json({
      ...json,
      statusCode: exception.getStatus(),
      success: false,
      error: undefined,
    });
  }
}
