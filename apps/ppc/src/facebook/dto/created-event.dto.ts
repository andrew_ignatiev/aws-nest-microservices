import { ApiProperty } from '@nestjs/swagger';
import { FacebookError } from '../facebook.error';

export class CreatedEventDto {
  @ApiProperty({
    type: () => [String],
    example: ['EventName'],
  })
  events: string[];

  @ApiProperty({ example: 1 })
  eventsSent: number;

  @ApiProperty({ example: 'ACiJy8cMiVwlmYF3N2iqaSg' })
  fbtraceId: string;

  error?: FacebookError;
}
