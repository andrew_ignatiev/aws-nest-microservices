import { IsInt } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { CreateEventViewContentDto } from './create-event-view-content.dto';

export class CreateEventAddToCartDto extends CreateEventViewContentDto {
  @IsInt()
  @ApiProperty({
    description: 'Product quantity',
  })
  quantity: number;
}
