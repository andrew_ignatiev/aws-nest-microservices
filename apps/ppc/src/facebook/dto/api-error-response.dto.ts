import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { ApiErrorResponseInterface } from '../../interfaces';

export class ApiBadRequestResponseDto implements ApiErrorResponseInterface {
  @ApiProperty({ example: false })
  success: boolean;

  @ApiProperty({
    example: [
      'userId must be an integer number',
      'sourceUrl must be base64 encoded',
      'userAgent must be base64 encoded',
    ],
  })
  message: string[];

  @ApiProperty({ example: 400 })
  statusCode: number;
}

export class ApiInternalServerErrorResponseDto
  implements ApiErrorResponseInterface {
  @ApiProperty({ example: false })
  success: boolean;

  @ApiProperty({ example: 'Error: getaddrinfo ENOTFOUND graph.facebook.com' })
  message: string;

  @ApiProperty({ example: 500 })
  statusCode: number;
}
