import { ApiProperty } from '@nestjs/swagger';
import { ApiSuccessResponseInterface } from '../../interfaces';
import { CreatedEventDto } from './created-event.dto';

export class ApiSuccessResponseDto
  implements ApiSuccessResponseInterface<CreatedEventDto> {
  @ApiProperty({
    type: () => CreatedEventDto,
  })
  data: CreatedEventDto;

  @ApiProperty()
  success: true;
}
