import { Transform } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsEnum, IsInt } from 'class-validator';
import { CurrencyEnum } from '../../enums/currency.enum';
import { CreateEventBaseDto } from './create-event-base.dto';

export class CreateEventViewContentDto extends CreateEventBaseDto {
  @IsNumber()
  @ApiProperty({
    description: 'Product price',
  })
  price: number;

  @Transform(({ value }) => value?.toString()?.toUpperCase())
  @IsEnum(CurrencyEnum)
  @ApiProperty({
    description: 'Product price currency',
  })
  currency: string;

  @IsInt()
  @ApiProperty({
    description: 'Product ID',
  })
  productId: number;
}
