import { ApiProperty } from '@nestjs/swagger';
import { IsInt, Min, IsBase64, IsNotEmpty } from 'class-validator';

export class CreateEventBaseDto {
  @IsInt()
  @Min(1)
  @ApiProperty({
    description: 'User ID',
  })
  userId: number;

  @IsNotEmpty()
  @ApiProperty({
    description: 'Event ID',
  })
  eventId: string;

  @IsNotEmpty()
  @IsBase64()
  @ApiProperty({
    description: 'Base64 encoded referrer url',
  })
  sourceUrl: string;

  @IsNotEmpty()
  @IsBase64()
  @ApiProperty({
    description: 'Base64 encoded user agent',
  })
  userAgent: string;
}
