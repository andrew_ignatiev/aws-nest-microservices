import { ApiProperty } from '@nestjs/swagger';
import { CreateEventBaseDto } from './create-event-base.dto';
import { Transform, Type } from 'class-transformer';
import { IsEnum, IsInt, IsNumber, ValidateNested } from 'class-validator';
import { CurrencyEnum } from '../../enums/currency.enum';

export class TransactionProduct {
  @IsNumber()
  @ApiProperty({
    description: 'Order item price',
  })
  price: number;

  @IsInt()
  @ApiProperty({
    description: 'Order item quantity',
  })
  quantity: number;

  @IsInt()
  @ApiProperty({
    description: 'Order item product ID',
  })
  productId: number;
}

export class CreateEventPurchaseDto extends CreateEventBaseDto {
  @IsInt()
  @ApiProperty({
    description: 'Order ID',
  })
  transactionId: number;

  @Transform(({ value }) => value?.toString()?.toUpperCase())
  @IsEnum(CurrencyEnum)
  @ApiProperty({
    description: 'Order currency',
  })
  transactionCurrency: string;

  @IsNumber()
  @ApiProperty({
    description: 'Order shipping cost',
  })
  transactionShipping: number;

  @IsNumber()
  @ApiProperty({
    description: 'Order tax value',
  })
  transactionTax: number;

  @IsNumber()
  @ApiProperty({
    description: 'Order total',
  })
  transactionTotal: number;

  @Type(() => TransactionProduct)
  @ValidateNested()
  @ApiProperty({
    description: 'Order items',
    type: () => [TransactionProduct],
  })
  transactionProducts: TransactionProduct[];
}
