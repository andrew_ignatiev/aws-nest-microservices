import { CreateEventBaseDto } from './create-event-base.dto';

export class CreateEventCompleteRegistrationDto extends CreateEventBaseDto {}
