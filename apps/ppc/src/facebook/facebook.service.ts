import { Injectable } from '@nestjs/common';
import { FacebookConfigService } from '../config/facebook/config.service';
import * as bizSdk from 'facebook-nodejs-business-sdk';
import {
  CreateEventBaseDto,
  CreateEventViewContentDto,
  CreateEventAddToCartDto,
  CreateEventPageViewDto,
  CreatedEventDto,
  CreateEventCompleteRegistrationDto,
  CreateEventPurchaseDto,
} from './dto';
import { FacebookError } from './facebook.error';

const {
  Content,
  CustomData,
  EventRequest,
  FacebookAdsApi,
  ServerEvent,
  UserData,
} = bizSdk;

@Injectable()
export class FacebookService {
  private api: InstanceType<typeof FacebookAdsApi>;

  constructor(private configService: FacebookConfigService) {
    this.api = FacebookAdsApi.init(this.configService.adsPixelAccessToken);
  }

  /**
   * Setup debug mode in Facebook Ads API
   */
  setDebug(flag: boolean) {
    this.api.setDebug(flag);
  }

  /**
   * Creates PageView the default pixel tracking page visits.
   */
  createEventPageView(eventDto: CreateEventPageViewDto) {
    const serverEvent = this.getServerEvent('PageView', eventDto);

    return this.sendEvents([serverEvent]);
  }

  /**
   * Creates ViewContent event. ViewContent tells you if someone visits a web page's URL,
   * but not what they see or do on that page.
   */
  createEventViewContent(eventDto: CreateEventViewContentDto) {
    const customData = new CustomData()
      .setCurrency(eventDto.currency)
      .setValue(this.getPriceValue(eventDto.price))
      .setContentIds([`${eventDto.productId}`])
      .setContentType('product');

    const serverEvent = this.getServerEvent(
      'ViewContent',
      eventDto,
    ).setCustomData(customData);

    return this.sendEvents([serverEvent]);
  }

  /**
   * Creates CompleteRegistration event when a registration form is completed.
   * Also it creates custom CompleteSignup event for the same purpose.
   */
  createEventCompleteRegistration(
    eventDto: CreateEventCompleteRegistrationDto,
  ) {
    const completeSignupEvent = this.getServerEvent('CompleteSignup', eventDto);
    const completeRegistrationEvent = this.getServerEvent(
      'CompleteRegistration',
      eventDto,
    );

    return this.sendEvents([completeRegistrationEvent, completeSignupEvent]);
  }

  /**
   * Creates AddToCart event when a product is added to the shopping cart.
   */
  createEventAddToCart(eventDto: CreateEventAddToCartDto) {
    const content = new Content()
      .setId(`${eventDto.productId}`)
      .setQuantity(eventDto.quantity);

    const customData = new CustomData()
      .setCurrency(eventDto.currency)
      .setValue(this.getPriceValue(eventDto.price))
      .setContentType('product')
      .setContents([content]);

    const serverEvent = this.getServerEvent(
      'AddToCart',
      eventDto,
    ).setCustomData(customData);

    return this.sendEvents([serverEvent]);
  }

  /**
   * Creates Purchase event when a purchase is made or checkout flow is completed.
   * @param eventDto
   */
  createEventPurchase(eventDto: CreateEventPurchaseDto) {
    const total = [
      eventDto.transactionTotal,
      eventDto.transactionTax,
      eventDto.transactionShipping,
    ]
      .map((price) => parseFloat(`${price}`))
      .reduce((acc, curr, index) => (index === 0 ? curr : acc - curr), 0);

    const index = {};
    const products = eventDto.transactionProducts;

    for (let i = 0; i < products.length; i++) {
      if (products[i].productId in index) {
        index[products[i].productId].quantity += products[i].quantity;
      } else {
        index[products[i].productId] = {
          id: `${products[i].productId}`,
          quantity: products[i].quantity,
        };
      }
    }

    const contents = Object.values(index).map(({ id, quantity }) => {
      return new Content().setId(`${id}`).setQuantity(quantity);
    }) as Array<InstanceType<typeof Content>>;

    const customData = new CustomData()
      .setCurrency(eventDto.transactionCurrency)
      .setValue(this.getPriceValue(total))
      .setContentType('product')
      .setContents(contents);

    const purchaseEvent = this.getServerEvent(
      'Purchase',
      eventDto,
    ).setCustomData(customData);

    return this.sendEvents([purchaseEvent]);
  }

  /**
   * Creates custom HealthCheck event to check health of service.
   * @param eventDto
   */
  createEventHealthCheck(eventDto: CreateEventBaseDto) {
    const serverEvent = this.getServerEvent(
      'HealthCheck',
      eventDto,
    ).setActionSource('other');

    return this.sendEvents([serverEvent]);
  }

  /**
   * Creates Event Request for given events and sends them to Facebook.
   */
  private async sendEvents(events: Array<InstanceType<typeof ServerEvent>>) {
    let response: CreatedEventDto = {
      events: events.map((evt) => evt.event_name),
      eventsSent: 0,
      fbtraceId: '',
    };

    try {
      const eventRequest = new EventRequest(
        this.configService.adsPixelAccessToken,
        this.configService.adsPixelId,
      ).setEvents(events);

      const eventResponse = await eventRequest.execute();

      response = {
        ...response,
        eventsSent: eventResponse.events_received,
        fbtraceId: eventResponse.fbtrace_id,
        error:
          eventResponse.events_received !== events.length
            ? new FacebookError('Events have not been sent')
            : undefined,
      };
    } catch (error) {
      response = {
        ...response,
        error: new FacebookError(error),
      };
    }

    return response;
  }

  /**
   * Returns new Server Event which is shared with corresponding website event.
   */
  private getServerEvent(eventName: string, eventDto: CreateEventBaseDto) {
    const serverEvent = new ServerEvent()
      .setEventName(eventName)
      .setEventId(eventDto.eventId)
      .setEventTime(this.getCurrentTimestamp())
      .setUserData(this.getUserData(eventDto))
      .setActionSource('website');

    if (eventDto.sourceUrl !== '') {
      serverEvent.setEventSourceUrl(this.getSourceUrl(eventDto));
    }

    return serverEvent;
  }

  /**
   * Returns user data to be used in Server Event
   */
  private getUserData(eventDto: CreateEventBaseDto) {
    const userData = new UserData().setExternalId(`${eventDto.userId}`);

    if (eventDto.userAgent !== '') {
      userData.setClientUserAgent(this.getUserAgent(eventDto));
    }

    return userData;
  }

  /**
   * Returns server current timestamp
   */
  private getCurrentTimestamp() {
    return Math.floor(Date.now() / 1000);
  }

  /**
   * Returns Base64 decoded user agent
   */
  private getUserAgent(eventDto: CreateEventBaseDto) {
    return Buffer.from(eventDto.userAgent, 'base64').toString('utf-8');
  }

  /**
   * Returns Base64 decoded source url
   */
  private getSourceUrl(eventDto: CreateEventBaseDto) {
    return Buffer.from(eventDto.sourceUrl, 'base64').toString('utf-8');
  }

  /**
   * Returns formatted float value of given price
   */
  private getPriceValue(price: string | number) {
    return parseFloat(parseFloat(`${price}`).toFixed(2));
  }
}
