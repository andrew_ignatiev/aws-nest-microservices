import { Module } from '@nestjs/common';
import { FacebookService } from './facebook.service';
import { FacebookController } from './facebook.controller';
import { FacebookConfigModule } from '../config/facebook/config.module';
import { FacebookConfigService } from '../config/facebook/config.service';

@Module({
  imports: [FacebookConfigModule],
  controllers: [FacebookController],
  providers: [FacebookConfigService, FacebookService],
  exports: [FacebookService],
})
export class FacebookModule {}
