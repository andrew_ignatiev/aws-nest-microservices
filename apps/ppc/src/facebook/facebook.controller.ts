import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  UseInterceptors,
  UseFilters,
} from '@nestjs/common';
import {
  CreateEventAddToCartDto,
  CreateEventCompleteRegistrationDto,
  CreateEventViewContentDto,
  CreateEventPurchaseDto,
  CreateEventPageViewDto,
} from './dto';
import { ApiTags } from '@nestjs/swagger';
import { FacebookService } from './facebook.service';
import { ApiExceptionFilter } from '../filters';
import {
  ApiSuccessResponseInterceptor,
  ApiErrorResponseInterceptor,
} from '../interceptors';
import { FacebookApiResponse } from './facebook.response';

@ApiTags('facebook')
@UseInterceptors(ApiSuccessResponseInterceptor, ApiErrorResponseInterceptor)
@UseFilters(new ApiExceptionFilter())
@Controller('facebook')
export class FacebookController {
  constructor(private readonly facebookService: FacebookService) {}

  @Post('page-view')
  @FacebookApiResponse()
  @UsePipes(new ValidationPipe({ transform: true }))
  createEventPageView(@Body() eventDto: CreateEventPageViewDto) {
    return this.facebookService.createEventPageView(eventDto);
  }

  @Post('view-content')
  @FacebookApiResponse()
  @UsePipes(new ValidationPipe({ transform: true }))
  createEventViewContent(@Body() eventDto: CreateEventViewContentDto) {
    return this.facebookService.createEventViewContent(eventDto);
  }

  @Post('complete-registration')
  @FacebookApiResponse()
  @UsePipes(new ValidationPipe({ transform: true }))
  createEventCompleteRegistration(
    @Body()
    eventDto: CreateEventCompleteRegistrationDto,
  ) {
    return this.facebookService.createEventCompleteRegistration(eventDto);
  }

  @Post('add-to-cart')
  @FacebookApiResponse()
  @UsePipes(new ValidationPipe({ transform: true }))
  createEventAddToCart(@Body() eventDto: CreateEventAddToCartDto) {
    return this.facebookService.createEventAddToCart(eventDto);
  }

  @Post('purchase')
  @FacebookApiResponse()
  @UsePipes(new ValidationPipe({ transform: true }))
  createEventPurchase(@Body() eventDto: CreateEventPurchaseDto) {
    return this.facebookService.createEventPurchase(eventDto);
  }
}
