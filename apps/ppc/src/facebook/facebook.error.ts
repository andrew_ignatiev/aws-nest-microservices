import * as sdk from 'facebook-nodejs-business-sdk';
import { HttpStatus } from '@nestjs/common';
import { ApiErrorInterface } from '../interfaces';

type FacebookRequestError = InstanceType<typeof sdk.APIResponse>['error'];

export class FacebookError extends Error implements ApiErrorInterface {
  status = HttpStatus.INTERNAL_SERVER_ERROR;
  errors: Array<unknown> = [];

  constructor(error: string);
  constructor(error: Error | FacebookRequestError);
  constructor(error: any) {
    super(error?.message ?? error ?? 'Unexpected Facebook API Error');

    this.stack = new Error().stack;

    if (isFacebookRequestError(error)) {
      const { error: e } = error?.response ?? {};

      this.status = error.status ?? HttpStatus.INTERNAL_SERVER_ERROR;
      this.errors = e != null ? [e] : [];
    }
  }

  toJSON() {
    return {
      code: this.status,
      errors: this.errors,
      message: this.message,
    };
  }

  toString() {
    return JSON.stringify(this.toJSON());
  }
}

export function isFacebookRequestError(
  error: any,
): error is FacebookRequestError {
  return (
    typeof error?.name === 'string' &&
    error.name === 'FacebookRequestError' &&
    error instanceof Error
  );
}
