import { applyDecorators } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
} from '@nestjs/swagger';
import {
  ApiBadRequestResponseDto,
  ApiInternalServerErrorResponseDto,
  ApiSuccessResponseDto,
} from './dto';

export const FacebookApiResponse = () => {
  return applyDecorators(
    ApiCreatedResponse({
      type: ApiSuccessResponseDto,
      description: 'Success: Event Created',
    }),
    ApiBadRequestResponse({
      type: ApiBadRequestResponseDto,
      description: 'Error: Bad Request',
    }),
    ApiInternalServerErrorResponse({
      type: ApiInternalServerErrorResponseDto,
      description: 'Error: Internal Server Error',
    }),
  );
};
