import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ServerlessModule, ServerlessService } from '@app-lib/serverless';

export async function createApp() {
  const app = await NestFactory.create(AppModule);
  const cfg = {
    title: 'PPC Tracking API',
    description:
      'The PPC Tracking API to create and send events to third-party trackers (facebook, google, etc)',
    version: '2.0.0',
  };

  return app
    .setGlobalPrefix('v2.0.0/ppc')
    .select(ServerlessModule)
    .get(ServerlessService)
    .createAppDocument(app, '/v2.0.0/ppc-docs/', cfg);
}
