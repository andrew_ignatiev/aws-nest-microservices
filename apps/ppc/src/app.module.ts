import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { HealthModule } from './health/health.module';
import { ConfigModule } from '@nestjs/config';
import { FacebookModule } from './facebook/facebook.module';
import { ServerlessModule } from '@app-lib/serverless';

@Module({
  imports: [
    ConfigModule.forRoot(),
    HealthModule,
    FacebookModule,
    ServerlessModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
