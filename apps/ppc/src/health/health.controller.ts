import { Controller, Get } from '@nestjs/common';
import { HealthCheckService, HealthCheck } from '@nestjs/terminus';
import { HealthIndicator } from './health.indicator';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('health checks')
@Controller('health')
export class HealthController {
  constructor(
    private health: HealthCheckService,
    private appHealth: HealthIndicator,
  ) {}

  @Get()
  @HealthCheck()
  check() {
    return this.health.check([async () => this.appHealth.check()]);
  }
}
