import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { HealthController } from './health.controller';
import { HealthIndicator } from './health.indicator';
import { FacebookConfigModule } from '../config/facebook/config.module';
import { FacebookService } from '../facebook/facebook.service';

@Module({
  imports: [FacebookConfigModule, TerminusModule],
  controllers: [HealthController],
  providers: [FacebookService, HealthIndicator],
})
export class HealthModule {}
