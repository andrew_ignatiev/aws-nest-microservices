import { Injectable } from '@nestjs/common';
import {
  HealthIndicator as TerminusHealthIndicator,
  HealthIndicatorResult,
  HealthCheckError,
} from '@nestjs/terminus';
import { FacebookService } from '../facebook/facebook.service';
import { CreateEventBaseDto } from '../facebook/dto';

@Injectable()
export class HealthIndicator extends TerminusHealthIndicator {
  constructor(private facebookService: FacebookService) {
    super();
  }

  async check(): Promise<HealthIndicatorResult> {
    const data = {};
    const event: CreateEventBaseDto = {
      userId: 1,
      eventId: `HealthCheck.1.${Date.now()}`,
      sourceUrl: '',
      userAgent: '',
    };

    const res = await this.facebookService.createEventHealthCheck(event);

    if (res.error != null) {
      data['error'] = res.error;
    }

    const isHealthy = data['error'] == null;
    const result = this.getStatus('facebook', isHealthy, data);

    if (isHealthy) {
      return result;
    }

    throw new HealthCheckError('Health check failed', result);
  }
}
