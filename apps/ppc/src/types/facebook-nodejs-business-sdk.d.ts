declare module 'http-status' {
  const _default: {
    OK: string;
    NOT_MODIFIED: string;
  };
  /**
   * Copyright (c) 2017-present, Facebook, Inc.
   * All rights reserved.
   *
   * This source code is licensed under the license found in the
   * LICENSE file in the root directory of this source tree.
   *
   * @format
   */
  export default _default;
}
declare module 'http' {
  export default class Http {
    /**
     * Request
     * @param   {String}  method
     * @param   {String}  url
     * @param   {Object}  [data]
     * @return  {Promise}
     */
    static request(
      method: string,
      url: string,
      data: Record<string, any>,
      files: Record<string, any>,
      useMultipartFormData: boolean,
      showHeader: boolean,
    ): Promise<any>;
    /**
     * XmlHttpRequest request
     * @param   {String}  method
     * @param   {String}  url
     * @param   {Object}  [data]
     * @return  {Promise}
     */
    static xmlHttpRequest(method: any, url: any, data: any): Promise<any>;
    /**
     * Request Promise
     * @param   {String}  method The HTTP method name (e.g. 'GET').
     * @param   {String}  url A full URL string.
     * @param   {Object}  [data] A mapping of request parameters where a key
     *   is the parameter name and its value is a string or an object
     *   which can be JSON-encoded.
     * @param   {Object}  [files] An optional mapping of file names to ReadStream
     *   objects. These files will be attached to the request.
     * @param   {Boolean} [useMultipartFormData] An optional flag to call with
     *   multipart/form-data.
     * @param showHeader
     * @return  {Promise}
     */
    static requestPromise(
      method: string,
      url: string,
      data: Record<string, any>,
      files: Record<string, any>,
      useMultipartFormData?: boolean,
      showHeader?: boolean,
    ): Promise<any>;
  }
}
declare module 'exceptions' {
  class FacebookError extends Error {
    name: string;
    message: string;
    stack: string;

    constructor(error: any);
  }

  /**
   * Raised when an api request fails.
   */
  export class FacebookRequestError extends FacebookError {
    name: string;
    message: string;
    status: number;
    response: Record<string, unknown>;
    headers: Record<string, unknown>;
    method: string;
    url: string;

    /**
     * @param  {[Object}  response
     * @param  {String}   method
     * @param  {String}   url
     * @param  {Object}   data
     */
    constructor(response: any, method: any, url: any, data: any);
  }
}
declare module 'crash-reporter' {
  export default class CrashReporter {
    static _instance: CrashReporter;
    _active: boolean;
    constructor();
    static enable(): void;
    static disable(): void;
  }
}
declare module 'api' {
  /**
   * Facebook Ads API
   */
  export default class FacebookAdsApi {
    _debug: boolean;
    _showHeader: boolean;
    accessToken: string;
    locale: string;
    static _defaultApi: FacebookAdsApi;
    static get VERSION(): string;
    static get SDK_VERSION(): string;
    static get GRAPH(): string;
    static get GRAPH_VIDEO(): string;
    /**
     * @param {String} accessToken
     * @param {String} [locale]
     */
    constructor(accessToken: string, locale?: string, crash_log?: boolean);
    /**
     * Instantiate an API and store it as the default
     * @param  {String} accessToken
     * @param  {String} [locale]
     * @return {FacebookAdsApi}
     */
    static init(
      accessToken: string,
      locale?: string,
      crash_log?: boolean,
    ): FacebookAdsApi;
    static setDefaultApi(api: FacebookAdsApi): void;
    static getDefaultApi(): FacebookAdsApi;
    getAppID(): Promise<any>;
    setDebug(flag: boolean): FacebookAdsApi;
    setShowHeader(flag: boolean): FacebookAdsApi;
    /**
     * Http Request
     * @param  {String} method
     * @param  {String} path
     * @param  {Object} [params]
     * @param  {Object} [files]
     * @return {Promise}
     */
    call(
      method: string,
      path: string | Array<string> | string,
      params?: Record<string, any>,
      files?: Record<string, any>,
      useMultipartFormData?: boolean,
      urlOverride?: string,
    ): Promise<any>;
    static _encodeParams(params: Record<string, any>): string;
  }
}
declare module 'api-request' {
  /**
   * Represents an API request
   */
  class APIRequest {
    _nodeId: string;
    _method: string;
    _endpoint: string;
    _path: Array<string>;
    _fields: Array<string>;
    _params: Record<string, any>;
    _fileParams: Record<string, any>;
    _fileCounter: number;
    /**
     * @param {string} nodeId The node id to perform the api call.
     * @param {string} method The HTTP method of the call.
     * @param {string} endpoint The edge of the api call.
     */
    constructor(nodeId: string, method: string, endpoint: string);
    /**
     * Getter function for node ID
     * @return {string} Node ID
     */
    get nodeId(): string;
    /**
     * Getter function for HTTP method e.g. GET, POST
     * @return {string} HTTP method
     */
    get method(): string;
    /**
     * Getter function for the edge of the API call
     * @return {string} Endpoint edge
     */
    get endpoint(): string;
    /**
     * Getter function for path tokens
     * @return {Array<string>} Array of path tokens
     */
    get path(): Array<string>;
    /**
     * Getter function for requested fields
     * @return {Array<string>} Array of request fields
     */
    get fields(): Array<string>;
    /**
     * Getter function for API params
     * @return {Object} Object containing API Params
     */
    get params(): Record<string, any>;
    /**
     * Getter function for API fileparams
     * @return {Object} Object containing API fileParams
     */
    get fileParams(): Record<string, any>;
    /**
     * @param {string} filePath Path to file attached to the request
     * @return {APIReqeust} APIRequest instance
     */
    addFile(filePath: string): APIRequest;
    /**
     * @param {string[]} filePaths Array of paths to files attached to the request
     * @return {APIRequest} APIRequest instance
     */
    addFiles(filePaths: Array<string>): APIRequest;
    /**
     * @param {string} field Requested field
     * @return {APIReqeust} APIRequest instance
     */
    addField(field: string): APIRequest;
    /**
     * @param {string[]} fields Array of requested fields
     * @return {APIRequest} APIRequest instance
     */
    addFields(fields: Array<string>): APIRequest;
    /**
     * @param {string} key Param key
     * @param {*} value Param value
     * @return {APIRequest} APIRequest instance
     */
    addParam(key: string, value: any): APIRequest;
    /**
     * @param {Object} params An object containing param keys and values
     * @return {APIRequest} APIRequest instance
     */
    addParams(params: Record<string, any>): APIRequest;
  }
  export default APIRequest;
}
declare module 'api-response' {
  import { FacebookRequestError } from 'exceptions';
  /**
   * Encapsulates an http response from Facebook's Graph API.
   */
  class APIResponse {
    _body: Record<string, any>;
    _httpStatus: string;
    _headers: Record<string, any>;
    _call: Record<string, any>;
    _response: Record<string, any>;
    constructor(response: Record<string, any>, call?: Record<string, any>);
    /**
     * @return {Object} The response body
     */
    get body(): Record<string, any>;
    get headers(): Record<string, any>;
    get etag(): any;
    get status(): string;
    get isSuccess(): any;
    get error(): FacebookRequestError;
  }
  export default APIResponse;
}
declare module 'api-batch' {
  import FacebookAdsApi from 'api';
  import APIRequest from 'api-request';
  /**
   * Facebook Ads API Batch
   */
  class FacebookAdsApiBatch {
    _api: FacebookAdsApi;
    _files: Array<Record<string, any>>;
    _batch: Array<Record<string, any>>;
    _successCallbacks: Array<(...args: Array<any>) => any>;
    _failureCallbacks: Array<(...args: Array<any>) => any>;
    _requests: Array<APIRequest>;
    /**
     * @param {FacebookAdsApi} api
     * @param {Function} successCallback
     * @param {Function} failureCallback
     */
    constructor(
      api: FacebookAdsApi,
      successCallback?: (...args: Array<any>) => any,
      failureCallback?: (...args: Array<any>) => any,
    );
    /**
     * Adds a call to the batch.
     * @param  {string} method The HTTP method name (e.g. 'GET').
     * @param  {string[]|string} relativePath An array of path tokens or
     *   a relative URL string. An array will be translated to a url as follows:
     *     <graph url>/<tuple[0]>/<tuple[1]>...
     *   It will be assumed that if the path is not a string, it will be iterable.
     * @param  {Object} [params] A mapping of request parameters
     *   where a key is the parameter name and its value is a string or an object
     *   which can be JSON-encoded.
     * @param {Object} [files] An optional mapping of file names to binary open
     *   file objects. These files will be attached to the request.
     * @param {Function} [successCallback] A callback function which will be
     *   called with the response of this call if the call succeeded.
     * @param {Function} [failureCallback] A callback function which will be
     *   called with the response of this call if the call failed.
     * @param {APIRequest} [request] The APIRequest object
     * @return {Object} An object describing the call
     */
    add(
      method: string,
      relativePath: Array<string> | string,
      params?: Record<string, any>,
      files?: Record<string, any>,
      successCallback?: (...args: Array<any>) => any,
      failureCallback?: (...args: Array<any>) => any,
      request?: APIRequest,
    ): {
      method: string;
      relative_url: string;
      body: string;
      name: any;
      attachedFiles: string;
    };
    /**
     * Interface to add a APIRequest to the batch.
     * @param  {APIRequest} request The APIRequest object to add
     * @param  {Function} [successCallback] A callback function which
     *   will be called with response of this call if the call succeeded.
     * @param  {Function} [failureCallback] A callback function which
     *   will be called with the FacebookResponse of this call if the call failed.
     * @return {Object} An object describing the call
     */
    addRequest(
      request: APIRequest,
      successCallback?: (...args: Array<any>) => any,
      failureCallback?: (...args: Array<any>) => any,
    ): {
      method: string;
      relative_url: string;
      body: string;
      name: any;
      attachedFiles: string;
    };
    /**
     * Makes a batch call to the api associated with this object.
     * For each individual call response, calls the success or failure callback
     * function if they were specified.
     * Note: Does not explicitly raise exceptions. Individual exceptions won't
     * be thrown for each call that fails. The success and failure callback
     * functions corresponding to a call should handle its success or failure.
     * @return {FacebookAdsApiBatch|None} If some of the calls have failed,
     *   returns a new FacebookAdsApiBatch object with those calls.
     *   Otherwise, returns None.
     */
    execute(): Promise<FacebookAdsApiBatch>;
  }
  export default FacebookAdsApiBatch;
}
declare module 'abstract-object' {
  export default class AbstractObject {
    _data: any;
    _fields: Array<string>;
    static get Fields(): Readonly<Record<any, unknown>>;
    constructor();
    /**
     * Define data getter and setter field
     * @param {String} field
     */
    _defineProperty(field: string): void;
    /**
     * Set data field
     * @param {String} field
     * @param {Mixed} value
     * @return this
     */
    set(field: string, value: unknown): AbstractObject;
    /**
     * Set multiple data fields
     * @param {Object} data
     * @return this
     */
    setData(data: Record<string, any>): AbstractObject;
    /**
     * Export object data
     * @return {Object}
     */
    exportData(): Record<string, any>;
  }
}
declare module 'utils' {
  export default class Utils {
    static normalizeEndpoint(str: string): string;
    static removePreceedingSlash(str: string): string;
  }
}
declare module 'cursor' {
  import FacebookAdsApi from 'api';
  export default class Cursor extends Array<Record<string, any>> {
    sourceObject: Record<string, any>;
    _api: FacebookAdsApi;
    _targetClass: Record<string, any>;
    _loadPage: (path: string) => Promise<any>;
    _buildObjectsFromResponse: (
      response: Record<string, any>,
    ) => Array<Record<string, any>>;
    paging: any;
    summary: any;
    clear: () => void;
    next: () => any;
    previous: () => Promise<any>;
    hasNext: () => boolean;
    hasPrevious: () => boolean;
    set: (array: Array<Record<string, any>>) => void;
    /**
     * @param  {Object} sourceObject
     * @param  {Object} targetClass
     * @param  {Object} [params]
     * @param  {String} [endpoint]
     */
    constructor(
      sourceObject: Record<string, any>,
      targetClass: Record<string, any>,
      params: Record<string, any>,
      endpoint: string | null | undefined,
    );
  }
}
declare module 'abstract-crud-object' {
  import FacebookAdsApi from 'api';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  export class AbstractCrudObject extends AbstractObject {
    _parentId: string | null | undefined;
    _changes: Record<string, any>;
    _api: FacebookAdsApi;
    id: string;
    /**
     * @param  {Object} data
     * @param  {String} parentId
     * @param  {FacebookAdApi} [api]
     */
    constructor(
      id: number | (string | null | undefined),
      data: Record<string, any>,
      parentId: string | null | undefined,
      api: FacebookAdsApi | null | undefined,
    );
    /**
     * Define data getter and setter recording changes
     * @param {String} field
     */
    _defineProperty(field: string): void;
    /**
     * Set object data as if it were read from the server. Wipes related changes
     * @param {Object} data
     * @return this
     */
    setData(data: Record<string, any>): AbstractCrudObject;
    /**
     * Export changed object data
     * @return {Object}
     */
    exportData(): Record<string, any>;
    /**
     * Export object data
     * @return {Object}
     */
    exportAllData(): Record<string, any>;
    /**
     * Clear change history
     * @return this
     */
    clearHistory(): Record<string, any>;
    /**
     * @throws {Error} if object has no id
     * @return {String}
     */
    getId(): string;
    /**
     * @throws {Error} if object has no parent id
     * @return {String}
     */
    getParentId(): string;
    /**
     * @return {String}
     */
    getNodePath(): string;
    /**
     * Return object API instance
     * @throws {Error} if object doesn't hold an API
     * @return {FacebookAdsApi}
     */
    getApi(): FacebookAdsApi;
    /**
     * Read object data
     * @param   {Array}   [fields]
     * @param   {Object}  [params]
     * @return  {Promise}
     */
    read(fields: Array<string>, params?: Record<string, any>): Promise<any>;

    update(...args: any[]): any;
    delete(...args: any[]): any;

    /**
     * Initialize Cursor to paginate on edges
     * @param  {Object}  targetClass
     * @param  {Array}   [fields]
     * @param  {Object}  [params]
     * @param  {Boolean} [fetchFirstPage]
     * @param  {String}  [endpoint]
     * @return {Cursor}
     */
    getEdge(
      targetClass: Record<string, any>,
      fields: Array<string>,
      params: Record<string, any>,
      fetchFirstPage: boolean,
      endpoint: string | null | undefined,
    ): Cursor | Promise<any>;
    /**
     * Create edge object
     * @param   {String}  [endpoint]
     * @param   {Array}  [fields]
     * @param   {Object}  [params]
     * @param   {Function} [targetClassConstructor]
     * @return  {Promise}
     */
    createEdge(
      endpoint: string,
      fields: Array<string>,
      params?: Record<string, any>,
      targetClassConstructor?: (...args: Array<any>) => any,
    ): Promise<any>;
    /**
     * Delete edge object
     * @param   {String}  [endpoint]
     * @param   {Object}  [params]
     * @return  {Promise}
     */
    deleteEdge(endpoint: string, params?: Record<string, any>): Promise<any>;
    /**
     * Read Objects by Ids
     * @param  {Array}          ids
     * @param  {Array}          [fields]
     * @param  {Object}         [params]
     * @param  {FacebookAdsApi} [api]
     * @return {Promise}
     */
    static getByIds(
      ids: Array<number>,
      fields: Array<string>,
      params: Record<string, any>,
      api: FacebookAdsApi,
    ): Promise<any>;
  }
  export default AbstractCrudObject;
}
declare module 'objects/serverside/delivery-category' {
  const _default_1: Readonly<{
    /**
     * Customer needs to enter the store to get the purchased product.
     */
    IN_STORE: string;
    /**
     * Customer picks up their order by driving to a store and waiting inside their vehicle.
     */
    CURBSIDE: string;
    /**
     * Purchase is delivered to the customer's home.
     */
    HOME_DELIVERY: string;
  }>;
  /**
   * Type of delivery for a purchase event.
   */
  export default _default_1;
}
declare module 'objects/serverside/utils' {
  /**
   * ServerSideUtils contains the Utility modules used for sending Server Side Events
   */
  export default class ServerSideUtils {
    /**
     * Normalizes and hashes the input given the field name.
     * @param  {String} [input] Value to be normalized. eg: `foo@bar.com` for email input.
     * @param  {String} [field] Key(Type) of Value to be normalized eg: 'em' for email field.
     * @return {String} Normalized and hashed value for the string.
     */
    static normalizeAndHash(input: string, field: string): any;
    /**
     * Normalizes the given country token and returns acceptable two letter ISO country code
     * @param  {String} [country] country value to be normalized.
     * @return {String} Normalized ISO country code.
     */
    static normalizeCountry(country: string): string;
    /**
     * Normalizes the given city and returns acceptable city value
     * @param  {String} [city] city value to be normalized.
     * @return {String} Normalized city value.
     */
    static normalizeCity(city: string): string;
    /**
     * Normalizes the given currency string and returns acceptable three letter  ISO code
     * @param  {String} [currency] country value to be normalized.
     * @return {String} Normalized ISO currency code.
     */
    static normalizeCurrency(currency: string): string;
    /**
     * Normalizes the given delivery category value and returns a valid string.
     * @param  {String} [input] delivery_category input to be validated.
     * @return {String} Valid delivery_category value.
     */
    static normalizeDeliveryCategory(input: string): string;
    /**
     * Normalizes the given email to RFC 822 standard and returns acceptable email value
     * @param  {String} [email] email value to be normalized.
     * @return {String} Normalized email value.
     */
    static normalizeEmail(email: string): string;
    /**
     * Normalizes the given gender and returns acceptable('f' or 'm') gender value
     * @param  {String} [gender] gender value to be normalized.
     * @return {String} Normalized gender value.
     */
    static normalizeGender(gender: string): string;
    /**
     * Normalizes the 5 character name field.
     * @param  {String} [name] name value to be normalized.
     * @return {String} Normalized 5 character {first,last}name field value.
     */
    static normalizeF5NameField(name: string): string;
    /**
     * Normalizes the given phone and returns acceptable phone value
     * @param  {String} [phone_number] phone number value to be normalized.
     * @return {String} Normalized phone number value.
     */
    static normalizePhone(phone_number: string): string;
    /**
     * Normalizes the given state and returns acceptable city value
     * @param  {String} [state] state value to be normalized.
     * @return {String} Normalized state value.
     */
    static normalizeState(state: string): string;
    /**
     * Normalizes the given zip/postal code and returns acceptable zip code value
     * @param  {String} [zip] zip value to be normalized.
     * @return {String} Normalized zip code value.
     */
    static normalizeZip(zip: string): string;
    /**
     * Normalizes the given date of birth day
     * @param  {String} [dobd] value to be normalized.
     * @return {String} Normalized value.
     */
    static normalizeDobd(dobd: string): string;
    /**
     * Normalizes the given date of birth month
     * @param  {String} [dobm] value to be normalized.
     * @return {String} Normalized value.
     */
    static normalizeDobm(dobm: string): string;
    /**
     * Normalizes the given date of birth year
     * @param  {String} [doby] value to be normalized.
     * @return {String} Normalized value.
     */
    static normalizeDoby(doby: string): string;
    /**
     * Boolean method which checks if a given number is represented in international format
     * @param  {String} phone_number that has to be tested.
     * @return {Boolean} value if a number is represented international format
     */
    static isInternationalPhoneNumber(phone_number: string): boolean;
    /**
     * Calculates the SHA 256 hash of a given non-null string.
     * @param  {String} [input] String to be hashed
     * @return {String} SHA 256 Hash of the string
     */
    static toSHA256(input: string | null | undefined): any;
  }
}
declare module 'objects/serverside/content' {
  export default class Content {
    _id: string;
    _quantity: number;
    _item_price: number;
    _title: string;
    _description: string;
    _category: string;
    _brand: string;
    _delivery_category: string;
    /**
     * @param {String} id Product Id of the Item.
     * @param {Number} quantity Quantity of the Item.
     * @param {Number} item_price Price per unit of the content/product.
     * @param {String} title Title of the listed Item.
     * @param {String} description Product description used for the item.
     * @param {String} brand Brand of the item.
     * @param {String} category Category of the Item.
     * @param {String} delivery_category The type of delivery for a purchase event
     */
    constructor(
      id?: string,
      quantity?: number,
      item_price?: number,
      title?: string,
      description?: string,
      brand?: string,
      category?: string,
      delivery_category?: string,
    );
    /**
     * Gets the Product Id of the Item.
     * A string representing the unique Id for the product.
     * Example: XYZ.
     */
    get id(): string;
    /**
     * Sets the Product Id of the Item.
     * @param id A string representing the unique Id for the product.
     * Example: XYZ.
     */
    set id(id: string);
    /**
     * Sets the Product Id of the Item.
     * @param id is a string representing the unique id for the product.
     * Example: XYZ.
     */
    setId(id: string): Content;
    /**
     * Gets the quantity of the Item.
     * The number/quantity of the content that is being involved in the customer interaction.
     * Example: 5
     */
    get quantity(): number;
    /**
     * Sets the quantity of the Item.
     * @param quantity The number/quantity of the product that is being involved in the customer interaction.
     * Example: 5
     */
    set quantity(quantity: number);
    /**
     * Sets the quantity of the Content/Item.
     * @param {Number} quantity The number/quantity of the product that is being involved in the customer interaction.
     * Example: 5
     */
    setQuantity(quantity: number): Content;
    /**
     * Gets the item price for the Product.
     * The item_price or price per unit of the product.
     * Example: '123.45'
     */
    get item_price(): number;
    /**
     * Sets the item price for the Content.
     * @param item_price The item_price or price per unit of the product.
     * Example: '123.45'
     */
    set item_price(item_price: number);
    /**
     * Sets the item price for the Content.
     * @param {Number} item_price The item_price or price per unit of the product.
     * Example: '123.45'
     */
    setItemPrice(item_price: number): Content;
    /**
     * Gets the Title of the listed Item.
     * A string representing the Title for the product.
     */
    get title(): string;
    /**
     * Sets the Title of the listed Item.
     * @param title A string representing the Title for the product.
     */
    set title(title: string);
    /**
     * Sets the Title of the Item.
     * @param title is a string representing listed title for the product.
     */
    setTitle(title: string): Content;
    /**
     * Gets the Description of the listed Item.
     * A string representing the Description for the product.
     */
    get description(): string;
    /**
     * Sets the Description of the listed Item.
     * @param description A string representing the Description for the product.
     */
    set description(description: string);
    /**
     * Sets the Product Description of the Item.
     * @param description is a string representing the description for the product.
     */
    setDescription(description: string): Content;
    /**
     * Gets the Brand of the listed Item.
     * A string representing the Brand for the product.
     */
    get brand(): string;
    /**
     * Sets the Brand of the listed Item.
     * @param brand A string representing the Brand for the product.
     */
    set brand(brand: string);
    /**
     * Sets the Brand of the Product.
     * @param brand is a string representing the Brand for the product.
     */
    setBrand(brand: string): Content;
    /**
     * Gets the Category of the listed Item.
     * A string representing the Category for the product.
     */
    get category(): string;
    /**
     * Sets the Category of the listed Item.
     * @param category A string representing the Category for the product.
     */
    set category(category: string);
    /**
     * Sets the Category of the Product.
     * @param category is a string representing the Category for the product.
     */
    setCategory(category: string): Content;
    /**
     * Gets the delivery category.
     */
    get delivery_category(): string;
    /**
     * Sets the type of delivery for a purchase event.
     * @param delivery_category The delivery category.
     */
    set delivery_category(delivery_category: string);
    /**
     * Sets the type of delivery for a purchase event.
     * @param {String} delivery_category The delivery category.
     */
    setDeliveryCategory(delivery_category: string): Content;
    /**
     * Returns the normalized payload for the Content.
     * @returns {Object} normalized Content payload.
     */
    normalize(): Record<string, any>;
  }
}
declare module 'objects/serverside/custom-data' {
  import Content from 'objects/serverside/content';
  /**
   * CustomData represents the Custom Data Parameters of a Server Side Event Request. Use these parameters to send additional data we can use for ads delivery optimization.
   * @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#custom}
   */
  export default class CustomData {
    _value: number;
    _currency: string;
    _content_name: string;
    _content_category: string;
    _content_ids: Array<string>;
    _contents: Array<Content>;
    _content_type: string;
    _order_id: string;
    _predicted_ltv: number;
    _num_items: number;
    _search_string: string;
    _status: string;
    _item_number: string;
    _delivery_category: string;
    _custom_properties: Record<string, any>;
    /**
     * @param {Number} value value of the item Eg: 123.45
     * @param {String} currency currency involved in the transaction Eg: usd
     * @param {String} content_name name of the Content Eg: lettuce
     * @param {String} content_category category of the content Eg: grocery
     * @param {Array<String>} content_ids list of content unique ids involved in the event
     * @param {Array<Content>} contents Array of Content Objects. Use {Content} class to define a content.
     * @param {String} content_type Type of the Content group or Product SKU
     * @param {String} order_id Unique id representing the order
     * @param {Number} predicted_ltv Predicted LifeTime Value for the customer involved in the event
     * @param {Number} num_items Number of items involved
     * @param {String} search_string query string used for the Search event
     * @param {String} status Status of the registration in Registration event
     * @param {String} item_number The item number
     * @param {String} delivery_category The type of delivery for a purchase event
     * @param {Object} custom_properties Custom Properties to be added to the Custom Data
     */
    constructor(
      value?: number,
      currency?: string,
      content_name?: string,
      content_category?: string,
      content_ids?: Array<string>,
      contents?: Array<Content>,
      content_type?: string,
      order_id?: string,
      predicted_ltv?: number,
      num_items?: number,
      search_string?: string,
      status?: string,
      item_number?: string,
      delivery_category?: string,
      custom_properties?: Record<string, any>,
    );
    /**
     * Gets the value of the custom data.
     * A numeric value associated with this event. This could be a monetary value or a value in some other metric.
     * Example: 142.54.
     */
    get value(): number;
    /**
     * Sets the value of the custom data.
     * @param value A numeric value associated with this event. This could be a monetary value or a value in some other metric.
     * Example: 142.54.
     */
    set value(value: number);
    /**
     * Sets the value of the custom data.
     * @param {Number} value A numeric value associated with this event. This could be a monetary value or a value in some other metric.
     * Example: 142.54.
     */
    setValue(value: number): CustomData;
    /**
     * Gets the currency for the custom data.
     * The currency for the value specified, if applicable. Currency must be a valid ISO 4217 three digit currency code.
     * Example: 'usd'
     */
    get currency(): string;
    /**
     * Sets the currency for the custom data.
     * @param currency The currency for the value specified, if applicable. Currency must be a valid ISO 4217 three digit currency code.
     * Example: 'usd'
     */
    set currency(currency: string);
    /**
     * Sets the currency for the custom data.
     * @param {String} currency The currency for the value specified, if applicable. Currency must be a valid ISO 4217 three digit currency code.
     * Example: 'usd'
     */
    setCurrency(currency: string): CustomData;
    /**
     * Gets the content name for the custom data. The name of the page or product associated with the event.
     * The name of the page or product associated with the event.
     * Example: 'lettuce'
     */
    get content_name(): string;
    /**
     * Sets the content name for the custom data.
     * @param content_name The name of the page or product associated with the event.
     * Example: 'lettuce'
     */
    set content_name(content_name: string);
    /**
     * Sets the content name for the custom data.
     * @param content_name The name of the page or product associated with the event.
     * Example: 'lettuce'
     */
    setContentName(content_name: string): CustomData;
    /**
     * Gets the content category for the custom data.
     * The category of the content associated with the event.
     * Example: 'grocery'
     */
    get content_category(): string;
    /**
     * Sets the content_category for the custom data.
     * @param content_category The category of the content associated with the event.
     * Example: 'grocery'
     */
    set content_category(content_category: string);
    /**
     * Sets the content_category for the custom data.
     * @param content_category The category of the content associated with the event.
     * Example: 'grocery'
     */
    setContentCategory(content_category: string): CustomData;
    /**
     * Gets the content_ids for the custom data.
     * The content IDs associated with the event, such as product SKUs for items in an AddToCart, represented as Array of string.
     * If content_type is a product, then your content IDs must be an array with a single string value. Otherwise, this array can contain any number of string values.
     * Example: ['ABC123', 'XYZ789']
     */
    get content_ids(): Array<string>;
    /**
     * Sets the content_ids for the custom data.
     * @param content_ids The content IDs associated with the event, such as product SKUs for items in an AddToCart, represented as Array of string.
     * If content_type is a product, then your content IDs must be an array with a single string value. Otherwise, this array can contain any number of string values.
     * Example: ['ABC123', 'XYZ789']
     */
    set content_ids(content_ids: Array<string>);
    /**
     * Sets the content_ids for the custom data.
     * @param {Array} content_ids The content IDs associated with the event, such as product SKUs for items in an AddToCart, represented as Array of string.
     * If content_type is a product, then your content IDs must be an array with a single string value. Otherwise, this array can contain any number of string values.
     * Example: ['ABC123', 'XYZ789']
     */
    setContentIds(content_ids: Array<string>): CustomData;
    /**
     * Gets the contents for the custom data.
     * An array of Content objects that contain the product IDs associated with the event plus information about the products. id, quantity, and item_price are available fields.
     * Example: [{'id':'ABC123','quantity' :2,'item_price':5.99}, {'id':'XYZ789','quantity':2, 'item_price':9.99}]
     */
    get contents(): Array<Content>;
    /**
     * Sets the contents for the custom data.
     * @param contents An array of Content objects that contain the product IDs associated with the event plus information about the products. id, quantity, and item_price are available fields.
     * Example: [{'id':'ABC123','quantity' :2,'item_price':5.99}, {'id':'XYZ789','quantity':2, 'item_price':9.99}]
     */
    set contents(contents: Array<Content>);
    /**
     * Sets the contents for the custom data.
     * @param {Array<Content>} contents An array of Content objects that contain the product IDs associated with the event plus information about the products. id, quantity, and item_price are available fields.
     * Example: [{'id':'ABC123','quantity' :2,'item_price':5.99}, {'id':'XYZ789','quantity':2, 'item_price':9.99}]
     */
    setContents(contents: Array<Content>): CustomData;
    /**
     * Gets the content type for the custom data.
     * A String equal to either product or product_group. Set to product if the keys you send content_ids or contents represent products.
     * Set to product_group if the keys you send in content_ids represent product groups.
     */
    get content_type(): string;
    /**
     * Sets the content type for the custom data.
     * A String equal to either product or product_group. Set to product if the keys you send content_ids or contents represent products.
     * Set to product_group if the keys you send in content_ids represent product groups.
     */
    set content_type(content_type: string);
    /**
     * Sets the content type for the custom data.
     * @param {String} content_type A string equal to either product or product_group. Set to product if the keys you send content_ids or contents represent products.
     * Set to product_group if the keys you send in content_ids represent product groups.
     */
    setContentType(content_type: string): CustomData;
    /**
     * Gets the order id for the custom data.
     * order_id is the order ID for this transaction as a String.
     * Example: 'order1234'
     */
    get order_id(): string;
    /**
     * Sets the order_id for the custom data.
     * @param order_id The order ID for this transaction as a String.
     * Example: 'order1234'
     */
    set order_id(order_id: string);
    /**
     * Sets the order_id for the custom data.
     * @param {String} order_id The order ID for this transaction as a String.
     * Example: 'order1234'
     */
    setOrderId(order_id: string): CustomData;
    /**
     * Gets the predicted LifeTimeValue for the (user) in custom data.
     * The predicted lifetime value of a conversion event, as a String.
     * Example: '432.12'
     */
    get predicted_ltv(): number;
    /**
     * Sets the predicted LifeTimeValue for the custom data.
     * @param predicted_ltv The predicted lifetime value of a conversion event, as a String.
     * Example: '432.12'
     */
    set predicted_ltv(predicted_ltv: number);
    /**
     * Sets the predicted LifeTimeValue for the custom data.
     * @param {Number} predicted_ltv The predicted lifetime value of a conversion event, as a String.
     * Example: '432.12'
     */
    setPredictedLtv(predicted_ltv: number): CustomData;
    /**
     * Gets the number of items for the custom data.
     * The number of items that a user tries to buy during checkout. Use only with InitiateCheckout type events.
     * Example: 5
     */
    get num_items(): number;
    /**
     * Sets the number of items for the custom data.
     * @param num_items The number of items that a user tries to buy during checkout. Use only with InitiateCheckout type events.
     * Example: 5
     */
    set num_items(num_items: number);
    /**
     * Sets the number of items for the custom data.
     * @param {Number} num_items The number of items that a user tries to buy during checkout. Use only with InitiateCheckout type events.
     * Example: 5
     */
    setNumItems(num_items: number): CustomData;
    /**
     * Gets the search string for the custom data.
     * A search query made by a user.Use only with Search events.
     * Eg: 'lettuce'
     */
    get search_string(): string;
    /**
     * Sets the search string for the custom data.
     * @param {Number} search_string A search query made by a user.Use only with Search events.
     * Eg: 'lettuce'
     */
    set search_string(search_string: string);
    /**
     * Sets the search string for the custom data.
     * @param search_string A search query made by a user.Use only with Search events.
     * Eg: 'lettuce'
     */
    setSearchString(search_string: string): CustomData;
    /**
     * Gets the item number.
     */
    get item_number(): string;
    /**
     * Sets the item number.
     * @param item_number The item number.
     */
    set item_number(item_number: string);
    /**
     * Sets the item number.
     * @param {String} item_number The item number.
     */
    setItemNumber(item_number: string): CustomData;
    /**
     * Gets the delivery category.
     */
    get delivery_category(): string;
    /**
     * Sets the type of delivery for a purchase event.
     * @param delivery_category The delivery category.
     */
    set delivery_category(delivery_category: string);
    /**
     * Sets the type of delivery for a purchase event.
     * @param {String} delivery_category The delivery category.
     */
    setDeliveryCategory(delivery_category: string): CustomData;
    /**
     * Gets the custom properties to be included in the Custom Data.
     * If our predefined object properties don't suit your needs, you can include your own, custom properties. Custom properties can be used with both standard and custom events, and can help you further define custom audiences.
     * This behavior is the same for Server-Side API and Facebook Pixel.
     * @see {@link https://developers.facebook.com/docs/marketing-api/server-side-api/parameters/custom-data#custom-properties}
     * Eg: '{ 'warehouse_location' : 'washington', 'package_size' : 'L'}'
     */
    get custom_properties(): Record<string, any>;
    /**
     * Sets the custom properties to be included in the Custom Data.
     * If our predefined object properties don't suit your needs, you can include your own, custom properties. Custom properties can be used with both standard and custom events, and can help you further define custom audiences.
     * This behavior is the same for Server-Side API and Facebook Pixel.
     * @see {@link https://developers.facebook.com/docs/marketing-api/server-side-api/parameters/custom-data#custom-properties}
     * @param {Object} custom_properties custom properties property bag to be included in the Custom Data. Eg: '{ 'warehouse_location' : 'washington', 'package_size' : 'L'}'
     */
    set custom_properties(custom_properties: Record<string, any>);
    /**
     * Sets the search string for the custom data.
     * @param custom_properties A custom properties property bag to be included in the Custom Data.
     * If our predefined object properties don't suit your needs, you can include your own, custom properties. Custom properties can be used with both standard and custom events, and can help you further define custom audiences.
     * This behavior is the same for Server-Side API and Facebook Pixel.
     * @see {@link https://developers.facebook.com/docs/marketing-api/server-side-api/parameters/custom-data#custom-properties}
     * Eg: '{ 'warehouse_location' : 'washington', 'package_size' : 'L'}'
     * * @returns {Object} custom_properties property bag.
     */
    setCustomProperties(custom_properties: Record<string, any>): CustomData;
    /**
     * Gets the status of the registration event.
     * Status of the registration event, as a String.Use only with CompleteRegistration events.
     */
    get status(): string;
    /**
     * Sets the status of the registration event.
     * @param status Status of the registration event, as a String.Use only with CompleteRegistration events.
     */
    set status(status: string);
    /**
     * Sets the status of the registration event.
     * @param {String} status Status of the registration event, as a String. Use only with CompleteRegistration events.
     */
    setStatus(status: string): CustomData;
    /**
     * Adds the custom property (key, value) to the custom property bag.
     * @param {string} key The Key for the property to be added.
     * @param {string} value The Value for the property to be added.
     */
    add_custom_property(key: string, value: string): void;
    /**
     * Returns the normalized payload for the custom_data.
     * @returns {Object} normalized custom_data payload.
     */
    normalize(): Record<string, any>;
  }
}
declare module 'objects/assigned-user' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AssignedUser
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AssignedUser extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/da-check' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * DACheck
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class DACheck extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get ConnectionMethod(): Record<string, any>;
  }
}
declare module 'objects/ad-activity' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdActivity
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdActivity extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get EventType(): Record<string, any>;
    static get Category(): Record<string, any>;
    static get DataSource(): Record<string, any>;
  }
}
declare module 'objects/ad-place-page-set' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdPlacePageSet
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdPlacePageSet extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Category(): Record<string, any>;
    static get LocationTypes(): Record<string, any>;
    static get TargetedAreaType(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): AdPlacePageSet;
  }
}
declare module 'objects/ad-creative-insights' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeInsights
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeInsights extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-preview' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdPreview
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdPreview extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get AdFormat(): Record<string, any>;
    static get RenderType(): Record<string, any>;
  }
}
declare module 'objects/ad-creative' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * AdCreative
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreative extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get ApplinkTreatment(): Record<string, any>;
    static get CallToActionType(): Record<string, any>;
    static get ObjectType(): Record<string, any>;
    static get Status(): Record<string, any>;
    static get AuthorizationCategory(): Record<string, any>;
    static get CategorizationCriteria(): Record<string, any>;
    static get CategoryMediaSource(): Record<string, any>;
    static get DynamicAdVoice(): Record<string, any>;
    static get InstantCheckoutSetting(): Record<string, any>;
    static get Operator(): Record<string, any>;
    createAdLabel(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdCreative>;
    getCreativeInsights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPreviews(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): AdCreative;
    update(fields: Array<string>, params?: Record<string, any>): AdCreative;
  }
}
declare module 'objects/ad-rule-history' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdRuleHistory
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdRuleHistory extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Action(): Record<string, any>;
  }
}
declare module 'objects/ad-rule' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * AdRule
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdRule extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Status(): Record<string, any>;
    createExecute(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    getHistory(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createPreview(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdRule>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): AdRule;
    update(fields: Array<string>, params?: Record<string, any>): AdRule;
  }
}
declare module 'objects/ads-insights' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdsInsights
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdsInsights extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get ActionAttributionWindows(): Record<string, any>;
    static get ActionBreakdowns(): Record<string, any>;
    static get ActionReportTime(): Record<string, any>;
    static get Breakdowns(): Record<string, any>;
    static get DatePreset(): Record<string, any>;
    static get Level(): Record<string, any>;
    static get SummaryActionBreakdowns(): Record<string, any>;
  }
}
declare module 'objects/ad-report-run' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  import FacebookAdsApi from 'api';
  /**
   * AdReportRun
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdReportRun extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getInsights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): AdReportRun;
    constructor(
      id: number | (string | null | undefined),
      data: Record<string, any>,
      parentId: string | null | undefined,
      api: FacebookAdsApi | null | undefined,
    );
  }
}
declare module 'objects/lead' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  /**
   * Lead
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Lead extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): Lead;
  }
}
declare module 'objects/targeting-sentence-line' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TargetingSentenceLine
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TargetingSentenceLine extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  import AdReportRun from 'objects/ad-report-run';
  /**
   * Ad
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Ad extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get BidType(): Record<string, any>;
    static get ConfiguredStatus(): Record<string, any>;
    static get EffectiveStatus(): Record<string, any>;
    static get Status(): Record<string, any>;
    static get DatePreset(): Record<string, any>;
    static get ExecutionOptions(): Record<string, any>;
    static get Operator(): Record<string, any>;
    static get StatusOption(): Record<string, any>;
    getAdCreatives(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdLabel(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Ad>;
    getAdRulesGoverned(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCopies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createCopy(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Ad>;
    getInsights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getInsightsAsync(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdReportRun>;
    getLeads(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPreviews(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getTargetingSentenceLines(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): Ad;
    update(fields: Array<string>, params?: Record<string, any>): Ad;
  }
}
declare module 'objects/ad-async-request' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  /**
   * AdAsyncRequest
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAsyncRequest extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Statuses(): Record<string, any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): AdAsyncRequest;
  }
}
declare module 'objects/content-delivery-report' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ContentDeliveryReport
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ContentDeliveryReport extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Platform(): Record<string, any>;
    static get Position(): Record<string, any>;
  }
}
declare module 'objects/ad-campaign-delivery-estimate' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCampaignDeliveryEstimate
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCampaignDeliveryEstimate extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get OptimizationGoal(): Record<string, any>;
  }
}
declare module 'objects/ad-set' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  import AdReportRun from 'objects/ad-report-run';
  /**
   * AdSet
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdSet extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get BidStrategy(): Record<string, any>;
    static get BillingEvent(): Record<string, any>;
    static get ConfiguredStatus(): Record<string, any>;
    static get EffectiveStatus(): Record<string, any>;
    static get OptimizationGoal(): Record<string, any>;
    static get Status(): Record<string, any>;
    static get DatePreset(): Record<string, any>;
    static get DestinationType(): Record<string, any>;
    static get ExecutionOptions(): Record<string, any>;
    static get FullFunnelExplorationMode(): Record<string, any>;
    static get MultiOptimizationGoalWeight(): Record<string, any>;
    static get OptimizationSubEvent(): Record<string, any>;
    static get TuneForCategory(): Record<string, any>;
    static get Operator(): Record<string, any>;
    static get StatusOption(): Record<string, any>;
    getActivities(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAdStudies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAdCreatives(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteAdLabels(params?: Record<string, any>): Promise<any>;
    createAdLabel(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdSet>;
    getAdRulesGoverned(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAds(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAsyncAdRequests(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getContentDeliveryReport(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCopies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createCopy(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdSet>;
    getDeliveryEstimate(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getInsights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getInsightsAsync(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdReportRun>;
    getTargetingSentenceLines(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): AdSet;
    update(fields: Array<string>, params?: Record<string, any>): AdSet;
  }
}
declare module 'objects/campaign' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  import AdReportRun from 'objects/ad-report-run';
  /**
   * Campaign
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Campaign extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get BidStrategy(): Record<string, any>;
    static get ConfiguredStatus(): Record<string, any>;
    static get EffectiveStatus(): Record<string, any>;
    static get Status(): Record<string, any>;
    static get DatePreset(): Record<string, any>;
    static get ExecutionOptions(): Record<string, any>;
    static get Objective(): Record<string, any>;
    static get SmartPromotionType(): Record<string, any>;
    static get SpecialAdCategories(): Record<string, any>;
    static get SpecialAdCategoryCountry(): Record<string, any>;
    static get Operator(): Record<string, any>;
    static get SpecialAdCategory(): Record<string, any>;
    static get StatusOption(): Record<string, any>;
    getAdStudies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdLabel(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Campaign>;
    getAdRulesGoverned(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAds(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAdSets(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getContentDeliveryReport(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCopies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createCopy(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Campaign>;
    getInsights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getInsightsAsync(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdReportRun>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): Campaign;
    update(fields: Array<string>, params?: Record<string, any>): Campaign;
  }
}
declare module 'objects/ad-study-cell' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * AdStudyCell
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdStudyCell extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get CreationTemplate(): Record<string, any>;
    getAdAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAdSets(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCampaigns(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): AdStudyCell;
    update(fields: Array<string>, params?: Record<string, any>): AdStudyCell;
  }
}
declare module 'objects/ad-network-analytics-sync-query-result' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdNetworkAnalyticsSyncQueryResult
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdNetworkAnalyticsSyncQueryResult extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get AggregationPeriod(): Record<string, any>;
    static get Breakdowns(): Record<string, any>;
    static get Metrics(): Record<string, any>;
    static get OrderingColumn(): Record<string, any>;
    static get OrderingType(): Record<string, any>;
  }
}
declare module 'objects/ad-network-analytics-async-query-result' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdNetworkAnalyticsAsyncQueryResult
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdNetworkAnalyticsAsyncQueryResult extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-placement' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdPlacement
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdPlacement extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): AdPlacement;
  }
}
declare module 'objects/custom-conversion-stats-result' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CustomConversionStatsResult
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CustomConversionStatsResult extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Aggregation(): Record<string, any>;
  }
}
declare module 'objects/custom-conversion' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * CustomConversion
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CustomConversion extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get CustomEventType(): Record<string, any>;
    getStats(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): CustomConversion;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): CustomConversion;
  }
}
declare module 'objects/instagram-user' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * InstagramUser
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class InstagramUser extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getAgencies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAuthorizedAdAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAuthorizedAdAccount(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<InstagramUser>;
    get(fields: Array<string>, params?: Record<string, any>): InstagramUser;
  }
}
declare module 'objects/custom-audience-session' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CustomAudienceSession
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CustomAudienceSession extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/custom-audienceshared-account-info' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CustomAudiencesharedAccountInfo
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CustomAudiencesharedAccountInfo extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/custom-audience' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * CustomAudience
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CustomAudience extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get ClaimObjective(): Record<string, any>;
    static get ContentType(): Record<string, any>;
    static get CustomerFileSource(): Record<string, any>;
    static get Subtype(): Record<string, any>;
    deleteAdAccounts(params?: Record<string, any>): Promise<any>;
    getAdAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdAccount(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<CustomAudience>;
    getAds(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getSessions(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getSharedAccountInfo(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteUsers(params?: Record<string, any>): Promise<any>;
    createUser(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<CustomAudience>;
    createUsersReplace(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<CustomAudience>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): CustomAudience;
    update(fields: Array<string>, params?: Record<string, any>): CustomAudience;
  }
}
declare module 'objects/offline-conversion-data-set' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * OfflineConversionDataSet
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class OfflineConversionDataSet extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get PermittedRoles(): Record<string, any>;
    static get RelationshipType(): Record<string, any>;
    getAdAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdAccount(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<OfflineConversionDataSet>;
    getAgencies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAgency(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<OfflineConversionDataSet>;
    getAudiences(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCustomConversions(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createEvent(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    getStats(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getUploads(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createUpload(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    createValidate(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<OfflineConversionDataSet>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): OfflineConversionDataSet;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): OfflineConversionDataSet;
  }
}
declare module 'objects/profile-picture-source' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ProfilePictureSource
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProfilePictureSource extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Type(): Record<string, any>;
    static get BreakingChange(): Record<string, any>;
  }
}
declare module 'objects/profile' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * Profile
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Profile extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get ProfileType(): Record<string, any>;
    static get Type(): Record<string, any>;
    getPicture(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): Profile;
  }
}
declare module 'objects/comment' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * Comment
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Comment extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get CommentPrivacyValue(): Record<string, any>;
    static get Filter(): Record<string, any>;
    static get LiveFilter(): Record<string, any>;
    static get Order(): Record<string, any>;
    getComments(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createComment(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Comment>;
    deleteLikes(params?: Record<string, any>): Promise<any>;
    getLikes(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createLike(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Comment>;
    getReactions(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): Comment;
    update(fields: Array<string>, params?: Record<string, any>): Comment;
  }
}
declare module 'objects/rtb-dynamic-post' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * RTBDynamicPost
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class RTBDynamicPost extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getComments(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getLikes(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): RTBDynamicPost;
  }
}
declare module 'objects/insights-result' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * InsightsResult
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class InsightsResult extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get DatePreset(): Record<string, any>;
    static get Period(): Record<string, any>;
  }
}
declare module 'objects/post' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  import Comment from 'objects/comment';
  /**
   * Post
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Post extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get BackdatedTimeGranularity(): Record<string, any>;
    static get CheckinEntryPoint(): Record<string, any>;
    static get Formatting(): Record<string, any>;
    static get PlaceAttachmentSetting(): Record<string, any>;
    static get PostSurfacesBlacklist(): Record<string, any>;
    static get PostingToRedspace(): Record<string, any>;
    static get TargetSurface(): Record<string, any>;
    static get UnpublishedContentType(): Record<string, any>;
    static get FeedStoryVisibility(): Record<string, any>;
    static get TimelineVisibility(): Record<string, any>;
    getAttachments(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getComments(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createComment(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Comment>;
    getDynamicPosts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getInsights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteLikes(params?: Record<string, any>): Promise<any>;
    createLike(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Post>;
    getReactions(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getSharedPosts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getSponsorTags(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getTo(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): Post;
    update(fields: Array<string>, params?: Record<string, any>): Post;
  }
}
declare module 'objects/page-post' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  import Comment from 'objects/comment';
  /**
   * PagePost
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PagePost extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get With(): Record<string, any>;
    static get BackdatedTimeGranularity(): Record<string, any>;
    static get FeedStoryVisibility(): Record<string, any>;
    static get TimelineVisibility(): Record<string, any>;
    getAttachments(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getComments(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createComment(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Comment>;
    getDynamicPosts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getInsights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteLikes(params?: Record<string, any>): Promise<any>;
    getLikes(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createLike(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<PagePost>;
    getReactions(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getSharedPosts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getSponsorTags(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getTo(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): PagePost;
    update(fields: Array<string>, params?: Record<string, any>): PagePost;
  }
}
declare module 'objects/photo' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  import Comment from 'objects/comment';
  /**
   * Photo
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Photo extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get BackdatedTimeGranularity(): Record<string, any>;
    static get UnpublishedContentType(): Record<string, any>;
    static get Type(): Record<string, any>;
    getComments(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createComment(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Comment>;
    getInsights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getLikes(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createLike(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Photo>;
    getSponsorTags(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): Photo;
  }
}
declare module 'objects/album' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  import Comment from 'objects/comment';
  import Photo from 'objects/photo';
  /**
   * Album
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Album extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getComments(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createComment(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Comment>;
    getLikes(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createLike(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Album>;
    getPhotos(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createPhoto(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Photo>;
    getPicture(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): Album;
  }
}
declare module 'objects/page-call-to-action' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  /**
   * PageCallToAction
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PageCallToAction extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get AndroidDestinationType(): Record<string, any>;
    static get IphoneDestinationType(): Record<string, any>;
    static get Type(): Record<string, any>;
    static get WebDestinationType(): Record<string, any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): PageCallToAction;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): PageCallToAction;
  }
}
declare module 'objects/canvas-body-element' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CanvasBodyElement
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CanvasBodyElement extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/canvas' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  /**
   * Canvas
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Canvas extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    createDuplicateCanva(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Canvas>;
    createPreviewNotification(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Canvas>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): Canvas;
    update(fields: Array<string>, params?: Record<string, any>): Canvas;
  }
}
declare module 'objects/url' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * URL
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class URL extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Scopes(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): URL;
    update(fields: Array<string>, params?: Record<string, any>): URL;
  }
}
declare module 'objects/page-commerce-eligibility' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PageCommerceEligibility
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PageCommerceEligibility extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/automotive-model' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AutomotiveModel
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AutomotiveModel extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get BodyStyle(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): AutomotiveModel;
  }
}
declare module 'objects/product-catalog-category' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ProductCatalogCategory
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductCatalogCategory extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get CategorizationCriteria(): Record<string, any>;
  }
}
declare module 'objects/check-batch-request-status' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CheckBatchRequestStatus
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CheckBatchRequestStatus extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/collaborative-ads-share-settings' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CollaborativeAdsShareSettings
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CollaborativeAdsShareSettings extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): CollaborativeAdsShareSettings;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): CollaborativeAdsShareSettings;
  }
}
declare module 'objects/destination' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * Destination
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Destination extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): Destination;
  }
}
declare module 'objects/product-event-stat' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ProductEventStat
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductEventStat extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get DeviceType(): Record<string, any>;
    static get Event(): Record<string, any>;
    static get Breakdowns(): Record<string, any>;
  }
}
declare module 'objects/external-event-source' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ExternalEventSource
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ExternalEventSource extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/flight' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * Flight
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Flight extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): Flight;
    update(fields: Array<string>, params?: Record<string, any>): Flight;
  }
}
declare module 'objects/home-listing' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  /**
   * HomeListing
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class HomeListing extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): HomeListing;
    update(fields: Array<string>, params?: Record<string, any>): HomeListing;
  }
}
declare module 'objects/product-catalog-hotel-rooms-batch' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ProductCatalogHotelRoomsBatch
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductCatalogHotelRoomsBatch extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/dynamic-price-config-by-date' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * DynamicPriceConfigByDate
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class DynamicPriceConfigByDate extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): DynamicPriceConfigByDate;
  }
}
declare module 'objects/hotel-room' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * HotelRoom
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class HotelRoom extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getPricingVariables(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): HotelRoom;
    update(fields: Array<string>, params?: Record<string, any>): HotelRoom;
  }
}
declare module 'objects/hotel' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * Hotel
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Hotel extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getHotelRooms(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): Hotel;
    update(fields: Array<string>, params?: Record<string, any>): Hotel;
  }
}
declare module 'objects/product-catalog-pricing-letiables-batch' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ProductCatalogPricingVariablesBatch
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductCatalogPricingVariablesBatch extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/catalog-item-channels-to-integrity-status' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CatalogItemChannelsToIntegrityStatus
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CatalogItemChannelsToIntegrityStatus extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/vehicle-offer' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * VehicleOffer
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class VehicleOffer extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): VehicleOffer;
  }
}
declare module 'objects/vehicle' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * Vehicle
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Vehicle extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Availability(): Record<string, any>;
    static get BodyStyle(): Record<string, any>;
    static get Condition(): Record<string, any>;
    static get Drivetrain(): Record<string, any>;
    static get FuelType(): Record<string, any>;
    static get StateOfVehicle(): Record<string, any>;
    static get Transmission(): Record<string, any>;
    static get VehicleType(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): Vehicle;
    update(fields: Array<string>, params?: Record<string, any>): Vehicle;
  }
}
declare module 'objects/product-set' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * ProductSet
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductSet extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getAutomotiveModels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getDestinations(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getFlights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getHomeListings(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getHotels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getProducts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getVehicleOffers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getVehicles(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): ProductSet;
    update(fields: Array<string>, params?: Record<string, any>): ProductSet;
  }
}
declare module 'objects/product-item' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * ProductItem
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductItem extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get AgeGroup(): Record<string, any>;
    static get Availability(): Record<string, any>;
    static get Condition(): Record<string, any>;
    static get Gender(): Record<string, any>;
    static get ReviewStatus(): Record<string, any>;
    static get ShippingWeightUnit(): Record<string, any>;
    static get Visibility(): Record<string, any>;
    static get CommerceTaxCategory(): Record<string, any>;
    getChannelsToIntegrityStatus(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getProductSets(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): ProductItem;
    update(fields: Array<string>, params?: Record<string, any>): ProductItem;
  }
}
declare module 'objects/product-feed-rule' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  /**
   * ProductFeedRule
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductFeedRule extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get RuleType(): Record<string, any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): ProductFeedRule;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): ProductFeedRule;
  }
}
declare module 'objects/product-feed-schedule' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ProductFeedSchedule
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductFeedSchedule extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get DayOfWeek(): Record<string, any>;
    static get Interval(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): ProductFeedSchedule;
  }
}
declare module 'objects/product-feed-upload-error-sample' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ProductFeedUploadErrorSample
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductFeedUploadErrorSample extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): ProductFeedUploadErrorSample;
  }
}
declare module 'objects/product-feed-rule-suggestion' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ProductFeedRuleSuggestion
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductFeedRuleSuggestion extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/product-feed-upload-error' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * ProductFeedUploadError
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductFeedUploadError extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get AffectedSurfaces(): Record<string, any>;
    static get Severity(): Record<string, any>;
    getSamples(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getSuggestedRules(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): ProductFeedUploadError;
  }
}
declare module 'objects/product-feed-upload' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * ProductFeedUpload
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductFeedUpload extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get InputMethod(): Record<string, any>;
    createErrorReport(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProductFeedUpload>;
    getErrors(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): ProductFeedUpload;
  }
}
declare module 'objects/product-feed' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  import ProductFeedRule from 'objects/product-feed-rule';
  import ProductFeedUpload from 'objects/product-feed-upload';
  /**
   * ProductFeed
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductFeed extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Delimiter(): Record<string, any>;
    static get QuotedFieldsMode(): Record<string, any>;
    static get Encoding(): Record<string, any>;
    static get FeedType(): Record<string, any>;
    static get ItemSubType(): Record<string, any>;
    static get OverrideType(): Record<string, any>;
    getAutomotiveModels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getDestinations(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getFlights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getHomeListings(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getHotels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getProducts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getRules(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createRule(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProductFeedRule>;
    getUploadSchedules(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createUploadSchedule(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProductFeed>;
    getUploads(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createUpload(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProductFeedUpload>;
    getVehicleOffers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getVehicles(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): ProductFeed;
    update(fields: Array<string>, params?: Record<string, any>): ProductFeed;
  }
}
declare module 'objects/product-group' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  import ProductItem from 'objects/product-item';
  /**
   * ProductGroup
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductGroup extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getProducts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createProduct(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProductItem>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): ProductGroup;
    update(fields: Array<string>, params?: Record<string, any>): ProductGroup;
  }
}
declare module 'objects/product-catalog-product-sets-batch' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ProductCatalogProductSetsBatch
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductCatalogProductSetsBatch extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/product-catalog' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  import AutomotiveModel from 'objects/automotive-model';
  import ProductCatalogCategory from 'objects/product-catalog-category';
  import HomeListing from 'objects/home-listing';
  import Hotel from 'objects/hotel';
  import ProductFeed from 'objects/product-feed';
  import ProductGroup from 'objects/product-group';
  import ProductSet from 'objects/product-set';
  import ProductItem from 'objects/product-item';
  import Vehicle from 'objects/vehicle';
  /**
   * ProductCatalog
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductCatalog extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Vertical(): Record<string, any>;
    static get PermittedRoles(): Record<string, any>;
    static get PermittedTasks(): Record<string, any>;
    static get Tasks(): Record<string, any>;
    static get Standard(): Record<string, any>;
    static get ItemSubType(): Record<string, any>;
    deleteAgencies(params?: Record<string, any>): Promise<any>;
    getAgencies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAgency(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProductCatalog>;
    deleteAssignedUsers(params?: Record<string, any>): Promise<any>;
    getAssignedUsers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAssignedUser(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProductCatalog>;
    getAutomotiveModels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAutomotiveModel(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AutomotiveModel>;
    createBatch(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProductCatalog>;
    getCategories(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createCategory(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProductCatalogCategory>;
    getCheckBatchRequestStatus(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCollaborativeAdsShareSettings(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getDestinations(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getEventStats(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteExternalEventSources(params?: Record<string, any>): Promise<any>;
    getExternalEventSources(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createExternalEventSource(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProductCatalog>;
    getFlights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getHomeListings(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createHomeListing(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<HomeListing>;
    getHotelRoomsBatch(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createHotelRoomsBatch(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProductCatalog>;
    getHotels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createHotel(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Hotel>;
    createItemsBatch(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProductCatalog>;
    getPricingVariablesBatch(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createPricingVariablesBatch(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProductCatalog>;
    getProductFeeds(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createProductFeed(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProductFeed>;
    getProductGroups(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createProductGroup(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProductGroup>;
    getProductSets(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createProductSet(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProductSet>;
    getProductSetsBatch(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getProducts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createProduct(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProductItem>;
    getVehicleOffers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getVehicles(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createVehicle(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Vehicle>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): ProductCatalog;
    update(fields: Array<string>, params?: Record<string, any>): ProductCatalog;
  }
}
declare module 'objects/commerce-merchant-settings-setup-status' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CommerceMerchantSettingsSetupStatus
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CommerceMerchantSettingsSetupStatus extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/commerce-merchant-settings' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * CommerceMerchantSettings
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CommerceMerchantSettings extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getOrderManagementApps(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createOrderManagementApp(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<CommerceMerchantSettings>;
    getProductCatalogs(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getReturns(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getSetupStatus(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getShippingProfiles(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createShippingProfile(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    getTaxSettings(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createWhatsappChannel(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): CommerceMerchantSettings;
  }
}
declare module 'objects/commerce-order' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * CommerceOrder
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CommerceOrder extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get ReasonCode(): Record<string, any>;
    static get Filters(): Record<string, any>;
    static get State(): Record<string, any>;
    createAcknowledgeOrder(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<CommerceOrder>;
    getCancellations(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createCancellation(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<CommerceOrder>;
    getItems(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPayments(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPromotionDetails(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPromotions(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getRefunds(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createRefund(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<CommerceOrder>;
    getReturns(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getShipments(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createShipment(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<CommerceOrder>;
    createUpdateShipment(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<CommerceOrder>;
    get(fields: Array<string>, params?: Record<string, any>): CommerceOrder;
  }
}
declare module 'objects/commerce-payout' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CommercePayout
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CommercePayout extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/commerce-order-transaction-detail' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * CommerceOrderTransactionDetail
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CommerceOrderTransactionDetail extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getItems(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getTaxDetails(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
  }
}
declare module 'objects/unified-thread' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * UnifiedThread
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class UnifiedThread extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getMessages(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): UnifiedThread;
  }
}
declare module 'objects/page-user-message-thread-label' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  /**
   * PageUserMessageThreadLabel
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PageUserMessageThreadLabel extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    deleteLabel(params?: Record<string, any>): Promise<any>;
    createLabel(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<PageUserMessageThreadLabel>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): PageUserMessageThreadLabel;
  }
}
declare module 'objects/custom-user-settings' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CustomUserSettings
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CustomUserSettings extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/null-node' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * NullNode
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class NullNode extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/app-request-former-recipient' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AppRequestFormerRecipient
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AppRequestFormerRecipient extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/app-request' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  /**
   * AppRequest
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AppRequest extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): AppRequest;
  }
}
declare module 'objects/business-user' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * BusinessUser
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class BusinessUser extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Role(): Record<string, any>;
    getAssignedAdAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAssignedBusinessAssetGroups(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAssignedPages(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAssignedProductCatalogs(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): BusinessUser;
    update(fields: Array<string>, params?: Record<string, any>): BusinessUser;
  }
}
declare module 'objects/video-thumbnail' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * VideoThumbnail
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class VideoThumbnail extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'video-uploader' {
  import AdVideo from 'objects/ad-video';
  import FacebookAdsApi from 'api';
  /**
   * Video uploader that can upload videos to adaccount
   **/
  class VideoUploader {
    _session: VideoUploadSession | null | undefined;
    constructor();
    /**
     * Upload the given video file.
     * @param {AdVideo} video The AdVideo object that will be uploaded
     * @param {Boolean} [waitForEncoding] Whether to wait until encoding
     *   is finished
     **/
    upload(video: AdVideo, waitForEncoding: boolean): Record<string, any>;
  }
  type SlideshowSpec = {
    images_urls: Array<string>;
    duration_ms: number;
    transition_ms: number;
  };
  class VideoUploadSession {
    _accountId: string;
    _api: FacebookAdsApi;
    _endOffset: number;
    _filePath: string | null | undefined;
    _sessionId: string;
    _slideshowSpec: SlideshowSpec | null | undefined;
    _startOffset: number;
    _startRequestManager: VideoUploadStartRequestManager;
    _transferRequestManager: VideoUploadTransferRequestManager;
    _finishRequestManager: VideoUploadFinishRequestManager;
    _video: AdVideo;
    _waitForEncoding: boolean;
    constructor(video: AdVideo, waitForEncoding?: boolean);
    start(): Record<string, any>;
    getStartRequestContext(): VideoUploadRequestContext;
    getTransferRequestContext(): VideoUploadRequestContext;
    getFinishRequestContext(): VideoUploadRequestContext;
  }
  /**
   * Abstract class for request managers
   **/
  class VideoUploadRequestManager {
    _api: FacebookAdsApi;
    constructor(api: FacebookAdsApi);
    sendRequest(context: VideoUploadRequestContext): Record<string, any>;
    getParamsFromContext(
      context: VideoUploadRequestContext,
    ): Record<string, any>;
  }
  class VideoUploadStartRequestManager extends VideoUploadRequestManager {
    /**
     * Send start request with the given context
     **/
    sendRequest(context: VideoUploadRequestContext): Record<string, any>;
    getParamsFromContext(
      context: VideoUploadRequestContext,
    ): Record<string, any>;
  }
  class VideoUploadTransferRequestManager extends VideoUploadRequestManager {
    _startOffset: number;
    _endOffset: number;
    /**
     * Send transfer request with the given context
     **/
    sendRequest(context: VideoUploadRequestContext): Record<string, any>;
  }
  class VideoUploadFinishRequestManager extends VideoUploadRequestManager {
    /**
     * Send transfer request with the given context
     **/
    sendRequest(context: VideoUploadRequestContext): Record<string, any>;
    getParamsFromContext(
      context: VideoUploadRequestContext,
    ): Record<string, any>;
  }
  /**
   * Upload request context that contains the param data
   **/
  class VideoUploadRequestContext {
    _accountId: string;
    _fileName: string;
    _filePath: string;
    _fileSize: number;
    _name: string;
    _sessionId: string;
    _startOffset: number;
    _endOffset: number;
    _slideshowSpec: SlideshowSpec;
    _videoFileChunk: string;
    get accountId(): string;
    set accountId(accountId: string);
    get fileName(): string;
    set fileName(fileName: string);
    get filePath(): string;
    set filePath(filePath: string);
    get fileSize(): number;
    set fileSize(fileSize: number);
    get name(): string;
    set name(name: string);
    get sessionId(): string;
    set sessionId(sessionId: string);
    get startOffset(): number;
    set startOffset(startOffset: number);
    get endOffset(): number;
    set endOffset(endOffset: number);
    get slideshowSpec(): SlideshowSpec;
    set slideshowSpec(slideshowSpec: SlideshowSpec);
    get videoFileChunk(): string;
    set videoFileChunk(videoFileChunk: string);
  }
  class VideoUploadRequest {
    _params: Record<string, any>;
    _files: Record<string, any>;
    _api: FacebookAdsApi;
    constructor(api: FacebookAdsApi);
    /**
     * Send the current request
     **/
    send(path: string | Array<string>): Record<string, any>;
    setParams(params: Record<string, any>, files?: Record<string, any>): void;
  }
  class VideoEncodingStatusChecker {
    static waitUntilReady(
      api: FacebookAdsApi,
      videoId: number,
      interval: number,
      timeout: number,
    ): Promise<void>;
    static getStatus(api: FacebookAdsApi, videoId: number): any;
  }
  export { VideoUploader, VideoUploadRequest, VideoEncodingStatusChecker };
  export type { SlideshowSpec };
}
declare module 'objects/ad-video' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  import FacebookAdsBatchApi from 'api-batch';
  import type { SlideshowSpec } from 'video-uploader';
  /**
   * AdVideo
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdVideo extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get filepath(): string;
    get slideshow_spec(): SlideshowSpec | null | undefined;
    /**
     * Uploads filepath and creates the AdVideo object from it.
     * It requires 'filepath' property to be defined.
     **/
    create(
      batch: FacebookAdsBatchApi,
      failureHandler: (...args: Array<any>) => any,
      successHandler: (...args: Array<any>) => any,
    ): any;
    waitUntilEncodingReady(interval?: number, timeout?: number): void;
    /**
     *  Returns all the thumbnails associated with the ad video
     */
    getThumbnails(
      fields: Record<string, any>,
      params: Record<string, any>,
    ): Cursor | Promise<any>;
  }
}
declare module 'objects/group' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  import Album from 'objects/album';
  import Post from 'objects/post';
  import LiveVideo from 'objects/live-video';
  import Photo from 'objects/photo';
  import AdVideo from 'objects/ad-video';
  /**
   * Group
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Group extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get JoinSetting(): Record<string, any>;
    static get PostPermissions(): Record<string, any>;
    static get Purpose(): Record<string, any>;
    static get GroupType(): Record<string, any>;
    deleteAdmins(params?: Record<string, any>): Promise<any>;
    createAdmin(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Group>;
    getAlbums(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAlbum(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Album>;
    getDocs(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getEvents(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getFeed(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createFeed(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Post>;
    getFiles(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getGroups(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createGroup(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Group>;
    getLiveVideos(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createLiveVideo(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<LiveVideo>;
    deleteMembers(params?: Record<string, any>): Promise<any>;
    createMember(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Group>;
    getOptedInMembers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createPhoto(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Photo>;
    getPicture(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getVideos(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createVideo(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdVideo>;
    get(fields: Array<string>, params?: Record<string, any>): Group;
    update(fields: Array<string>, params?: Record<string, any>): Group;
  }
}
declare module 'objects/user-id-for-app' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * UserIDForApp
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class UserIDForApp extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/user-id-for-page' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * UserIDForPage
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class UserIDForPage extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/live-encoder' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  /**
   * LiveEncoder
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class LiveEncoder extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get CapAudioCodecs(): Record<string, any>;
    static get CapStreamingProtocols(): Record<string, any>;
    static get CapVideoCodecs(): Record<string, any>;
    static get Status(): Record<string, any>;
    createTelemetry(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<LiveEncoder>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): LiveEncoder;
    update(fields: Array<string>, params?: Record<string, any>): LiveEncoder;
  }
}
declare module 'objects/payment-engine-payment' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PaymentEnginePayment
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PaymentEnginePayment extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Reason(): Record<string, any>;
    createDispute(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<PaymentEnginePayment>;
    createRefund(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<PaymentEnginePayment>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): PaymentEnginePayment;
  }
}
declare module 'objects/permission' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * Permission
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Permission extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Status(): Record<string, any>;
  }
}
declare module 'objects/user' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  import Page from 'objects/page';
  import AdStudy from 'objects/ad-study';
  import Business from 'objects/business';
  import Post from 'objects/post';
  import LiveEncoder from 'objects/live-encoder';
  import LiveVideo from 'objects/live-video';
  import Photo from 'objects/photo';
  import AdVideo from 'objects/ad-video';
  /**
   * User
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class User extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get LocalNewsMegaphoneDismissStatus(): Record<string, any>;
    static get LocalNewsSubscriptionStatus(): Record<string, any>;
    static get Filtering(): Record<string, any>;
    static get Type(): Record<string, any>;
    deleteAccessTokens(params?: Record<string, any>): Promise<any>;
    createAccessToken(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<User>;
    getAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAccount(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    getAdStudies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdStudy(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdStudy>;
    getAdAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAlbums(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createApplication(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<User>;
    getAppRequestFormerRecipients(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAppRequests(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAssignedAdAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAssignedBusinessAssetGroups(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAssignedPages(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAssignedProductCatalogs(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getBusinessUsers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteBusinesses(params?: Record<string, any>): Promise<any>;
    getBusinesses(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createBusiness(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Business>;
    getConversations(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCustomLabels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getEvents(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getFeed(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createFeed(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Post>;
    getFriends(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createGameItem(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    createGameTime(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    createGamesPlay(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    getGroups(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getIdsForApps(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getIdsForBusiness(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getIdsForPages(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getLikes(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getLiveEncoders(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createLiveEncoder(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<LiveEncoder>;
    getLiveVideos(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createLiveVideo(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<LiveVideo>;
    getMusic(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createNotification(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<User>;
    getPaymentTransactions(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deletePermissions(params?: Record<string, any>): Promise<any>;
    getPermissions(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPersonalAdAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPhotos(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createPhoto(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Photo>;
    getPicture(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPosts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getRichMediaDocuments(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createStagingResource(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<User>;
    getVideos(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createVideo(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdVideo>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): User;
    update(fields: Array<string>, params?: Record<string, any>): User;
  }
}
declare module 'objects/live-video-error' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * LiveVideoError
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class LiveVideoError extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): LiveVideoError;
  }
}
declare module 'objects/live-video-input-stream' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * LiveVideoInputStream
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class LiveVideoInputStream extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): LiveVideoInputStream;
  }
}
declare module 'objects/video-poll' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * VideoPoll
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class VideoPoll extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Status(): Record<string, any>;
    static get Action(): Record<string, any>;
    getPollOptions(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): VideoPoll;
    update(fields: Array<string>, params?: Record<string, any>): VideoPoll;
  }
}
declare module 'objects/live-video' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  import LiveVideoInputStream from 'objects/live-video-input-stream';
  import VideoPoll from 'objects/video-poll';
  /**
   * LiveVideo
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class LiveVideo extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Projection(): Record<string, any>;
    static get SpatialAudioFormat(): Record<string, any>;
    static get Status(): Record<string, any>;
    static get StereoscopicMode(): Record<string, any>;
    static get StreamType(): Record<string, any>;
    static get BroadcastStatus(): Record<string, any>;
    static get Source(): Record<string, any>;
    static get LiveCommentModerationSetting(): Record<string, any>;
    getBlockedUsers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getComments(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCrosspostSharedPages(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCrosspostedBroadcasts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getErrors(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createInputStream(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<LiveVideoInputStream>;
    getPolls(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createPoll(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<VideoPoll>;
    getReactions(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): LiveVideo;
    update(fields: Array<string>, params?: Record<string, any>): LiveVideo;
  }
}
declare module 'objects/event' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  import LiveVideo from 'objects/live-video';
  /**
   * Event
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Event extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Category(): Record<string, any>;
    static get OnlineEventFormat(): Record<string, any>;
    static get Type(): Record<string, any>;
    static get EventStateFilter(): Record<string, any>;
    static get TimeFilter(): Record<string, any>;
    getComments(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getFeed(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getLiveVideos(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createLiveVideo(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<LiveVideo>;
    getPhotos(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPicture(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPosts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getRoles(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getVideos(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): Event;
  }
}
declare module 'objects/image-copyright' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  /**
   * ImageCopyright
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ImageCopyright extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get GeoOwnership(): Record<string, any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): ImageCopyright;
    update(fields: Array<string>, params?: Record<string, any>): ImageCopyright;
  }
}
declare module 'objects/instant-article-insights-query-result' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * InstantArticleInsightsQueryResult
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class InstantArticleInsightsQueryResult extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Breakdown(): Record<string, any>;
    static get Period(): Record<string, any>;
  }
}
declare module 'objects/instant-article' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * InstantArticle
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class InstantArticle extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getInsights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): InstantArticle;
  }
}
declare module 'objects/leadgen-form' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  import Lead from 'objects/lead';
  /**
   * LeadgenForm
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class LeadgenForm extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Status(): Record<string, any>;
    static get Locale(): Record<string, any>;
    getLeads(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getTestLeads(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createTestLead(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Lead>;
    get(fields: Array<string>, params?: Record<string, any>): LeadgenForm;
    update(fields: Array<string>, params?: Record<string, any>): LeadgenForm;
  }
}
declare module 'objects/media-fingerprint' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  /**
   * MediaFingerprint
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class MediaFingerprint extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get FingerprintContentType(): Record<string, any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): MediaFingerprint;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): MediaFingerprint;
  }
}
declare module 'objects/messaging-feature-review' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * MessagingFeatureReview
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class MessagingFeatureReview extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/messenger-profile' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * MessengerProfile
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class MessengerProfile extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/native-offer-view' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * NativeOfferView
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class NativeOfferView extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    createPhoto(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<NativeOfferView>;
    createVideo(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<NativeOfferView>;
    get(fields: Array<string>, params?: Record<string, any>): NativeOfferView;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): NativeOfferView;
  }
}
declare module 'objects/native-offer' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * NativeOffer
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class NativeOffer extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get UniqueCodesFileCodeType(): Record<string, any>;
    static get BarcodeType(): Record<string, any>;
    static get LocationType(): Record<string, any>;
    createCode(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<NativeOffer>;
    createNativeOfferView(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<NativeOffer>;
    getViews(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): NativeOffer;
  }
}
declare module 'objects/persona' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  /**
   * Persona
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Persona extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): Persona;
  }
}
declare module 'objects/recommendation' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * Recommendation
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Recommendation extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/page-settings' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PageSettings
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PageSettings extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/tab' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * Tab
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Tab extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/page-thread-owner' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PageThreadOwner
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PageThreadOwner extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/video-copyright-rule' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * VideoCopyrightRule
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class VideoCopyrightRule extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Source(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): VideoCopyrightRule;
  }
}
declare module 'objects/video-copyright' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * VideoCopyright
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class VideoCopyright extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get ContentCategory(): Record<string, any>;
    static get MonitoringType(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): VideoCopyright;
    update(fields: Array<string>, params?: Record<string, any>): VideoCopyright;
  }
}
declare module 'objects/video-list' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * VideoList
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class VideoList extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getVideos(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): VideoList;
  }
}
declare module 'objects/page' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  import CanvasBodyElement from 'objects/canvas-body-element';
  import Canvas from 'objects/canvas';
  import PageUserMessageThreadLabel from 'objects/page-user-message-thread-label';
  import ImageCopyright from 'objects/image-copyright';
  import AdVideo from 'objects/ad-video';
  import InstagramUser from 'objects/instagram-user';
  import InstantArticle from 'objects/instant-article';
  import LeadgenForm from 'objects/leadgen-form';
  import LiveEncoder from 'objects/live-encoder';
  import LiveVideo from 'objects/live-video';
  import MediaFingerprint from 'objects/media-fingerprint';
  import NativeOffer from 'objects/native-offer';
  import Persona from 'objects/persona';
  import Photo from 'objects/photo';
  import ProfilePictureSource from 'objects/profile-picture-source';
  import VideoCopyrightRule from 'objects/video-copyright-rule';
  import VideoCopyright from 'objects/video-copyright';
  /**
   * Page
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Page extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Attire(): Record<string, any>;
    static get FoodStyles(): Record<string, any>;
    static get PickupOptions(): Record<string, any>;
    static get TemporaryStatus(): Record<string, any>;
    static get PermittedTasks(): Record<string, any>;
    static get Tasks(): Record<string, any>;
    static get BackdatedTimeGranularity(): Record<string, any>;
    static get CheckinEntryPoint(): Record<string, any>;
    static get Formatting(): Record<string, any>;
    static get PlaceAttachmentSetting(): Record<string, any>;
    static get PostSurfacesBlacklist(): Record<string, any>;
    static get PostingToRedspace(): Record<string, any>;
    static get TargetSurface(): Record<string, any>;
    static get UnpublishedContentType(): Record<string, any>;
    static get PublishStatus(): Record<string, any>;
    static get MessagingType(): Record<string, any>;
    static get NotificationType(): Record<string, any>;
    static get SenderAction(): Record<string, any>;
    static get Model(): Record<string, any>;
    static get SubscribedFields(): Record<string, any>;
    createAcknowledgeOrder(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    getAdsPosts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteAgencies(params?: Record<string, any>): Promise<any>;
    getAgencies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAgency(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    getAlbums(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteAssignedUsers(params?: Record<string, any>): Promise<any>;
    getAssignedUsers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAssignedUser(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    deleteBlocked(params?: Record<string, any>): Promise<any>;
    getBlocked(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createBlocked(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    deleteBusinessData(params?: Record<string, any>): Promise<any>;
    createBusinessDatum(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    getCallToActions(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCanvasElements(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createCanvasElement(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<CanvasBodyElement>;
    getCanvases(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createCanvase(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Canvas>;
    getClaimedUrls(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCommerceEligibility(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCommerceMerchantSettings(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCommerceOrders(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCommercePayouts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCommerceTransactions(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getConversations(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createCopyrightManualClaim(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    getCopyrightWhitelistedPartners(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCrosspostWhitelistedPages(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCustomLabels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createCustomLabel(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<PageUserMessageThreadLabel>;
    deleteCustomUserSettings(params?: Record<string, any>): Promise<any>;
    getCustomUserSettings(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createCustomUserSetting(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    getEvents(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createExtendThreadControl(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    getFeed(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createFeed(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    getGlobalBrandChildren(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getImageCopyrights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createImageCopyright(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ImageCopyright>;
    getIndexedVideos(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getInsights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getInstagramAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getInstantArticles(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createInstantArticle(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<InstantArticle>;
    getInstantArticlesInsights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createInstantArticlesPublish(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    getLeadGenForms(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createLeadGenForm(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<LeadgenForm>;
    getLikes(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getLiveEncoders(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createLiveEncoder(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<LiveEncoder>;
    getLiveVideos(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createLiveVideo(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<LiveVideo>;
    deleteLocations(params?: Record<string, any>): Promise<any>;
    getLocations(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createLocation(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    getMediaFingerprints(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createMediaFingerprint(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<MediaFingerprint>;
    createMessageAttachment(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    createMessage(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    getMessagingFeatureReview(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteMessengerProfile(params?: Record<string, any>): Promise<any>;
    getMessengerProfile(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createMessengerProfile(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    getNativeOffers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createNativeOffer(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<NativeOffer>;
    createNlpConfig(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    getPageBackedInstagramAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createPageBackedInstagramAccount(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<InstagramUser>;
    createPageWhatsappNumberVerification(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    createPassThreadControl(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    createPassThreadMetadatum(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    getPersonas(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createPersona(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Persona>;
    getPhotos(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createPhoto(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Photo>;
    getPicture(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createPicture(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProfilePictureSource>;
    getPosts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getProductCatalogs(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPublishedPosts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getRatings(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createReleaseThreadControl(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    createRequestThreadControl(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    getRoles(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getRtbDynamicPosts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getScheduledPosts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getSecondaryReceivers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getSettings(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createSetting(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    getShopSetupStatus(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteSubscribedApps(params?: Record<string, any>): Promise<any>;
    getSubscribedApps(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createSubscribedApp(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    deleteTabs(params?: Record<string, any>): Promise<any>;
    getTabs(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createTab(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    getTagged(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createTakeThreadControl(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    getThreadOwner(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getThreads(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getTours(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createUnlinkAccount(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Page>;
    getVideoCopyrightRules(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createVideoCopyrightRule(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<VideoCopyrightRule>;
    createVideoCopyright(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<VideoCopyright>;
    getVideoLists(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getVideos(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createVideo(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdVideo>;
    getVisitorPosts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): Page;
    update(fields: Array<string>, params?: Record<string, any>): Page;
  }
}
declare module 'objects/business-asset-group' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * BusinessAssetGroup
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class BusinessAssetGroup extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get AdaccountTasks(): Record<string, any>;
    static get OfflineConversionDataSetTasks(): Record<string, any>;
    static get PageTasks(): Record<string, any>;
    static get PixelTasks(): Record<string, any>;
    deleteAssignedUsers(params?: Record<string, any>): Promise<any>;
    getAssignedUsers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAssignedUser(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<BusinessAssetGroup>;
    deleteContainedAdAccounts(params?: Record<string, any>): Promise<any>;
    getContainedAdAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createContainedAdAccount(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<BusinessAssetGroup>;
    deleteContainedApplications(params?: Record<string, any>): Promise<any>;
    getContainedApplications(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createContainedApplication(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<BusinessAssetGroup>;
    deleteContainedCustomConversions(
      params?: Record<string, any>,
    ): Promise<any>;
    getContainedCustomConversions(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createContainedCustomConversion(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<BusinessAssetGroup>;
    deleteContainedInstagramAccounts(
      params?: Record<string, any>,
    ): Promise<any>;
    getContainedInstagramAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createContainedInstagramAccount(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<BusinessAssetGroup>;
    deleteContainedOfflineConversionDataSets(
      params?: Record<string, any>,
    ): Promise<any>;
    getContainedOfflineConversionDataSets(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createContainedOfflineConversionDataSet(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<BusinessAssetGroup>;
    deleteContainedPages(params?: Record<string, any>): Promise<any>;
    getContainedPages(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createContainedPage(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<BusinessAssetGroup>;
    deleteContainedPixels(params?: Record<string, any>): Promise<any>;
    getContainedPixels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createContainedPixel(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<BusinessAssetGroup>;
    deleteContainedProductCatalogs(params?: Record<string, any>): Promise<any>;
    getContainedProductCatalogs(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createContainedProductCatalog(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<BusinessAssetGroup>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): BusinessAssetGroup;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): BusinessAssetGroup;
  }
}
declare module 'objects/invoice-campaign' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * InvoiceCampaign
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class InvoiceCampaign extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/oracle-transaction' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * OracleTransaction
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class OracleTransaction extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Type(): Record<string, any>;
    getCampaigns(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): OracleTransaction;
  }
}
declare module 'objects/atlas-campaign' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * AtlasCampaign
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AtlasCampaign extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getAdSets(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getBusinessUnit(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getMetricsBreakdown(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getSources(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): AtlasCampaign;
  }
}
declare module 'objects/business-unit' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * BusinessUnit
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class BusinessUnit extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getAdAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAdPlatforms(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAtlasSalesAccesses(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAtlasSalesAccess(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    getCampaigns(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getConversionEvents(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getConversionPaths(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCustomBreakdowns(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getDiagnostics(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getExternalImportFile(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getReports(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getSources(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getUsers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): BusinessUnit;
  }
}
declare module 'objects/whats-app-business-account' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * WhatsAppBusinessAccount
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class WhatsAppBusinessAccount extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Tasks(): Record<string, any>;
    static get Category(): Record<string, any>;
    deleteAssignedUsers(params?: Record<string, any>): Promise<any>;
    getAssignedUsers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAssignedUser(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<WhatsAppBusinessAccount>;
    deleteMessageTemplates(params?: Record<string, any>): Promise<any>;
    getMessageTemplates(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createMessageTemplate(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<WhatsAppBusinessAccount>;
    getPhoneNumbers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteSubscribedApps(params?: Record<string, any>): Promise<any>;
    getSubscribedApps(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createSubscribedApp(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<WhatsAppBusinessAccount>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): WhatsAppBusinessAccount;
  }
}
declare module 'objects/cpas-collaboration-request' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CPASCollaborationRequest
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CPASCollaborationRequest extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get RequesterAgencyOrBrand(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): CPASCollaborationRequest;
  }
}
declare module 'objects/cpas-advertiser-partnership-recommendation' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CPASAdvertiserPartnershipRecommendation
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CPASAdvertiserPartnershipRecommendation extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): CPASAdvertiserPartnershipRecommendation;
  }
}
declare module 'objects/event-source-group' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * EventSourceGroup
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class EventSourceGroup extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getSharedAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createSharedAccount(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<EventSourceGroup>;
    get(fields: Array<string>, params?: Record<string, any>): EventSourceGroup;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): EventSourceGroup;
  }
}
declare module 'objects/extended-credit-invoice-group' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  import AdAccount from 'objects/ad-account';
  /**
   * ExtendedCreditInvoiceGroup
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ExtendedCreditInvoiceGroup extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    deleteAdAccounts(params?: Record<string, any>): Promise<any>;
    getAdAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdAccount(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdAccount>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): ExtendedCreditInvoiceGroup;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): ExtendedCreditInvoiceGroup;
  }
}
declare module 'objects/extended-credit-allocation-config' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  /**
   * ExtendedCreditAllocationConfig
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ExtendedCreditAllocationConfig extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get LiabilityType(): Record<string, any>;
    static get PartitionType(): Record<string, any>;
    static get SendBillTo(): Record<string, any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): ExtendedCreditAllocationConfig;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): ExtendedCreditAllocationConfig;
  }
}
declare module 'objects/extended-credit' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  import ExtendedCreditInvoiceGroup from 'objects/extended-credit-invoice-group';
  import ExtendedCreditAllocationConfig from 'objects/extended-credit-allocation-config';
  /**
   * ExtendedCredit
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ExtendedCredit extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getExtendedCreditInvoiceGroups(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createExtendedCreditInvoiceGroup(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ExtendedCreditInvoiceGroup>;
    getOwningCreditAllocationConfigs(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createOwningCreditAllocationConfig(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ExtendedCreditAllocationConfig>;
    createWhatsappCreditSharingAndAttach(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    get(fields: Array<string>, params?: Record<string, any>): ExtendedCredit;
  }
}
declare module 'objects/business-asset-sharing-agreement' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * BusinessAssetSharingAgreement
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class BusinessAssetSharingAgreement extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get RequestStatus(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): BusinessAssetSharingAgreement;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): BusinessAssetSharingAgreement;
  }
}
declare module 'objects/business-agreement' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * BusinessAgreement
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class BusinessAgreement extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get RequestStatus(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): BusinessAgreement;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): BusinessAgreement;
  }
}
declare module 'objects/instagram-insights-result' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * InstagramInsightsResult
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class InstagramInsightsResult extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Metric(): Record<string, any>;
    static get Period(): Record<string, any>;
  }
}
declare module 'objects/ig-comment' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * IGComment
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class IGComment extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getReplies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createReply(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<IGComment>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): IGComment;
    update(fields: Array<string>, params?: Record<string, any>): IGComment;
  }
}
declare module 'objects/ig-media' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  import IGComment from 'objects/ig-comment';
  /**
   * IGMedia
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class IGMedia extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getChildren(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getComments(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createComment(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<IGComment>;
    getInsights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): IGMedia;
    update(fields: Array<string>, params?: Record<string, any>): IGMedia;
  }
}
declare module 'objects/ig-user' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  import IGMedia from 'objects/ig-media';
  /**
   * IGUser
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class IGUser extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getContentPublishingLimit(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getInsights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getMedia(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createMedia(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<IGMedia>;
    createMediaPublish(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<IGMedia>;
    createMention(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    getRecentlySearchedHashtags(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getStories(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getTags(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): IGUser;
  }
}
declare module 'objects/business-ad-account-request' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * BusinessAdAccountRequest
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class BusinessAdAccountRequest extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/business-application-request' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * BusinessApplicationRequest
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class BusinessApplicationRequest extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/business-page-request' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * BusinessPageRequest
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class BusinessPageRequest extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/business-role-request' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  /**
   * BusinessRoleRequest
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class BusinessRoleRequest extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Role(): Record<string, any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): BusinessRoleRequest;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): BusinessRoleRequest;
  }
}
declare module 'objects/system-user' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * SystemUser
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class SystemUser extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Role(): Record<string, any>;
    getAssignedAdAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAssignedBusinessAssetGroups(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAssignedPages(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAssignedProductCatalogs(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): SystemUser;
  }
}
declare module 'objects/third-party-measurement-report-dataset' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ThirdPartyMeasurementReportDataset
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ThirdPartyMeasurementReportDataset extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): ThirdPartyMeasurementReportDataset;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): ThirdPartyMeasurementReportDataset;
  }
}
declare module 'objects/measurement-upload-event' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * MeasurementUploadEvent
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class MeasurementUploadEvent extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get AggregationLevel(): Record<string, any>;
    static get EventStatus(): Record<string, any>;
    static get LookbackWindow(): Record<string, any>;
    static get MatchUniverse(): Record<string, any>;
    static get Timezone(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): MeasurementUploadEvent;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): MeasurementUploadEvent;
  }
}
declare module 'objects/business' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  import AdStudy from 'objects/ad-study';
  import AdAccount from 'objects/ad-account';
  import AdsPixel from 'objects/ads-pixel';
  import BusinessUnit from 'objects/business-unit';
  import BusinessUser from 'objects/business-user';
  import CustomConversion from 'objects/custom-conversion';
  import ProductCatalog from 'objects/product-catalog';
  import CPASCollaborationRequest from 'objects/cpas-collaboration-request';
  import EventSourceGroup from 'objects/event-source-group';
  import OfflineConversionDataSet from 'objects/offline-conversion-data-set';
  import SystemUser from 'objects/system-user';
  import MeasurementUploadEvent from 'objects/measurement-upload-event';
  /**
   * Business
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Business extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get TwoFactorType(): Record<string, any>;
    static get Vertical(): Record<string, any>;
    static get PermittedTasks(): Record<string, any>;
    static get SurveyBusinessType(): Record<string, any>;
    static get PagePermittedTasks(): Record<string, any>;
    createAccessToken(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Business>;
    deleteAdAccounts(params?: Record<string, any>): Promise<any>;
    getAdStudies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdStudy(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdStudy>;
    createAdAccount(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdAccount>;
    getAdNetworkAnalytics(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdNetworkAnalytic(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Business>;
    getAdNetworkAnalyticsResults(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAdsPixels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdsPixel(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdsPixel>;
    deleteAgencies(params?: Record<string, any>): Promise<any>;
    getAgencies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAggregateRevenue(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    getAnPlacements(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createBlockListDraft(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Business>;
    getBusinessAssetGroups(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getBusinessInvoices(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getBusinessUnits(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createBusinessUnit(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<BusinessUnit>;
    getBusinessUsers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createBusinessUser(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<BusinessUser>;
    createClaimCustomConversion(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<CustomConversion>;
    getClientAdAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createClientAdAccount(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Business>;
    getClientApps(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createClientApp(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Business>;
    getClientPages(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createClientPage(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Business>;
    getClientPixels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getClientProductCatalogs(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getClientWhatsAppBusinessAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteClients(params?: Record<string, any>): Promise<any>;
    getClients(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCollaborativeAdsCollaborationRequests(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createCollaborativeAdsCollaborationRequest(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<CPASCollaborationRequest>;
    getCollaborativeAdsSuggestedPartners(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCommerceMerchantSettings(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getContentDeliveryReport(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createCreateAndApplyPublisherBlockList(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    createCustomConversion(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<CustomConversion>;
    getEventSourceGroups(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createEventSourceGroup(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<EventSourceGroup>;
    getExtendedCredits(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createFranchiseProgram(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    getInitiatedAudienceSharingRequests(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getInitiatedSharingAgreements(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteInstagramAccounts(params?: Record<string, any>): Promise<any>;
    getInstagramAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getInstagramBusinessAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteManagedBusinesses(params?: Record<string, any>): Promise<any>;
    createManagedBusiness(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Business>;
    createMoveAsset(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Business>;
    getOfflineConversionDataSets(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createOfflineConversionDataSet(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<OfflineConversionDataSet>;
    getOwnedAdAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createOwnedAdAccount(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Business>;
    getOwnedApps(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createOwnedApp(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Business>;
    deleteOwnedBusinesses(params?: Record<string, any>): Promise<any>;
    getOwnedBusinesses(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createOwnedBusiness(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Business>;
    getOwnedInstagramAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getOwnedPages(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createOwnedPage(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Business>;
    getOwnedPixels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getOwnedProductCatalogs(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createOwnedProductCatalog(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ProductCatalog>;
    getOwnedWhatsAppBusinessAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deletePages(params?: Record<string, any>): Promise<any>;
    getPendingClientAdAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPendingClientApps(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPendingClientPages(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPendingOwnedAdAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPendingOwnedPages(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPendingUsers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPicture(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createPixelTo(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    getReceivedAudienceSharingRequests(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getReceivedSharingAgreements(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getSystemUsers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createSystemUser(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<SystemUser>;
    getThirdPartyMeasurementReportDataset(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createUploadEvent(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<MeasurementUploadEvent>;
    get(fields: Array<string>, params?: Record<string, any>): Business;
    update(fields: Array<string>, params?: Record<string, any>): Business;
  }
}
declare module 'objects/application' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * Application
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Application extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get SupportedPlatforms(): Record<string, any>;
    static get AnPlatforms(): Record<string, any>;
    static get Platform(): Record<string, any>;
    static get RequestType(): Record<string, any>;
    static get MutationMethod(): Record<string, any>;
    static get PostMethod(): Record<string, any>;
    static get ScoreType(): Record<string, any>;
    static get SortOrder(): Record<string, any>;
    static get LoggingSource(): Record<string, any>;
    static get LoggingTarget(): Record<string, any>;
    deleteAccounts(params?: Record<string, any>): Promise<any>;
    getAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAccount(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    createActivity(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    getAdNetworkAnalytics(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdNetworkAnalytic(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Application>;
    getAdNetworkAnalyticsResults(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAgencies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAggregateRevenue(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    getAndroidDialogConfigs(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAppEventTypes(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAppIndexing(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Application>;
    createAppIndexingSession(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Application>;
    getAppInsights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAppInstalledGroups(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAppPushDeviceToken(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Application>;
    getAppAssets(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAsset(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Application>;
    getAuthorizedAdAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteBanned(params?: Record<string, any>): Promise<any>;
    getButtonAutoDetectionDeviceSelection(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createButtonIndexing(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Application>;
    createCodelessEventMapping(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Application>;
    getDaChecks(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getEvents(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getInsightsPushSchedule(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createInsightsPushSchedule(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    getIosDialogConfigs(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createLeaderboardsCreate(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Application>;
    createLeaderboardsDeleteEntry(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Application>;
    createLeaderboardsReset(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Application>;
    createLeaderboardsSetScore(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Application>;
    createMmpAuditing(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    getMobileSdkGk(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createOccludesPopup(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    createPageActivity(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Application>;
    deletePaymentCurrencies(params?: Record<string, any>): Promise<any>;
    createPaymentCurrency(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Application>;
    getPermissions(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getProducts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPurchases(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getRoles(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getSubscribedDomains(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createSubscribedDomain(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Application>;
    getSubscribedDomainsPhishing(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createSubscribedDomainsPhishing(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Application>;
    deleteSubscriptions(params?: Record<string, any>): Promise<any>;
    createSubscription(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    createUpload(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    createUserProperty(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    get(fields: Array<string>, params?: Record<string, any>): Application;
    update(fields: Array<string>, params?: Record<string, any>): Application;
  }
}
declare module 'objects/partner-study' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PartnerStudy
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PartnerStudy extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): PartnerStudy;
  }
}
declare module 'objects/ad-study-objective' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * AdStudyObjective
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdStudyObjective extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Type(): Record<string, any>;
    getAdPlacePageSets(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAdsPixels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getApplications(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCustomConversions(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getOfflineConversionDataSets(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPartnerStudies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): AdStudyObjective;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): AdStudyObjective;
  }
}
declare module 'objects/ad-study' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * AdStudy
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdStudy extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Type(): Record<string, any>;
    getCells(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getObjectives(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): AdStudy;
    update(fields: Array<string>, params?: Record<string, any>): AdStudy;
  }
}
declare module 'objects/ad-image' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdImage
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdImage extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Status(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): AdImage;
  }
}
declare module 'objects/ad-label' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * AdLabel
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdLabel extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getAdCreatives(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAds(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAdSets(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getCampaigns(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): AdLabel;
    update(fields: Array<string>, params?: Record<string, any>): AdLabel;
  }
}
declare module 'objects/playable-content' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PlayableContent
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PlayableContent extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): PlayableContent;
  }
}
declare module 'objects/ad-account-ad-rules-history' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAccountAdRulesHistory
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAccountAdRulesHistory extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Action(): Record<string, any>;
    static get EvaluationType(): Record<string, any>;
  }
}
declare module 'objects/ad-account-ad-volume' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAccountAdVolume
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAccountAdVolume extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get RecommendationType(): Record<string, any>;
  }
}
declare module 'objects/async-request' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AsyncRequest
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AsyncRequest extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Status(): Record<string, any>;
    static get Type(): Record<string, any>;
  }
}
declare module 'objects/ad-async-request-set' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * AdAsyncRequestSet
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAsyncRequestSet extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get NotificationMode(): Record<string, any>;
    static get NotificationStatus(): Record<string, any>;
    getRequests(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): AdAsyncRequestSet;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): AdAsyncRequestSet;
  }
}
declare module 'objects/broad-targeting-categories' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * BroadTargetingCategories
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class BroadTargetingCategories extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/custom-audiences-tos' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CustomAudiencesTOS
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CustomAudiencesTOS extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-account-delivery-estimate' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAccountDeliveryEstimate
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAccountDeliveryEstimate extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get OptimizationGoal(): Record<string, any>;
  }
}
declare module 'objects/ad-account-matched-search-applications-edge-data' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAccountMatchedSearchApplicationsEdgeData
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAccountMatchedSearchApplicationsEdgeData extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get AppStore(): Record<string, any>;
  }
}
declare module 'objects/ad-account-max-bid' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAccountMaxBid
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAccountMaxBid extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/minimum-budget' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * MinimumBudget
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class MinimumBudget extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/business-owned-object-on-behalf-of-request' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * BusinessOwnedObjectOnBehalfOfRequest
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class BusinessOwnedObjectOnBehalfOfRequest extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Status(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): BusinessOwnedObjectOnBehalfOfRequest;
  }
}
declare module 'objects/publisher-block-list' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * PublisherBlockList
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PublisherBlockList extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    createAppendPublisherUrl(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    getPagedWebPublishers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): PublisherBlockList;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): PublisherBlockList;
  }
}
declare module 'objects/ad-account-reach-estimate' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAccountReachEstimate
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAccountReachEstimate extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/reach-frequency-prediction' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ReachFrequencyPrediction
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ReachFrequencyPrediction extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Action(): Record<string, any>;
    static get BuyingType(): Record<string, any>;
    static get InstreamPackages(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): ReachFrequencyPrediction;
  }
}
declare module 'objects/saved-audience' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * SavedAudience
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class SavedAudience extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): SavedAudience;
  }
}
declare module 'objects/ad-account-subscribed-apps' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAccountSubscribedApps
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAccountSubscribedApps extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-account-targeting-unified' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAccountTargetingUnified
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAccountTargetingUnified extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get LimitType(): Record<string, any>;
    static get RegulatedCategories(): Record<string, any>;
    static get WhitelistedTypes(): Record<string, any>;
    static get AppStore(): Record<string, any>;
    static get Objective(): Record<string, any>;
    static get Mode(): Record<string, any>;
  }
}
declare module 'objects/ad-account-tracking-data' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAccountTrackingData
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAccountTrackingData extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-account-user' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAccountUser
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAccountUser extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-account' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  import AdPlacePageSet from 'objects/ad-place-page-set';
  import AdCreative from 'objects/ad-creative';
  import AdImage from 'objects/ad-image';
  import AdLabel from 'objects/ad-label';
  import PlayableContent from 'objects/playable-content';
  import AdRule from 'objects/ad-rule';
  import Ad from 'objects/ad';
  import AdSet from 'objects/ad-set';
  import AdsPixel from 'objects/ads-pixel';
  import AdVideo from 'objects/ad-video';
  import Campaign from 'objects/campaign';
  import AdAsyncRequestSet from 'objects/ad-async-request-set';
  import CustomAudience from 'objects/custom-audience';
  import CustomConversion from 'objects/custom-conversion';
  import AdReportRun from 'objects/ad-report-run';
  import PublisherBlockList from 'objects/publisher-block-list';
  import ReachFrequencyPrediction from 'objects/reach-frequency-prediction';
  import AdAccountSubscribedApps from 'objects/ad-account-subscribed-apps';
  /**
   * AdAccount
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAccount extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Currency(): Record<string, any>;
    static get PermittedTasks(): Record<string, any>;
    static get Tasks(): Record<string, any>;
    static get ClaimObjective(): Record<string, any>;
    static get ContentType(): Record<string, any>;
    static get Subtype(): Record<string, any>;
    getActivities(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAdPlacePageSets(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdPlacePageSet(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdPlacePageSet>;
    createAdPlacePageSetsAsync(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdPlacePageSet>;
    getAdStudies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAdCreatives(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdCreative(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdCreative>;
    getAdCreativesByLabels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteAdImages(params?: Record<string, any>): Promise<any>;
    getAdImages(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdImage(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdImage>;
    getAdLabels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdLabel(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdLabel>;
    getAdPlayables(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdPlayable(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<PlayableContent>;
    getAdRulesHistory(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAdRulesLibrary(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdRulesLibrary(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdRule>;
    getAds(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAd(fields: Array<string>, params?: Record<string, any>): Promise<Ad>;
    getAdsVolume(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAdsByLabels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAdSets(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdSet(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdSet>;
    getAdSetsByLabels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAdsPixels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdsPixel(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdsPixel>;
    getAdvertisableApplications(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteAdVideos(params?: Record<string, any>): Promise<any>;
    getAdVideos(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdVideo(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdVideo>;
    getAffectedAdSets(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteAgencies(params?: Record<string, any>): Promise<any>;
    getAgencies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAgency(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdAccount>;
    getApplications(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteAssignedUsers(params?: Record<string, any>): Promise<any>;
    getAssignedUsers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAssignedUser(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdAccount>;
    createAsyncBatchRequest(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Campaign>;
    getAsyncRequests(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getAsyncAdRequestSets(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAsyncAdRequestSet(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdAsyncRequestSet>;
    createAudienceReplace(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    createBlockListDraft(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdAccount>;
    getBroadTargetingCategories(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteCampaigns(params?: Record<string, any>): Promise<any>;
    getCampaigns(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createCampaign(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Campaign>;
    getCampaignsByLabels(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getConnectedInstagramAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getContentDeliveryReport(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createCreateAndApplyPublisherBlockList(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    getCustomAudiences(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createCustomAudience(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<CustomAudience>;
    getCustomAudiencesTos(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createCustomAudiencesTo(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdAccount>;
    getCustomConversions(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createCustomConversion(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<CustomConversion>;
    getDeliveryEstimate(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getDeprecatedTargetingAdSets(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getGeneratePreviews(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getImpactingAdStudies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getInsights(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getInsightsAsync(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdReportRun>;
    getInstagramAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getMatchedSearchApplications(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getMaxBid(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getMinimumBudgets(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getOfflineConversionDataSets(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getOnBehalfRequests(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createProductAudience(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<CustomAudience>;
    getPromotePages(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getPublisherBlockLists(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createPublisherBlockList(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<PublisherBlockList>;
    getReachEstimate(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getReachFrequencyPredictions(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createReachFrequencyPrediction(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<ReachFrequencyPrediction>;
    getSavedAudiences(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteSubscribedApps(params?: Record<string, any>): Promise<any>;
    getSubscribedApps(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createSubscribedApp(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdAccountSubscribedApps>;
    getTargetingBrowse(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getTargetingSearch(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getTargetingSentenceLines(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getTargetingSuggestions(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getTargetingValidation(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getTracking(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createTracking(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdAccount>;
    getUsers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    deleteUsersOfAnyAudience(params?: Record<string, any>): Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): AdAccount;
    update(fields: Array<string>, params?: Record<string, any>): AdAccount;
  }
}
declare module 'objects/ads-pixel-stats-result' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdsPixelStatsResult
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdsPixelStatsResult extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Aggregation(): Record<string, any>;
  }
}
declare module 'objects/ads-pixel' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  import Cursor from 'cursor';
  /**
   * AdsPixel
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdsPixel extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get SortBy(): Record<string, any>;
    static get AutomaticMatchingFields(): Record<string, any>;
    static get DataUseSetting(): Record<string, any>;
    static get FirstPartyCookieStatus(): Record<string, any>;
    static get Tasks(): Record<string, any>;
    getAssignedUsers(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAssignedUser(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdsPixel>;
    getDaChecks(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createEvent(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdsPixel>;
    createShadowTrafficHelper(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AbstractObject>;
    deleteSharedAccounts(params?: Record<string, any>): Promise<any>;
    getSharedAccounts(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createSharedAccount(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdsPixel>;
    getSharedAgencies(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getStats(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): AdsPixel;
    update(fields: Array<string>, params?: Record<string, any>): AdsPixel;
  }
}
declare module 'objects/serverside/event-response' {
  /**
   * EventResponse
   * @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters}
   */
  export default class EventResponse {
    _events_received: number;
    _messages: Array<string>;
    _fbtrace_id: string;
    _id: string;
    _num_processed_entries: number;
    /**
     * @param {Number} events_received
     * @param {Array<string>} messages
     * @param {String} fbtrace_id
     * @param {String} id
     * @param {Number} num_processed_entries
     */
    constructor(
      events_received: number,
      messages: Array<string>,
      fbtrace_id: string,
      id: string,
      num_processed_entries: number,
    );
    /**
     * Sets the events received for the response received from Graph API.
     * events_received is represented by integer.
     * @return events_received representing the number of events received for the event Request
     */
    get events_received(): number;
    /**
     * Sets the events received for the response received from Graph API.
     * events_received is represented by integer.
     * @param events_received representing the number of events received for the event Request
     */
    set events_received(events_received: number);
    /**
     * Sets the events received for the response received from Graph API.
     * events_received is represented by integer.
     * @param {Number} events_received representing the number of events received for the event Request
     */
    setEventsReceived(events_received: number): EventResponse;
    /**
     * Sets the messages as array for the response received from Graph API.
     * @return messages in the event Response
     */
    get messages(): Array<string>;
    /**
     * Sets the messages as array for the response received from Graph API.
     * @param messages in the event Response
     */
    set messages(messages: Array<string>);
    /**
     * Sets the messages as array for the response received from Graph API.
     * @param {Array} messages in the event Response
     */
    setMessages(messages: Array<string>): EventResponse;
    /**
     * Gets the fbtrace_id for the response received from Graph API.
     * @return fbtrace_id in the event Response that can used for debugging purposes
     */
    get fbtrace_id(): string;
    /**
     * Sets the fbtrace_id for the response received from Graph API.
     * @param fbtrace_id in the event Response that can used for debugging purposes
     */
    set fbtrace_id(fbtrace_id: string);
    /**
     * Sets the fbtrace_id for the response received from Graph API.
     * @param {String} fbtrace_id in the event Response that can used for debugging purposes
     */
    setFbtraceId(fbtrace_id: string): EventResponse;
    /**
     * Gets the id of container to which the event request was successfully posted to.
     * @return id of the dataset
     */
    get id(): string;
    /**
     * Sets the id of container to which the event request was successfully posted to.
     * @param  id of the dataset
     */
    set id(id: string);
    /**
     * Gets the number of events that got posted as part of the original request.
     * @return num_processed_entries number of events posted to the dataset.
     */
    get num_processed_entries(): number;
    /**
     * Sets the number of events that got posted as part of the original request.
     * @param num_processed_entries number of events posted to the dataset.
     */
    set num_processed_entries(num_processed_entries: number);
  }
}
declare module 'objects/serverside/http-method' {
  const _default_2: Readonly<{
    POST: string;
    PUT: string;
    GET: string;
    DELETE: string;
  }>;
  export default _default_2;
}
declare module 'objects/serverside/http-service-interface' {
  export default class HttpServiceInterface {
    /**
     * @param {String} $url The graph API endpoint that will be requested
     * @param {HttpMethod} $method The HTTP request method
     * @param {Object} $headers Contains HTTP request headers including User-Agent and Accept-Encoding
     * @param {Object} $params Contains request parameters including access_token, data, test_event_code, etc.
     * @return {Promise<Object>}
     */
    executeRequest(
      url: string,
      method: string,
      headers: Record<string, any>,
      params: Record<string, any>,
    ): Promise<Record<string, any>>;
  }
}
declare module 'objects/serverside/http-service-client-config' {
  import HttpServiceInterface from 'objects/serverside/http-service-interface';
  export default class HttpServiceClientConfig {
    static _client: HttpServiceInterface;
    static setClient(client: HttpServiceInterface): void;
    static getClient(): HttpServiceInterface;
  }
}
declare module 'objects/serverside/user-data' {
  /**
   * UserData represents the User Data Parameters(user_data) of a Server Side Event Request.
   * 'user_data' is a set of identifiers Facebook can use for targeted attribution. See Custom Audiences from CRM Data for details on how to normalize and hash the data you send.
   * @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#user}
   */
  export default class UserData {
    _email: string;
    _phone: string;
    _gender: string;
    _first_name: string;
    _last_name: string;
    _date_of_birth: string;
    _city: string;
    _state: string;
    _zip: string;
    _country: string;
    _external_id: string;
    _client_ip_address: string;
    _client_user_agent: string;
    _fbc: string;
    _fbp: string;
    _subscription_id: string;
    _fb_login_id: string;
    _lead_id: string;
    _f5first: string;
    _f5last: string;
    _fi: string;
    _dobd: string;
    _dobm: string;
    _doby: string;
    /**
     * @param {String} email An email address, in lowercase.
     * @param {String} phone A phone number. Include only digits with country code, area code, and number.
     * @param {String} gender Gender, in lowercase. Either f or m.
     * @param {String} first_name A first name in lowercase.
     * @param {String} last_name A last name in lowercase.
     * @param {String} date_of_birth A date of birth given as year, month, and day in YYYYMMDD format.
     * @param {String} city A city in lower-case without spaces or punctuation.
     * @param {String} state A two-letter state code in lowercase.
     * @param {String} country A two-letter country code in lowercase.
     * @param {String} zip Postal code of the city in your country standard
     * @param {String} external_id Any unique ID from the advertiser,
     * @param {String} client_ip_address The IP address of the browser corresponding to the event.
     * @param {String} client_user_agent The user agent for the browser corresponding to the event.
     * @param {String} fbp The Facebook click ID value stored in the _fbc browser cookie under your domain.
     * @param {String} fbc The Facebook browser ID value stored in the _fbp browser cookie under your domain.
     * @param {String} subscription_id The subscription ID for the user in this transaction.
     * @param {String} fb_login_id The FB login ID for the user.
     * @param {String} lead_id The Id associated with a lead generated by Facebook's Lead Ads.
     * @param {String} dobd The date of birth day in DD format.
     * @param {String} dobm The date of birth month in MM format.
     * @param {String} doby The date of birth year in YYYY format.
     */
    constructor(
      email?: string,
      phone?: string,
      gender?: string,
      first_name?: string,
      last_name?: string,
      date_of_birth?: string,
      city?: string,
      state?: string,
      zip?: string,
      country?: string,
      external_id?: string,
      client_ip_address?: string,
      client_user_agent?: string,
      fbp?: string,
      fbc?: string,
      subscription_id?: string,
      fb_login_id?: string,
      lead_id?: string,
      dobd?: string,
      dobm?: string,
      doby?: string,
    );
    static get Gender(): Record<string, any>;
    /**
     * Gets the email address for the user data field.
     * An email address, in lowercase.
     * Example: joe@eg.com
     */
    get email(): string;
    /**
     * Sets the email address for the user data field.
     * @param email An email address, in lowercase.
     * Example: joe@eg.com
     */
    set email(email: string);
    /**
     * Sets the email address for the user data field.
     * @param {String} email An email address, in lowercase.
     * Example: joe@eg.com
     */
    setEmail(email: string): UserData;
    /**
     * Gets the phone number for the user data.
     * A phone number. Include only digits with country code, area code, and number.
     * Example: 16505551212
     */
    get phone(): string;
    /**
     * Sets the phone number for the user data.
     * @param phone A phone number. Include only digits with country code, area code, and number.
     * Example: 16505551212
     */
    set phone(phone: string);
    /**
     * Sets the phone number for the user data.
     * @param {String} phone A phone number. Include only digits with country code, area code, and number.
     * Example: 16505551212
     */
    setPhone(phone: string): UserData;
    /**
     * Gets the gender value for the user data.
     * Gender in lowercase. Either f for FEMALE or m for MALE.
     * Example: f
     */
    get gender(): string;
    /**
     * Sets the gender value for the user data.
     * @param gender Gender in lowercase. Either f for FEMALE or m for MALE.
     * Example: f
     */
    set gender(gender: string);
    /**
     * Sets the gender value for the user data.
     * @param {String} gender Gender in lowercase. Either f for FEMALE or m for MALE.
     * Example: f
     */
    setGender(gender: string): UserData;
    /**
     * Gets the date of birth for the user data.
     * A date of birth given as year, month, and day in the Format YYYYMMDD
     * Example: 19971226 for December 26, 1997.
     */
    get date_of_birth(): string;
    /**
     * Sets the date of birth for the user data.
     * @param date_of_birth A date of birth given as year, month, and day in the Format YYYYMMDD
     * Example: 19971226 for December 26, 1997.
     */
    set date_of_birth(date_of_birth: string);
    /**
     * Sets the date of birth for the user data.
     * @param {String} date_of_birth A date of birth given as year, month, and day in the Format YYYYMMDD
     * Example: 19971226 for December 26, 1997.
     */
    setDateOfBirth(date_of_birth: string): UserData;
    /**
     * Gets the last name for the user data.
     * last_name is the last name in lowercase.
     * Example: smith
     */
    get last_name(): string;
    /**
     * Sets the last name for the user data.
     * @param last_name is last name in lowercase.
     * Example: smith
     */
    set last_name(last_name: string);
    /**
     * Sets the last name for the user data.
     * @param {String} last_name is last name in lowercase.
     * Example: smith
     */
    setLastName(last_name: string): UserData;
    /**
     * Gets the first name for the user data.
     * first_name is first name in lowercase.
     * Example: joe
     */
    get first_name(): string;
    /**
     * Sets the first name for the user data.
     * @param first_name is first name in lowercase.
     * Example: joe
     */
    set first_name(first_name: string);
    /**
     * Sets the first name for the user data.
     * @param {String} first_name is first name in lowercase.
     * Example: joe
     */
    setFirstName(first_name: string): UserData;
    /**
     * Gets the city for the user data.
     * city is city in lower-case without spaces or punctuation.
     * Example: menlopark
     */
    get city(): string;
    /**
     * Sets the city for the user data.
     * @param city is city in lower-case without spaces or punctuation.
     * Example: menlopark
     */
    set city(city: string);
    /**
     * Sets the city for the user data.
     * @param {String} city is city in lower-case without spaces or punctuation.
     * Example: menlopark
     */
    setCity(city: string): UserData;
    /**
     * Gets the zip/postal code for the user data.
     * zip is a five-digit zip code for United States.For other locations, follow each country's standards.
     * Example: 98121 (for United States zip code)
     */
    get zip(): string;
    /**
     * Sets the zip/postal code for the user data.
     * @param zip is a five-digit zip code for United States.For other locations, follow each country's standards.
     * Example: 98121 (for United States zip code)
     */
    set zip(zip: string);
    /**
     * Sets the zip/postal code for the user data.
     * @param {String} zip is a five-digit zip code for United States.For other locations, follow each country's standards.
     * Example: 98121 (for United States zip code)
     */
    setZip(zip: string): UserData;
    /**
     * Gets the state for the user data.
     * state is state in lower-case without spaces or punctuation.
     * Example: ca
     */
    get state(): string;
    /**
     * Sets the state for the user data.
     * @param state is state in lower-case without spaces or punctuation.
     * Example: ca
     */
    set state(state: string);
    /**
     * Sets the state for the user data.
     * @param {String} state is state in lower-case without spaces or punctuation.
     * Example: ca
     */
    setState(state: string): UserData;
    /**
     * Gets the country for the user data.
     * country is A two-letter country code in lowercase.
     * Example: usa
     */
    get country(): string;
    /**
     * Sets the country for the user data.
     * @param country is A two-letter country code in lowercase.
     * Example: usa
     */
    set country(country: string);
    /**
     * Sets the country for the user data.
     * @param {String} country is A two-letter country code in lowercase.
     * Example: usa
     */
    setCountry(country: string): UserData;
    /**
     * Gets the external id for the user data.
     * external_id is a unique ID from the advertiser, such as loyalty membership IDs, user IDs, and external cookie IDs.
     * In the Offline Conversions API this is known as extern_id. For more information, see Offline Conversions.
     * If External ID is being sent via other channels, then it should be sent in the same format via the server-side API
     * @see {@link https://www.facebook.com/business/help/104039186799009}
     */
    get external_id(): string;
    /**
     * Sets the external id for the user data.
     * @param external_id is a unique ID from the advertiser, such as loyalty membership IDs, user IDs, and external cookie IDs.
     * In the Offline Conversions API this is known as extern_id. For more information, see Offline Conversions.
     * If External ID is being sent via other channels, then it should be sent in the same format via the server-side API
     * @see {@link https://www.facebook.com/business/help/104039186799009}
     */
    set external_id(external_id: string);
    /**
     * Sets the external id for the user data.
     * @param {String} external_id is a unique ID from the advertiser, such as loyalty membership IDs, user IDs, and external cookie IDs.
     * In the Offline Conversions API this is known as extern_id. For more information, see Offline Conversions.
     * If External ID is being sent via other channels, then it should be sent in the same format via the server-side API
     * @see {@link https://www.facebook.com/business/help/104039186799009}
     */
    setExternalId(external_id: string): UserData;
    /**
     * Gets the client ip address for the user data.
     * client_ip_address is the IP address of the browser corresponding to the event.
     */
    get client_ip_address(): string;
    /**
     * Sets the client ip address for the user data.
     * @param client_ip_address is the IP address of the browser corresponding to the event.
     */
    set client_ip_address(client_ip_address: string);
    /**
     * Sets the client ip address for the user data.
     * @param {String} client_ip_address is the IP address of the browser corresponding to the event.
     */
    setClientIpAddress(client_ip_address: string): UserData;
    /**
     * Gets the client user agent for the user data.
     * client_user_agent is the user agent for the browser corresponding to the event.
     */
    get client_user_agent(): string;
    /**
     * Sets the client user agent for the user data.
     * @param client_user_agent is the user agent for the browser corresponding to the event.
     */
    set client_user_agent(client_user_agent: string);
    /**
     * Sets the client user agent for the user data.
     * @param {String} client_user_agent is the user agent for the browser corresponding to the event.
     */
    setClientUserAgent(client_user_agent: string): UserData;
    /**
     * Gets the fbc for the user data.
     * fbc is the Facebook click ID value stored in the _fbc browser cookie under your domain.
     * See Managing fbc and fbp Parameters for how to get this value @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#fbc},
     * You can also generate this value from a fbclid query parameter.
     */
    get fbc(): string;
    /**
     * Sets the fbc for the user data.
     * @param fbc is the Facebook click ID value stored in the _fbc browser cookie under your domain.
     * See Managing fbc and fbp Parameters for how to get this value @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#fbc},
     * You can also generate this value from a fbclid query parameter.
     */
    set fbc(fbc: string);
    /**
     * Sets the fbc for the user data.
     * @param {String} fbc is the Facebook click ID value stored in the _fbc browser cookie under your domain.
     * See Managing fbc and fbp Parameters for how to get this value @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#fbc},
     * You can also generate this value from a fbclid query parameter.
     */
    setFbc(fbc: string): UserData;
    /**
     * Gets the fbp for the user data.
     * fbp is Facebook browser ID value stored in the _fbp browser cookie under your domain.
     * See Managing fbc and fbp Parameters for how to get this value @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#fbc},
     */
    get fbp(): string;
    /**
     * Sets the fbp for the user data.
     * @param fbp is Facebook browser ID value stored in the _fbp browser cookie under your domain.
     * See Managing fbc and fbp Parameters for how to get this value @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#fbc},
     */
    set fbp(fbp: string);
    /**
     * Sets the fbp for the user data.
     * @param {String} fbp is Facebook browser ID value stored in the _fbp browser cookie under your domain.
     * See Managing fbc and fbp Parameters for how to get this value @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#fbc},
     */
    setFbp(fbp: string): UserData;
    /**
     * Gets the subscription id for the user data.
     * @return subscription_id is the subscription ID for the user in this transaction. This is similar to the order ID for an individual product.
     * Example: anid1234.
     */
    get subscription_id(): string;
    /**
     * Sets the subscription id for the user data.
     * @param {String} subscription_id is the subscription ID for the user in this transaction. This is similar to the order ID for an individual product.
     * Example: anid1234.
     */
    set subscription_id(subscription_id: string);
    /**
     * Sets the subscription id for the user data.
     * @param {String} subscription_id is the subscription ID for the user in this transaction. This is similar to the order ID for an individual product.
     * Example: anid1234.
     */
    setSubscriptionId(subscription_id: string): UserData;
    /**
     *
     * Gets the fb_login_id for the user data.
     */
    get fb_login_id(): string;
    /**
     * Sets the fb_login_id for the user data.
     */
    set fb_login_id(fb_login_id: string);
    /**
     * Sets the fb_login_id for the user data.
     */
    setFbLoginId(fb_login_id: string): UserData;
    /**
     *
     * Gets the lead_id for the user data. Lead ID is associated with a lead generated by Facebook's Lead Ads.
     */
    get lead_id(): string;
    /**
     * Sets the lead_id for the user data. Lead ID is associated with a lead generated by Facebook's Lead Ads.
     */
    set lead_id(lead_id: string);
    /**
     * Sets the lead_id for the user data. Lead ID is associated with a lead generated by Facebook's Lead Ads.
     */
    setLeadId(lead_id: string): UserData;
    /**
     *
     * Gets the first 5 characters of the FirstName.
     */
    get f5first(): string;
    /**
     * Sets the Gets the first 5 characters of the FirstName.
     */
    set f5first(f5first: string);
    /**
     * Sets the first 5 characters of the FirstName.
     */
    setF5First(f5first: string): UserData;
    /**
     *
     * Gets the first 5 characters of the LastName.
     */
    get f5last(): string;
    /**
     * Sets the first 5 characters of the LastName.
     */
    set f5last(f5last: string);
    /**
     * Sets the first 5 characters of the LastName.
     */
    setF5Last(f5last: string): UserData;
    /**
     *
     * Gets the first Name Initial.
     */
    get fi(): string;
    /**
     * Sets the first Name Initial.
     */
    set fi(fi: string);
    /**
     * Sets the first Name Initial.
     */
    setFi(fi: string): UserData;
    /**
     *
     * Gets the date of birth day.
     */
    get dobd(): string;
    /**
     * Sets the date of birth day.
     */
    set dobd(dobd: string);
    /**
     * Sets the date of birth day.
     */
    setDobd(dobd: string): UserData;
    /**
     *
     * Gets the date of birth month.
     */
    get dobm(): string;
    /**
     * Sets the date of birth month.
     */
    set dobm(dobm: string);
    /**
     * Sets the date of birth month.
     */
    setDobm(dobm: string): UserData;
    /**
     *
     * Gets the date of birth year.
     */
    get doby(): string;
    /**
     * Sets the date of birth year.
     */
    set doby(doby: string);
    /**
     * Sets the date of birth year.
     */
    setDoby(doby: string): UserData;
    /**
     * Returns the normalized payload for the user_data parameter.
     * @returns {Object} normalized user data payload.
     */
    normalize(): Record<string, any>;
  }
}
declare module 'objects/serverside/server-event' {
  import UserData from 'objects/serverside/user-data';
  import CustomData from 'objects/serverside/custom-data';
  /**
   * ServerEvent
   * @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#serv}
   */
  export default class ServerEvent {
    _event_name: string;
    _event_time: number;
    _event_source_url: string;
    _event_id: string;
    _action_source: string;
    _opt_out: boolean;
    _user_data: UserData;
    _custom_data: CustomData;
    _data_processing_options: Array<string>;
    _data_processing_options_state: number;
    _data_processing_options_country: number;
    /**
     * @param {String} event_name A Facebook pixel Standard Event or Custom Event name.
     * @param {Number} event_time A Unix timestamp in seconds indicating when the actual event occurred.
     * @param {String} event_source_url The browser URL where the event happened.
     * @param {String} event_id This ID can be any string chosen by the advertiser.
     * @param {String} action_source A string that indicates where the event took place.
     * @param {Boolean} opt_out A flag that indicates we should not use this event for ads delivery optimization.
     * @param {UserData} user_data A map that contains user data. See UserData Class for options.
     * @param {CustomData} custom_data A map that contains user data. See CustomData Class for options.
     * @param {Array<string>} data_processing_options Processing options you would like to enable for a specific event.
     * @param {Number} data_processing_options_country A country that you want to associate to this data processing option.
     * @param {Number} data_processing_options_state A state that you want to associate with this data processing option.
     */
    constructor(
      event_name?: string,
      event_time?: number,
      event_source_url?: string,
      user_data?: UserData,
      custom_data?: CustomData,
      event_id?: string,
      opt_out?: boolean,
      action_source?: string,
      data_processing_options?: Array<string>,
      data_processing_options_country?: number,
      data_processing_options_state?: number,
    );
    /**
     * Gets the Event Name for the current Event.
     */
    get event_name(): string;
    /**
     * Sets the Event Name for the current Event.
     * @param {String} event_name a Facebook pixel Standard Event or Custom Event name.
     */
    set event_name(event_name: string);
    /**
     * Sets the Event Name for the current Event.
     * @param {String} event_name Facebook pixel Standard Event or Custom Event name.
     */
    setEventName(event_name: string): ServerEvent;
    /**
     * Gets the Event Time when the current Event happened.
     */
    get event_time(): number;
    /**
     * Sets the Event Time when the current Event happened.
     * @param {Number} event_time is a Unix timestamp in seconds indicating when the actual event occurred.
     */
    set event_time(event_time: number);
    /**
     * Sets the Event Time when the current Event happened.
     * @param {Number} event_time is a Unix timestamp in seconds indicating when the actual event occurred.
     */
    setEventTime(event_time: number): ServerEvent;
    /**
     * Gets the browser url source for the current event.
     */
    get event_source_url(): string;
    /**
     * Sets the browser url source for the current event.
     * @param {String} event_source_url The browser URL where the event happened.
     */
    set event_source_url(event_source_url: string);
    /**
     * Sets the browser url source for the current event.
     * @param {String} event_source_url The browser URL where the event happened.
     */
    setEventSourceUrl(event_source_url: string): ServerEvent;
    /**
     * Gets the event_id for the current Event.
     */
    get event_id(): string;
    /**
     * Sets the event Id for the current Event.
     * @param {String} event_id can be any string chosen by the advertiser. This is used with event_name to determine if events are identical.Learn about Deduplicate Pixel and Server-Side Events
     * @see {@link https://developers.facebook.com/docs/marketing-api/server-side-api/using-the-api#dedup}
     */
    set event_id(event_id: string);
    /**
     * Sets the event Id for the current Event.
     * @param {String} event_id can be any string chosen by the advertiser. This is used with event_name to determine if events are identical. Learn about Deduplicate Pixel and Server-Side Events.
     * @see {@link https://developers.facebook.com/docs/marketing-api/server-side-api/using-the-api#dedup}
     */
    setEventId(event_id: string): ServerEvent;
    /**
     * Gets the action_source for the current event. The Action Source represents where the action took place.
     */
    get action_source(): string;
    /**
     * Sets the action_source for the current event.
     * @param {String} action_source represents where the action took place. One of {'physical_store','app','chat','email','other','phone_call','system_generated','website'}
     */
    set action_source(action_source: string);
    /**
     * Sets the action_source for the current event.
     * @param {String} action_source represents where the action took place. One of {'physical_store','app','chat','email','other','phone_call','system_generated','website'}
     */
    setActionSource(action_source: string): ServerEvent;
    /**
     * Gets the opt_out feature for the current event.opt_out is a boolean flag that indicates we should not use this event for ads delivery optimization. If set to true, we only use the event for attribution.
     */
    get opt_out(): boolean;
    /**
     * Sets the opt_out feature for the current event.
     * @param {Boolean} opt_out is a boolean flag that indicates we should not use this event for ads delivery optimization. If set to true, we only use the event for attribution.
     */
    set opt_out(opt_out: boolean);
    /**
     * Sets the opt_out feature for the current event.
     * @param {Boolean} opt_out is a boolean flag that indicates we should not use this event for ads delivery optimization. If set to true, we only use the event for attribution.
     */
    setOptOut(opt_out: boolean): ServerEvent;
    /**
     * Gets the user data object for the current Server Event.
     * @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#user}
     */
    get user_data(): UserData;
    /**
     * Sets the user data object for the current Server Event.
     * @param {UserData} user_data user_data is a map that contains user data. See User Data Parameter Table for options. Also see Advanced Matching with the Pixel to see comparable options available for data sent via Facebook pixel.
     * @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#user}
     */
    set user_data(user_data: UserData);
    /**
     * Sets the user data object for the current Server Event.
     * @param {UserData} user_data user_data is a map that contains user data. See User Data Parameter Table for options. Also see Advanced Matching with the Pixel to see comparable options available for data sent via Facebook pixel.
     * @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#user}
     */
    setUserData(user_data: UserData): ServerEvent;
    /**
     * Gets the custom data object for the current Server Event.
     * @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#custom}
     */
    get custom_data(): CustomData;
    /**
     * Sets the custom data object for the current Server Event.
     * @param {CustomData} custom_data is a map that includes additional business data about the event.
     * @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#custom}
     */
    set custom_data(custom_data: CustomData);
    /**
     * Sets the custom data object for the current Server Event.
     * @param {CustomData} custom_data is a map that includes additional business data about the event.
     * @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#custom}
     */
    setCustomData(custom_data: CustomData): ServerEvent;
    /**
     * Gets the data_processing_options for the current event.
     * Processing options you would like to enable for a specific event.
     */
    get data_processing_options(): Array<string>;
    /**
     * Sets the data_processing_options for the current event.
     * @param {Array<string>} data_processing_options represents Data processing options you would like to enable for a specific event, e.g. [] or ['LDU']
     * @see {@link https://developers.facebook.com/docs/marketing-apis/data-processing-options}
     */
    set data_processing_options(data_processing_options: Array<string>);
    /**
     * Sets the data_processing_options for the current event.
     * @param {Array<string>} data_processing_options represents Data processing options you would like to enable for a specific event, e.g. [] or ['LDU']
     * @see {@link https://developers.facebook.com/docs/marketing-apis/data-processing-options}
     */
    setDataProcessingOptions(
      data_processing_options: Array<string>,
    ): ServerEvent;
    /**
     * Gets the data_processing_options_country for the current event.
     * A country that you want to associate to this data processing option.
     * @see {@link https://developers.facebook.com/docs/marketing-apis/data-processing-options}
     */
    get data_processing_options_country(): number;
    /**
     * Sets the data_processing_options_country for the current event.
     * @param {number} data_processing_options_country represents country that you want to associate to this data processing option.
     */
    set data_processing_options_country(
      data_processing_options_country: number,
    );
    /**
     * Sets the data_processing_options_country for the current event.
     * @param {number} data_processing_options_country represents country that you want to associate to this data processing option.
     */
    setDataProcessingOptionsCountry(
      data_processing_options_country: number,
    ): ServerEvent;
    /**
     * Gets the data_processing_options_state for the current event.
     * A state that you want to associate with this data processing option.
     * @see {@link https://developers.facebook.com/docs/marketing-apis/data-processing-options}
     */
    get data_processing_options_state(): number;
    /**
     * Sets the data_processing_options_state for the current event.
     * @param {number} data_processing_options_state represents state that you want to associate with this data processing option.
     */
    set data_processing_options_state(data_processing_options_state: number);
    /**
     * Sets the data_processing_options_state for the current event.
     * @param {number} data_processing_options_state represents state that you want to associate with this data processing option.
     */
    setDataProcessingOptionsState(
      data_processing_options_state: number,
    ): ServerEvent;
    /**
     * Returns the normalized payload for the event.
     * @returns {Object} normalized event payload.
     */
    normalize(): Record<string, any>;
  }
}
declare module 'objects/serverside/event-request' {
  import EventResponse from 'objects/serverside/event-response';
  import HttpServiceInterface from 'objects/serverside/http-service-interface';
  import ServerEvent from 'objects/serverside/server-event';
  /**
   * EventRequest
   * @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters}
   */
  export default class EventRequest {
    _access_token: string;
    _pixel_id: string;
    _events: Array<ServerEvent>;
    _partner_agent: string | null | undefined;
    _test_event_code: string | null | undefined;
    _namespace_id: string | null | undefined;
    _upload_id: string | null | undefined;
    _upload_tag: string | null | undefined;
    _upload_source: string | null | undefined;
    _debug_mode: boolean;
    _api: Record<string, any>;
    _http_service: HttpServiceInterface | null | undefined;
    /**
     * @param {String} access_token Access Token for the user calling Graph API
     * @param {String} pixel_id Pixel Id to which you are sending the events
     * @param {Array<ServerEvent>} events Data for the request Payload for a Server Side Event
     * @param {?String} partner_agent Platform from which the event is sent e.g. wordpress
     * @param {?String} test_event_code Test Event Code used to verify that your server events are received correctly by Facebook.
     * @param {?String} namespace_id Scope used to resolve extern_id or Third-party ID. Can be another data set or data partner ID.
     * @param {?String} upload_id Unique id used to denote the current set being uploaded.
     * @param {?String} upload_tag Tag string added to track your Offline event uploads.
     * @param {?String} upload_source The origin/source of data for the dataset to be uploaded.
     * @param {Boolean} debug_mode_flag Set to true if you want to enable more logging in SDK
     * @param {?HttpServiceInterface} http_service Override the default http request method by setting an object that implements HttpServiceInterface
     */
    constructor(
      access_token: string,
      pixel_id: string,
      events?: Array<ServerEvent>,
      partner_agent?: string | null | undefined,
      test_event_code?: string | null | undefined,
      namespace_id?: string | null | undefined,
      upload_id?: string | null | undefined,
      upload_tag?: string | null | undefined,
      upload_source?: string | null | undefined,
      debug_mode_flag?: boolean,
      http_service?: HttpServiceInterface | null | undefined,
    );
    /**
     * Gets the data for the request Payload for a Server Side Event. events is represented by a list/array of ServerEvent objects.
     */
    get events(): Array<ServerEvent>;
    /**
     * Sets the events for the request Payload for a Server Side Event.
     * events is represented by a list/array of ServerEvent objects.
     * @param events for the current server event
     */
    set events(events: Array<ServerEvent>);
    /**
     * Sets the events for the request Payload for a Server Side Event.
     * events is represented by a list/array of ServerEvent objects.
     * @param events for the current server event
     */
    setEvents(events: Array<ServerEvent>): EventRequest;
    /**
     * Gets the partner_agent for the request
     * Allows you to specify the platform from which the event is sent e.g. wordpress
     */
    get partner_agent(): string;
    /**
     * Sets the partner_agent for the request
     * Allows you to specify the platform from which the event is sent e.g. wordpress
     * @param {String} partner_agent String value for the partner agent
     */
    set partner_agent(partner_agent: string);
    /**
     * Sets the partner_agent for the request
     * Allows you to specify the platform from which the event is sent e.g. wordpress
     * @param {String} partner_agent String value for the partner agent
     */
    setPartnerAgent(partner_agent: string): EventRequest;
    /**
     * Gets the test_event_code for the request
     * Code used to verify that your server events are received correctly by Facebook.
     * Use this code to test your server events in the Test Events feature in Events Manager.
     * See Test Events Tool @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/using-the-api#testEvents} for an example.
     */
    get test_event_code(): string;
    /**
     * Sets the test_event_code for the request
     * Code used to verify that your server events are received correctly by Facebook.
     * Use this code to test your server events in the Test Events feature in Events Manager.
     * See Test Events Tool @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/using-the-api#testEvents} for an example.
     */
    set test_event_code(test_event_code: string);
    /**
     * Sets the test_event_code for the request
     * Code used to verify that your server events are received correctly by Facebook.
     * Use this code to test your server events in the Test Events feature in Events Manager.
     * See Test Events Tool @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/using-the-api#testEvents} for an example.
     */
    setTestEventCode(test_event_code: string): EventRequest;
    /**
     * Gets the debug mode flag for the Graph API request
     */
    get debug_mode(): boolean;
    /**
     * Sets the debug mode flag for the Graph API request
     * @param debug_mode boolean value representing whether you want to send the request in debug mode to get detailed logging.
     */
    set debug_mode(debug_mode: boolean);
    /**
     * Sets the debug mode flag for the Graph API request
     * @param {Boolean} debug_mode boolean value representing whether you want to send the request in debug mode to get detailed logging.
     */
    setDebugMode(debug_mode: boolean): EventRequest;
    /**
     * Gets the access token for the Graph API request
     */
    get access_token(): string;
    /**
     * Sets the access token for the Graph API request
     * @param access_token string representing the access token that is used to make the Graph API.
     */
    set access_token(access_token: string);
    /**
     * Sets the access token for the Graph API request
     * @param {String} access_token string representing the access token that is used to make the Graph API.
     */
    setAccessToken(access_token: string): EventRequest;
    /**
     * Gets the pixel against which we send the events
     */
    get pixel(): string;
    /**
     * Sets the pixel against which we send the events
     * @param {String} pixel_id string value representing the Pixel's Id to which you are sending the events.
     */
    set pixel_id(pixel_id: string);
    /**
     * Sets the pixel against which we send the events
     * @param {String} pixel_id String value for the pixel_id against which you want to send the events.
     */
    setPixelId(pixel_id: string): EventRequest;
    /**
     * Gets the NamespaceId for the events
     */
    get namespace_id(): string;
    /**
     * Sets the namespace_id for the events
     * @param {String} namespace_id Scope used to resolve extern_id or Third-party ID. Can be another data set or data partner ID.
     */
    set namespace_id(namespace_id: string);
    /**
     * Sets the namespace_id for the events
     * @param {String} namespace_id Scope used to resolve extern_id or Third-party ID. Can be another data set or data partner ID.
     */
    setNamespaceId(namespace_id: string): EventRequest;
    /**
     * Gets the Upload Tag for the current events upload
     */
    get upload_tag(): string;
    /**
     * Sets the upload_tag for the current events upload
     * @param {String} upload_tag Tag string added to Track your Offline event uploads
     */
    set upload_tag(upload_tag: string);
    /**
     * Sets the upload_tag for the current events upload
     * @param {String} upload_tag Tag string added to Track your Offline event uploads
     */
    setUploadTag(upload_tag: string): EventRequest;
    /**
     * Gets the Upload Tag for the current events upload
     */
    get upload_id(): string;
    /**
     * Sets the upload_id for the current events upload
     * @param {String} upload_id Unique id used to denote the current set being uploaded
     */
    set upload_id(upload_id: string);
    /**
     * Sets the upload_id for the current events upload
     * @param {String} upload_id Unique id used to denote the current set being uploaded
     */
    setUploadId(upload_id: string): EventRequest;
    /**
     * Gets the Upload Tag for the current events upload
     */
    get upload_source(): string;
    /**
     * Sets the upload_source for the current events upload
     * @param {String} upload_source origin/source of data for the dataset to be uploaded.
     */
    set upload_source(upload_source: string);
    /**
     * Sets the upload_source for the current events upload
     * @param {String} upload_source origin/source of data for the dataset to be uploaded.
     */
    setUploadSource(upload_source: string): EventRequest;
    /**
     * Gets the http_service object for making the events request
     */
    get http_service(): HttpServiceInterface;
    /**
     * Sets the http_service object for making the events request
     * @param {HttpServiceInterface} http_service
     */
    set http_service(http_service: HttpServiceInterface);
    /**
     * Sets the http_service object for making the events request
     * @param {HttpServiceInterface} http_service
     */
    setHttpService(http_service: HttpServiceInterface): EventRequest;
    /**
     * Executes the current event_request data by making a call to the Facebook Graph API.
     */
    execute(): Promise<EventResponse>;
    cloneWithoutEvents(): EventRequest;
  }
}
declare module 'objects/serverside/batch-processor' {
  import ServerEvent from 'objects/serverside/server-event';
  import EventRequest from 'objects/serverside/event-request';
  export default class BatchProcessor {
    _batch_size: number;
    _concurrent_requests: number;
    constructor(batch_size: number, concurrent_requests: number);
    processEventRequestsGenerator(event_requests: Array<EventRequest>): any;
    processEventRequests(event_requests: Array<EventRequest>): Promise<void>;
    processEventsGenerator(
      event_request_to_clone: EventRequest,
      all_events: Array<ServerEvent>,
    ): any;
    processEvents(
      event_request_to_clone: EventRequest,
      all_events: Array<ServerEvent>,
    ): Promise<void>;
  }
}
declare module 'objects/businessdataapi/user-data' {
  /**
   * UserData represents the User Data Parameters(user_data) of a Business Data Event Request.
   */
  export default class UserData {
    _email: string;
    _phone: string;
    _first_name: string;
    _last_name: string;
    _date_of_birth: string;
    _city: string;
    _state: string;
    _country: string;
    _zip: string;
    _external_id: string;
    _address: string;
    /**
     * @param {String} email An email address, in lowercase.
     * @param {String} phone A phone number. Include only digits with country code, area code, and number.
     * @param {String} first_name A first name in lowercase.
     * @param {String} last_name A last name in lowercase.
     * @param {String} date_of_birth A date of birth given as year, month, and day in YYYYMMDD format.
     * @param {String} city A city in lower-case without spaces or punctuation.
     * @param {String} state A two-letter state code in lowercase.
     * @param {String} country A two-letter country code in lowercase.
     * @param {String} zip Postal code of the city in your country standard.
     * @param {String} external_id Any unique ID from the business.
     * @param {String} address An physical address.
     */
    constructor(
      email: string,
      phone: string,
      first_name: string,
      last_name: string,
      date_of_birth: string,
      city: string,
      state: string,
      zip: string,
      country: string,
      external_id: string,
      address: string,
    );
    /**
     * Gets the email address for the user data field.
     * An email address, in lowercase.
     * Example: joe@eg.com
     */
    get email(): string;
    /**
     * Sets the email address for the user data field.
     * @param email An email address, in lowercase.
     * Example: joe@eg.com
     */
    set email(email: string);
    /**
     * Sets the email address for the user data field.
     * @param {String} email An email address, in lowercase.
     * Example: joe@eg.com
     */
    setEmail(email: string): UserData;
    /**
     * Gets the phone number for the user data.
     * A phone number. Include only digits with country code, area code, and number.
     * Example: 16505551212
     */
    get phone(): string;
    /**
     * Sets the phone number for the user data.
     * @param phone A phone number. Include only digits with country code, area code, and number.
     * Example: 16505551212
     */
    set phone(phone: string);
    /**
     * Sets the phone number for the user data.
     * @param {String} phone A phone number. Include only digits with country code, area code, and number.
     * Example: 16505551212
     */
    setPhone(phone: string): UserData;
    /**
     * Gets the date of birth for the user data.
     * A date of birth given as year, month, and day in the Format YYYYMMDD
     * Example: 19971226 for December 26, 1997.
     */
    get date_of_birth(): string;
    /**
     * Sets the date of birth for the user data.
     * @param date_of_birth A date of birth given as year, month, and day in the Format YYYYMMDD
     * Example: 19971226 for December 26, 1997.
     */
    set date_of_birth(date_of_birth: string);
    /**
     * Sets the date of birth for the user data.
     * @param {String} date_of_birth A date of birth given as year, month, and day in the Format YYYYMMDD
     * Example: 19971226 for December 26, 1997.
     */
    setDateOfBirth(date_of_birth: string): UserData;
    /**
     * Gets the last name for the user data.
     * last_name is the last name in lowercase.
     * Example: smith
     */
    get last_name(): string;
    /**
     * Sets the last name for the user data.
     * @param last_name is last name in lowercase.
     * Example: smith
     */
    set last_name(last_name: string);
    /**
     * Sets the last name for the user data.
     * @param {String} last_name is last name in lowercase.
     * Example: smith
     */
    setLastName(last_name: string): UserData;
    /**
     * Gets the first name for the user data.
     * first_name is first name in lowercase.
     * Example: joe
     */
    get first_name(): string;
    /**
     * Sets the first name for the user data.
     * @param first_name is first name in lowercase.
     * Example: joe
     */
    set first_name(first_name: string);
    /**
     * Sets the first name for the user data.
     * @param {String} first_name is first name in lowercase.
     * Example: joe
     */
    setFirstName(first_name: string): UserData;
    /**
     * Gets the city for the user data.
     * city is city in lower-case without spaces or punctuation.
     * Example: menlopark
     */
    get city(): string;
    /**
     * Sets the city for the user data.
     * @param city is city in lower-case without spaces or punctuation.
     * Example: menlopark
     */
    set city(city: string);
    /**
     * Sets the city for the user data.
     * @param {String} city is city in lower-case without spaces or punctuation.
     * Example: menlopark
     */
    setCity(city: string): UserData;
    /**
     * Gets the zip/postal code for the user data.
     * zip is a five-digit zip code for United States.For other locations, follow each country's standards.
     * Example: 98121 (for United States zip code)
     */
    get zip(): string;
    /**
     * Sets the zip/postal code for the user data.
     * @param zip is a five-digit zip code for United States.For other locations, follow each country's standards.
     * Example: 98121 (for United States zip code)
     */
    set zip(zip: string);
    /**
     * Sets the zip/postal code for the user data.
     * @param {String} zip is a five-digit zip code for United States.For other locations, follow each country's standards.
     * Example: 98121 (for United States zip code)
     */
    setZip(zip: string): UserData;
    /**
     * Gets the state for the user data.
     * state is state in lower-case without spaces or punctuation.
     * Example: ca
     */
    get state(): string;
    /**
     * Sets the state for the user data.
     * @param state is state in lower-case without spaces or punctuation.
     * Example: ca
     */
    set state(state: string);
    /**
     * Sets the state for the user data.
     * @param {String} state is state in lower-case without spaces or punctuation.
     * Example: ca
     */
    setState(state: string): UserData;
    /**
     * Gets the country for the user data.
     * country is A two-letter country code in lowercase.
     * Example: usa
     */
    get country(): string;
    /**
     * Sets the country for the user data.
     * @param country is A two-letter country code in lowercase.
     * Example: usa
     */
    set country(country: string);
    /**
     * Sets the country for the user data.
     * @param {String} country is A two-letter country code in lowercase.
     * Example: usa
     */
    setCountry(country: string): UserData;
    /**
     * Gets the external id for the user data.
     * external_id is a unique ID from the business, such as loyalty membership IDs, user IDs, and external cookie IDs.
     */
    get external_id(): string;
    /**
     * Sets the external id for the user data.
     * @param external_id is a unique ID from the business, such as loyalty membership IDs, user IDs, and external cookie IDs.
     */
    set external_id(external_id: string);
    /**
     * Sets the external id for the user data.
     * @param {String} external_id is a unique ID from the business, such as loyalty membership IDs, user IDs, and external cookie IDs.
     */
    setExternalId(external_id: string): UserData;
    /**
     * Gets the address for the user data.
     * address is a physical address
     */
    get address(): string;
    /**
     * Sets the address for the user data.
     * @param address is a physical address
     */
    set address(address: string);
    /**
     * Sets the address for the user data.
     * @param {String} address is a physical address
     */
    setAddress(address: string): void;
    /**
     * Convert to Json object for api call
     */
    toJson(): Record<string, any>;
  }
}
declare module 'objects/signal/utils' {
  /**
   * Utils contains the Utility modules used for sending Signal Events
   */
  export default class Utils {
    /**
     * construct set method return
     * @param any server_return
     * @param any bdapi_return
     * @return {Object} combined server field and business data field return
     */
    static constructResponse(
      server_return: any,
      bdapi_return: any,
    ): Record<string, any>;
  }
}
declare module 'objects/signal/user-data' {
  import BusinessDataUserData from 'objects/businessdataapi/user-data';
  import ServerUserData from 'objects/serverside/user-data';
  /**
   * UserData represents the User Data Parameters(user_data) of Business Data API and Conversion API Request.
   */
  export default class UserData {
    _business_data_user_data: BusinessDataUserData;
    _server_user_data: ServerUserData;
    /**
     * @param {String} email An email address, in lowercase.
     * @param {String} phone A phone number. Include only digits with country code, area code, and number.
     * @param {String} first_name A first name in lowercase.
     * @param {String} last_name A last name in lowercase.
     * @param {String} date_of_birth A date of birth given as year, month, and day in YYYYMMDD format.
     * @param {String} city A city in lower-case without spaces or punctuation.
     * @param {String} state A two-letter state code in lowercase.
     * @param {String} country A two-letter country code in lowercase.
     * @param {String} zip Postal code of the city in your country standard
     * @param {String} external_id Any unique ID from the advertiser,
     * @param {String} gender Gender, in lowercase. Either f or m.
     * @param {String} client_ip_address The IP address of the browser corresponding to the event.
     * @param {String} client_user_agent The user agent for the browser corresponding to the event.
     * @param {String} fbp The Facebook click ID value stored in the _fbc browser cookie under your domain.
     * @param {String} fbc The Facebook browser ID value stored in the _fbp browser cookie under your domain.
     * @param {String} subscription_id The subscription ID for the user in this transaction.
     * @param {String} fb_login_id The FB login ID for the user.
     * @param {String} lead_id The Id associated with a lead generated by Facebook's Lead Ads.
     * @param {String} dobd The date of birth day in DD format.
     * @param {String} dobm The date of birth month in MM format.
     * @param {String} doby The date of birth year in YYYY format.
     * @param {String} f5first The first 5 characters of the first name.
     * @param {String} f5last The first 5 characters of the last name.
     * @param {String} fi The first Name Initial
     * @param {String} address An physical address.
     */
    constructor(
      email: string,
      phone: string,
      first_name: string,
      last_name: string,
      date_of_birth: string,
      city: string,
      state: string,
      zip: string,
      country: string,
      external_id: string,
      gender: string,
      client_ip_address: string,
      client_user_agent: string,
      fbp: string,
      fbc: string,
      subscription_id: string,
      fb_login_id: string,
      lead_id: string,
      dobd: string,
      dobm: string,
      doby: string,
      f5first: string,
      f5last: string,
      fi: string,
      address: string,
    );
    /**
     * Gets the email address for the user data field.
     * An email address, in lowercase.
     * Example: joe@eg.com
     */
    get email(): string;
    /**
     * Sets the email address for the user data field.
     * @param email An email address, in lowercase.
     * Example: joe@eg.com
     */
    set email(email: string);
    /**
     * Sets the email address for the user data field.
     * @param {String} email An email address, in lowercase.
     * Example: joe@eg.com
     */
    setEmail(email: string): UserData;
    /**
     * Gets the phone number for the user data.
     * A phone number. Include only digits with country code, area code, and number.
     * Example: 16505551212
     */
    get phone(): string;
    /**
     * Sets the phone number for the user data.
     * @param phone A phone number. Include only digits with country code, area code, and number.
     * Example: 16505551212
     */
    set phone(phone: string);
    /**
     * Sets the phone number for the user data.
     * @param {String} phone A phone number. Include only digits with country code, area code, and number.
     * Example: 16505551212
     */
    setPhone(phone: string): UserData;
    /**
     * Gets the date of birth for the user data.
     * A date of birth given as year, month, and day in the Format YYYYMMDD
     * Example: 19971226 for December 26, 1997.
     */
    get date_of_birth(): string;
    /**
     * Sets the date of birth for the user data.
     * @param date_of_birth A date of birth given as year, month, and day in the Format YYYYMMDD
     * Example: 19971226 for December 26, 1997.
     */
    set date_of_birth(date_of_birth: string);
    /**
     * Sets the date of birth for the user data.
     * @param {String} date_of_birth A date of birth given as year, month, and day in the Format YYYYMMDD
     * Example: 19971226 for December 26, 1997.
     */
    setDateOfBirth(date_of_birth: string): UserData;
    /**
     * Gets the last name for the user data.
     * last_name is the last name in lowercase.
     * Example: smith
     */
    get last_name(): string;
    /**
     * Sets the last name for the user data.
     * @param last_name is last name in lowercase.
     * Example: smith
     */
    set last_name(last_name: string);
    /**
     * Sets the last name for the user data.
     * @param {String} last_name is last name in lowercase.
     * Example: smith
     */
    setLastName(last_name: string): UserData;
    /**
     * Gets the first name for the user data.
     * first_name is first name in lowercase.
     * Example: joe
     */
    get first_name(): string;
    /**
     * Sets the first name for the user data.
     * @param first_name is first name in lowercase.
     * Example: joe
     */
    set first_name(first_name: string);
    /**
     * Sets the first name for the user data.
     * @param {String} first_name is first name in lowercase.
     * Example: joe
     */
    setFirstName(first_name: string): UserData;
    /**
     * Gets the city for the user data.
     * city is city in lower-case without spaces or punctuation.
     * Example: menlopark
     */
    get city(): string;
    /**
     * Sets the city for the user data.
     * @param city is city in lower-case without spaces or punctuation.
     * Example: menlopark
     */
    set city(city: string);
    /**
     * Sets the city for the user data.
     * @param {String} city is city in lower-case without spaces or punctuation.
     * Example: menlopark
     */
    setCity(city: string): UserData;
    /**
     * Gets the zip/postal code for the user data.
     * zip is a five-digit zip code for United States.For other locations, follow each country's standards.
     * Example: 98121 (for United States zip code)
     */
    get zip(): string;
    /**
     * Sets the zip/postal code for the user data.
     * @param zip is a five-digit zip code for United States.For other locations, follow each country's standards.
     * Example: 98121 (for United States zip code)
     */
    set zip(zip: string);
    /**
     * Sets the zip/postal code for the user data.
     * @param {String} zip is a five-digit zip code for United States.For other locations, follow each country's standards.
     * Example: 98121 (for United States zip code)
     */
    setZip(zip: string): UserData;
    /**
     * Gets the state for the user data.
     * state is state in lower-case without spaces or punctuation.
     * Example: ca
     */
    get state(): string;
    /**
     * Sets the state for the user data.
     * @param state is state in lower-case without spaces or punctuation.
     * Example: ca
     */
    set state(state: string);
    /**
     * Sets the state for the user data.
     * @param {String} state is state in lower-case without spaces or punctuation.
     * Example: ca
     */
    setState(state: string): UserData;
    /**
     * Gets the country for the user data.
     * country is A two-letter country code in lowercase.
     * Example: usa
     */
    get country(): string;
    /**
     * Sets the country for the user data.
     * @param country is A two-letter country code in lowercase.
     * Example: usa
     */
    set country(country: string);
    /**
     * Sets the country for the user data.
     * @param {String} country is A two-letter country code in lowercase.
     * Example: usa
     */
    setCountry(country: string): UserData;
    /**
     * Gets the external id for the user data.
     * external_id is a unique ID from the advertiser, such as loyalty membership IDs, user IDs, and external cookie IDs.
     * In the Offline Conversions API this is known as extern_id. For more information, see Offline Conversions.
     * If External ID is being sent via other channels, then it should be sent in the same format via the server-side API
     * @see {@link https://www.facebook.com/business/help/104039186799009}
     */
    get external_id(): string;
    /**
     * Sets the external id for the user data.
     * @param external_id is a unique ID from the advertiser, such as loyalty membership IDs, user IDs, and external cookie IDs.
     * In the Offline Conversions API this is known as extern_id. For more information, see Offline Conversions.
     * If External ID is being sent via other channels, then it should be sent in the same format via the server-side API
     * @see {@link https://www.facebook.com/business/help/104039186799009}
     */
    set external_id(external_id: string);
    /**
     * Sets the external id for the user data.
     * @param {String} external_id is a unique ID from the advertiser, such as loyalty membership IDs, user IDs, and external cookie IDs.
     * In the Offline Conversions API this is known as extern_id. For more information, see Offline Conversions.
     * If External ID is being sent via other channels, then it should be sent in the same format via the server-side API
     * @see {@link https://www.facebook.com/business/help/104039186799009}
     */
    setExternalId(external_id: string): UserData;
    /**
     * Gets the gender value for the user data.
     * Gender in lowercase. Either f for FEMALE or m for MALE.
     * Example: f
     */
    get gender(): string;
    /**
     * Sets the gender value for the user data.
     * @param gender Gender in lowercase. Either f for FEMALE or m for MALE.
     * Example: f
     */
    set gender(gender: string);
    /**
     * Sets the gender value for the user data.
     * @param {String} gender Gender in lowercase. Either f for FEMALE or m for MALE.
     * Example: f
     */
    setGender(gender: string): UserData;
    /**
     * Gets the client ip address for the user data.
     * client_ip_address is the IP address of the browser corresponding to the event.
     */
    get client_ip_address(): string;
    /**
     * Sets the client ip address for the user data.
     * @param client_ip_address is the IP address of the browser corresponding to the event.
     */
    set client_ip_address(client_ip_address: string);
    /**
     * Sets the client ip address for the user data.
     * @param {String} client_ip_address is the IP address of the browser corresponding to the event.
     */
    setClientIpAddress(client_ip_address: string): UserData;
    /**
     * Gets the client user agent for the user data.
     * client_user_agent is the user agent for the browser corresponding to the event.
     */
    get client_user_agent(): string;
    /**
     * Sets the client user agent for the user data.
     * @param client_user_agent is the user agent for the browser corresponding to the event.
     */
    set client_user_agent(client_user_agent: string);
    /**
     * Sets the client user agent for the user data.
     * @param {String} client_user_agent is the user agent for the browser corresponding to the event.
     */
    setClientUserAgent(client_user_agent: string): UserData;
    /**
     * Gets the fbc for the user data.
     * fbc is the Facebook click ID value stored in the _fbc browser cookie under your domain.
     * See Managing fbc and fbp Parameters for how to get this value @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#fbc},
     * You can also generate this value from a fbclid query parameter.
     */
    get fbc(): string;
    /**
     * Sets the fbc for the user data.
     * @param fbc is the Facebook click ID value stored in the _fbc browser cookie under your domain.
     * See Managing fbc and fbp Parameters for how to get this value @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#fbc},
     * You can also generate this value from a fbclid query parameter.
     */
    set fbc(fbc: string);
    /**
     * Sets the fbc for the user data.
     * @param {String} fbc is the Facebook click ID value stored in the _fbc browser cookie under your domain.
     * See Managing fbc and fbp Parameters for how to get this value @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#fbc},
     * You can also generate this value from a fbclid query parameter.
     */
    setFbc(fbc: string): UserData;
    /**
     * Gets the fbp for the user data.
     * fbp is Facebook browser ID value stored in the _fbp browser cookie under your domain.
     * See Managing fbc and fbp Parameters for how to get this value @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#fbc},
     */
    get fbp(): string;
    /**
     * Sets the fbp for the user data.
     * @param fbp is Facebook browser ID value stored in the _fbp browser cookie under your domain.
     * See Managing fbc and fbp Parameters for how to get this value @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#fbc},
     */
    set fbp(fbp: string);
    /**
     * Sets the fbp for the user data.
     * @param {String} fbp is Facebook browser ID value stored in the _fbp browser cookie under your domain.
     * See Managing fbc and fbp Parameters for how to get this value @see {@link https://developers.facebook.com/docs/marketing-api/facebook-pixel/server-side-api/parameters#fbc},
     */
    setFbp(fbp: string): UserData;
    /**
     * Gets the subscription id for the user data.
     * @return subscription_id is the subscription ID for the user in this transaction. This is similar to the order ID for an individual product.
     * Example: anid1234.
     */
    get subscription_id(): string;
    /**
     * Sets the subscription id for the user data.
     * @param {String} subscription_id is the subscription ID for the user in this transaction. This is similar to the order ID for an individual product.
     * Example: anid1234.
     */
    set subscription_id(subscription_id: string);
    /**
     * Sets the subscription id for the user data.
     * @param {String} subscription_id is the subscription ID for the user in this transaction. This is similar to the order ID for an individual product.
     * Example: anid1234.
     */
    setSubscriptionId(subscription_id: string): UserData;
    /**
     * Gets the fb_login_id for the user data.
     */
    get fb_login_id(): string;
    /**
     * Sets the fb_login_id for the user data.
     * @param fb_login_id
     */
    set fb_login_id(fb_login_id: string);
    /**
     * Sets the fb_login_id for the user data.
     * @param {String} fb_login_id
     */
    setFbLoginId(fb_login_id: string): UserData;
    /**
     * Gets the lead_id for the user data. Lead ID is associated with a lead generated by Facebook's Lead Ads.
     */
    get lead_id(): string;
    /**
     * Sets the lead_id for the user data. Lead ID is associated with a lead generated by Facebook's Lead Ads.
     * @param lead_id
     */
    set lead_id(lead_id: string);
    /**
     * Sets the lead_id for the user data. Lead ID is associated with a lead generated by Facebook's Lead Ads.
     * @param {String} lead_id
     */
    setLeadId(lead_id: string): UserData;
    /**
     * Gets the first 5 characters of the FirstName.
     */
    get f5first(): string;
    /**
     * Sets the Gets the first 5 characters of the FirstName.
     * @param f5first
     */
    set f5first(f5first: string);
    /**
     * Sets the first 5 characters of the FirstName.
     * @param {String} f5first
     */
    setF5First(f5first: string): UserData;
    /**
     * Gets the first 5 characters of the LastName.
     */
    get f5last(): string;
    /**
     * Sets the first 5 characters of the LastName.
     * @param f5last
     */
    set f5last(f5last: string);
    /**
     * Sets the first 5 characters of the LastName.
     * @param {String} f5last
     */
    setF5Last(f5last: string): UserData;
    /**
     * Gets the first Name Initial.
     */
    get fi(): string;
    /**
     * Sets the first Name Initial.
     * @param fi
     */
    set fi(fi: string);
    /**
     * Sets the first Name Initial.
     * @param {String} fi
     */
    setFi(fi: string): UserData;
    /**
     * Gets the date of birth day.
     */
    get dobd(): string;
    /**
     * Sets the date of birth day.
     * @param dobd
     */
    set dobd(dobd: string);
    /**
     * Sets the date of birth day.
     * @param {String} dobd
     */
    setDobd(dobd: string): UserData;
    /**
     * Gets the date of birth month.
     */
    get dobm(): string;
    /**
     * Sets the date of birth month.
     * @param dobm
     */
    set dobm(dobm: string);
    /**
     * Sets the date of birth month.
     * @param {String} dobm
     */
    setDobm(dobm: string): UserData;
    /**
     * Gets the date of birth year.
     */
    get doby(): string;
    /**
     * Sets the date of birth year.
     * @param {String} doby
     */
    set doby(doby: string);
    /**
     * Sets the date of birth year.
     * @param {String} doby
     */
    setDoby(doby: string): UserData;
    /**
     * Gets the address for the user data.
     * address is a physical address
     */
    get address(): string;
    /**
     * Sets the address for the user data.
     * @param address is a physical address
     */
    set address(address: string);
    /**
     * Sets the date of birth year.
     * @param {String} address
     */
    setAddress(address: string): UserData;
    /**
     * Gets the user_data for the Business Data API.
     */
    get business_data_user_data(): BusinessDataUserData;
    /**
     * Gets the user_data for the Conversion API.
     */
    get server_user_data(): ServerUserData;
  }
}
declare module 'objects/businessdataapi/content' {
  /**
   * Content is part of the Custom Data Parameters of a Business Data Event Request. Content can be used to set the item/product details added in the Custom Data.
   */
  export default class Content {
    _id: string;
    _quantity: number;
    _price: number;
    _title: string;
    _tax: number;
    _external_content_id: string;
    /**
     * @param {String} id Product Id of the Item.
     * @param {Number} quantity Quantity of the Item.
     * @param {Number} price Subtotal for the content/product.
     * @param {String} title Title of the listed Item.
     * @param {Number} tax Subtotal tax for the content/product.
     * @param {String} external_content_id Unique ID for the contents/products that are being involved in the customer interaction.
     */
    constructor(
      id: string,
      quantity: number,
      price: number,
      title: string,
      tax: number,
      external_content_id: string,
    );
    /**
     * Gets the Product Id of the Item.
     * A string representing the unique Id for the product.
     * Example: XYZ.
     */
    get id(): string;
    /**
     * Sets the Product Id of the Item.
     * @param id A string representing the unique Id for the product.
     * Example: XYZ.
     */
    set id(id: string);
    /**
     * Sets the Product Id of the Item.
     * @param {String} id is a string representing the unique id for the product.
     * Example: XYZ.
     */
    setId(id: string): Content;
    /**
     * Gets the quantity of the Item.
     * The number/quantity of the content that is being involved in the customer interaction.
     * Example: 5
     */
    get quantity(): number;
    /**
     * Sets the quantity of the Item.
     * @param quantity The number/quantity of the product that is being involved in the customer interaction.
     * Example: 5
     */
    set quantity(quantity: number);
    /**
     * Sets the quantity of the Content/Item.
     * @param {Number} quantity The number/quantity of the product that is being involved in the customer interaction.
     * Example: 5
     */
    setQuantity(quantity: number): Content;
    /**
     * Gets the total price of the Item.
     * The total price for the products that are being involved in the customer interaction.
     * Example: '123.45'
     */
    get price(): number;
    /**
     * Sets the total price of the Item.
     * @param price The total price for the products that are being involved in the customer interaction.
     * Example: '123.45'
     */
    set price(price: number);
    /**
     * Sets the total price of the Item.
     * @param {Number} price The total price for the products that are being involved in the customer interaction.
     * Example: '123.45'
     */
    setPrice(price: number): Content;
    /**
     * Gets the Title of the listed Item.
     * A string representing the Title for the product.
     */
    get title(): string;
    /**
     * Sets the Title of the listed Item.
     * @param title A string representing the Title for the product.
     */
    set title(title: string);
    /**
     * Sets the Title of the Item.
     * @param {String} title is a string representing listed title for the product.
     */
    setTitle(title: string): Content;
    /**
     * Gets the total tax of the Item.
     * The total tax for the products that are being involved in the customer interaction.
     * Example: 45.5
     */
    get tax(): number;
    /**
     * Sets the total tax of the Item.
     * @param tax The total tax for the products that are being involved in the customer interaction.
     * Example: 45.5
     */
    set tax(tax: number);
    /**
     * Sets the total tax of the Item.
     * @param {Number} tax The total tax for the products that are being involved in the customer interaction.
     * Example: 45.5
     */
    setTax(tax: number): Content;
    /**
     * Gets the external id for this order item
     * The external id for the products that are being involved in the customer interaction.
     */
    get external_content_id(): string;
    /**
     * Sets the external id for this order item
     * @param external_content_id The external id for the products that are being involved in the customer interaction.
     */
    set external_content_id(external_content_id: string);
    /**
     * Sets the external id for this order item
     * @param {String} external_content_id The external id for the products that are being involved in the customer interaction.
     */
    setExternalContentID(external_content_id: string): Content;
    /**
     * Convert to Json object for api call
     */
    toJson(): Record<string, any>;
  }
}
declare module 'objects/signal/content' {
  import BusinessDataContent from 'objects/businessdataapi/content';
  import ServerContent from 'objects/serverside/content';
  /**
   * UserData represents the User Data Parameters(user_data) of Business Data API and Conversion API Request.
   */
  export default class Content {
    _business_data_content: BusinessDataContent;
    _server_content: ServerContent;
    /**
     * @param {String} id Product Id of the Item.
     * @param {Number} quantity Quantity of the Item.
     * @param {Number} price Subtotal for the content/product.
     * @param {Number} item_price Price per unit of the content/product.
     * @param {String} title Title of the listed Item.
     * @param {String} description Product description used for the item.
     * @param {String} brand Brand of the item.
     * @param {String} category Category of the Item.
     * @param {String} delivery_category The type of delivery for a purchase event
     * @param {Number} tax Subtotal tax for the content/product.
     * @param {String} external_content_id Unique ID for the contents/products that are being involved in the customer interaction.
     */
    constructor(
      id: string,
      quantity: number,
      price: number,
      item_price: number,
      title: string,
      description: string,
      brand: string,
      category: string,
      delivery_category: string,
      tax: number,
      external_content_id: string,
    );
    /**
     * Gets the Product Id of the Item.
     * A string representing the unique Id for the product.
     * Example: XYZ.
     */
    get id(): string;
    /**
     * Sets the Product Id of the Item.
     * @param id A string representing the unique Id for the product.
     * Example: XYZ.
     */
    set id(id: string);
    /**
     * Sets the Product Id of the Item.
     * @param id is a string representing the unique id for the product.
     * Example: XYZ.
     */
    setId(id: string): Content;
    /**
     * Gets the quantity of the Item.
     * The number/quantity of the content that is being involved in the customer interaction.
     * Example: 5
     */
    get quantity(): number;
    /**
     * Sets the quantity of the Item.
     * @param quantity The number/quantity of the product that is being involved in the customer interaction.
     * Example: 5
     */
    set quantity(quantity: number);
    /**
     * Sets the quantity of the Content/Item.
     * @param {Number} quantity The number/quantity of the product that is being involved in the customer interaction.
     * Example: 5
     */
    setQuantity(quantity: number): Content;
    /**
     * Gets the item price for the Product.
     * The item_price or price per unit of the product.
     * Example: '123.45'
     */
    get item_price(): number;
    /**
     * Sets the item price for the Content.
     * @param item_price The item_price or price per unit of the product.
     * Example: '123.45'
     */
    set item_price(item_price: number);
    /**
     * Sets the item price for the Content.
     * @param {Number} item_price The item_price or price per unit of the product.
     * Example: '123.45'
     */
    setItemPrice(item_price: number): Content;
    /**
     * Gets the Title of the listed Item.
     * A string representing the Title for the product.
     */
    get title(): string;
    /**
     * Sets the Title of the listed Item.
     * @param title A string representing the Title for the product.
     */
    set title(title: string);
    /**
     * Sets the Title of the Item.
     * @param title is a string representing listed title for the product.
     */
    setTitle(title: string): Content;
    /**
     * Gets the Description of the listed Item.
     * A string representing the Description for the product.
     */
    get description(): string;
    /**
     * Sets the Description of the listed Item.
     * @param description A string representing the Description for the product.
     */
    set description(description: string);
    /**
     * Sets the Product Description of the Item.
     * @param description is a string representing the description for the product.
     */
    setDescription(description: string): Content;
    /**
     * Gets the Brand of the listed Item.
     * A string representing the Brand for the product.
     */
    get brand(): string;
    /**
     * Sets the Brand of the listed Item.
     * @param brand A string representing the Brand for the product.
     */
    set brand(brand: string);
    /**
     * Sets the Brand of the Product.
     * @param brand is a string representing the Brand for the product.
     */
    setBrand(brand: string): Content;
    /**
     * Gets the Category of the listed Item.
     * A string representing the Category for the product.
     */
    get category(): string;
    /**
     * Sets the Category of the listed Item.
     * @param category A string representing the Category for the product.
     */
    set category(category: string);
    /**
     * Sets the Category of the Product.
     * @param category is a string representing the Category for the product.
     */
    setCategory(category: string): Content;
    /**
     * Gets the delivery category.
     */
    get delivery_category(): string;
    /**
     * Sets the type of delivery for a purchase event.
     * @param delivery_category The delivery category.
     */
    set delivery_category(delivery_category: string);
    /**
     * Sets the type of delivery for a purchase event.
     * @param {String} delivery_category The delivery category.
     */
    setDeliveryCategory(delivery_category: string): Content;
    /**
     * Gets the total tax of the Item.
     * The total tax for the products that are being involved in the customer interaction.
     * Example: 45.5
     */
    get tax(): number;
    /**
     * Sets the total tax of the Item.
     * @param tax The total tax for the products that are being involved in the customer interaction.
     * Example: 45.5
     */
    set tax(tax: number);
    /**
     * Sets the total tax of the Item.
     * @param {Number} tax The total tax for the products that are being involved in the customer interaction.
     * Example: 45.5
     */
    setTax(tax: number): Content;
    /**
     * Gets the external id for this order item
     * The external id for the products that are being involved in the customer interaction.
     */
    get external_content_id(): string;
    /**
     * Sets the external id for this order item
     * @param external_content_id The external id for the products that are being involved in the customer interaction.
     */
    set external_content_id(external_content_id: string);
    /**
     * Sets the total tax of the Item.
     * @param {String} external_content_id The total tax for the products that are being involved in the customer interaction.
     */
    setExternalContentId(external_content_id: string): Content;
    /**
     * Gets the total price of the Item.
     * The total price for the products that are being involved in the customer interaction.
     * Example: '123.45'
     */
    get price(): number;
    /**
     * Sets the total price of the Item.
     * @param price The total price for the products that are being involved in the customer interaction.
     * Example: '123.45'
     */
    set price(price: number);
    /**
     * Sets the total price of the Item.
     * @param {Number} price The total price for the products that are being involved in the customer interaction.
     * Example: '123.45'
     */
    setPrice(price: number): Content;
    /**
     * Gets the constructed content for Business Data API
     */
    get business_data_content(): BusinessDataContent;
    /**
     * Gets the constructed content for Conversion API
     */
    get server_content(): ServerContent;
  }
}
declare module 'objects/businessdataapi/custom-data' {
  import Content from 'objects/businessdataapi/content';
  import UserData from 'objects/businessdataapi/user-data';
  /**
   * CustomData represents the Custom Data Parameters of a Business Data Event Request.
   */
  export default class CustomData {
    _value: number;
    _currency: string;
    _contents: Array<Content>;
    _order_id: string;
    _status: string;
    _shipping_contact: UserData;
    _billing_contact: UserData;
    _external_order_id: string;
    _original_order_id: string;
    _message: string;
    /**
     * @param {Number} value value of the order Eg: 123.45
     * @param {String} currency currency involved in the transaction Eg: usd
     * @param {Array<Content>} contents Array of Content Objects. Use {Content} class to define a content.
     * @param {String} order_id Unique id representing the order
     * @param {String} status Status of order
     * @param {String} shipping_contact Shipping contact information. User {UserData} class to define a contact.
     * @param {String} billing_contact Billing contact information. User {UserData} class to define a contact.
     * @param {String} external_order_id Unique ID representing the order, universal across multiple categories from the business
     * @param {String} original_order_id Original order id for refund. For Refund event only.
     * @param {String} message Reason for refund. For Refund event only.
     */
    constructor(
      value: number,
      currency: string,
      contents: Array<Content>,
      order_id: string,
      status: string,
      shipping_contact: UserData,
      billing_contact: UserData,
      external_order_id: string,
      original_order_id: string,
      message: string,
    );
    /**
     * Gets the total value of the order.
     * A numeric value associated with this event. This could be a monetary value or a value in some other metric.
     * Example: 142.54.
     */
    get value(): number;
    /**
     * Sets the value of the custom data.
     * @param value A numeric value associated with this event. This could be a monetary value or a value in some other metric.
     * Example: 142.54.
     */
    set value(value: number);
    /**
     * Sets the value of the custom data.
     * @param {Number} value A numeric value associated with this event. This could be a monetary value or a value in some other metric.
     * Example: 142.54.
     */
    setValue(value: number): CustomData;
    /**
     * Gets the currency for the custom data.
     * The currency for the value specified, if applicable. Currency must be a valid ISO 4217 three digit currency code.
     * Example: 'usd'
     */
    get currency(): string;
    /**
     * Sets the currency for the custom data.
     * @param currency The currency for the value specified, if applicable. Currency must be a valid ISO 4217 three digit currency code.
     * Example: 'usd'
     */
    set currency(currency: string);
    /**
     * Sets the currency for the custom data.
     * @param {String} currency The currency for the value specified, if applicable. Currency must be a valid ISO 4217 three digit currency code.
     * Example: 'usd'
     */
    setCurrency(currency: string): CustomData;
    /**
     * Gets the contents for the custom data.
     * An array of Content objects that contain the product IDs associated with the event plus information about the products.
     * Example: [{'id':'ABC123','quantity' :2,'price':5.99}, {'id':'XYZ789','quantity':2, 'price':9.99}]
     */
    get contents(): Array<Content>;
    /**
     * Sets the contents for the custom data.
     * @param contents An array of Content objects that contain the product IDs associated with the event plus information about the products.
     * Example: [{'id':'ABC123','quantity' :2,'price':5.99}, {'id':'XYZ789','quantity':2, 'price':9.99}]
     */
    set contents(contents: Array<Content>);
    /**
     * Sets the contents for the custom data.
     * @param { Array< Content >} contents An array of Content objects that contain the product IDs associated with the event plus information about the products.
     * Example: [{'id':'ABC123','quantity' :2,'item_price':5.99}, {'id':'XYZ789','quantity':2, 'item_price':9.99}]
     */
    setContents(contents: Array<Content>): CustomData;
    /**
     * Gets the order id for the custom data.
     * order_id is the order ID for this transaction as a String.
     * Example: 'order1234'
     */
    get order_id(): string;
    /**
     * Sets the order_id for the custom data.
     * @param order_id The order ID for this transaction as a String.
     * Example: 'order1234'
     */
    set order_id(order_id: string);
    /**
     * Sets the order_id for the custom data.
     * @param {String} order_id The order ID for this transaction as a String.
     * Example: 'order1234'
     */
    setOrderId(order_id: string): CustomData;
    /**
     * Gets the status of order.
     * Status of the order, as a String.
     */
    get status(): string;
    /**
     * Gets the status of order.
     * @param status Status of the order, as a String.
     */
    set status(status: string);
    /**
     * Sets the status of the registration event.
     * @param {String} status Status of the registration event, as a String.
     */
    setStatus(status: string): CustomData;
    /**
     * Gets the shipping contact of the order.
     * An Object contains the user data of shipping contact. Use {UserData} to construct the object.
     */
    get shipping_contact(): UserData;
    /**
     * Sets the shipping contact of the order.
     * @param shipping_contact An Object contains the user data of shipping contact. Use {UserData} to construct the object.
     */
    set shipping_contact(shipping_contact: UserData);
    /**
     * Sets the shipping contact of the order.
     * @param {UserData} shipping_contact An Object contains the user data of shipping contact. Use {UserData} to construct the object.
     */
    setShippingContact(shipping_contact: UserData): CustomData;
    /**
     * Gets the billing contact of the order.
     * An Object contains the user data of billing contact. Use {UserData} to construct the object.
     */
    get billing_contact(): UserData;
    /**
     * Sets the billing contact of the order.
     * @param billing_contact An Object contains the user data of billing contact. Use {UserData} to construct the object.
     */
    set billing_contact(billing_contact: UserData);
    /**
     * Sets the billing contact of the order.
     * @param {UserData} billing_contact An Object contains the user data of billing contact. Use {UserData} to construct the object.
     */
    setBillingContact(billing_contact: UserData): CustomData;
    /**
     * Gets the unique id of the order.
     * Unique ID representing the order, universal across multiple categories from the business.
     */
    get external_order_id(): string;
    /**
     * Sets the unique id of the order.
     * @param external_order_id Unique ID representing the order, universal across multiple categories from the business.
     */
    set external_order_id(external_order_id: string);
    /**
     * Sets the unique id of the order.
     * @param {String} external_order_id Unique ID representing the order, universal across multiple categories from the business.
     */
    setExternalOrderId(external_order_id: string): CustomData;
    /**
     * Gets the unique id of the original order.
     * Original order id for refund. For Refund event only.
     */
    get original_order_id(): string;
    /**
     * Sets the unique id of the original order.
     * @param original_order_id Original order id for refund. For Refund event only.
     */
    set original_order_id(original_order_id: string);
    /**
     * Sets the unique id of the original order.
     * @param {String} original_order_id Original order id for refund. For Refund event only.
     */
    setOriginalOrderId(original_order_id: string): CustomData;
    /**
     * Gets the unique id of the original order.
     * Reason for refund. For Refund event only.
     */
    get message(): string;
    /**
     * Sets the unique id of the original order.
     * @param message Reason for refund. For Refund event only.
     */
    set message(message: string);
    /**
     * Sets the unique id of the original order.
     * @param {String} message Reason for refund. For Refund event only.
     */
    setMessage(message: string): CustomData;
    /**
     * Convert to Json object for api call
     */
    toJson(): Record<string, any>;
  }
}
declare module 'objects/signal/custom-data' {
  import BusinessDataCustomData from 'objects/businessdataapi/custom-data';
  import ServerCustomData from 'objects/serverside/custom-data';
  import SignalUserData from 'objects/signal/user-data';
  import SignalContent from 'objects/signal/content';
  /**
   * CustomData represents the Custom Data Parameters for both Conversion API and Business Data API.
   */
  export default class CustomData {
    _business_data_custom_data: BusinessDataCustomData;
    _server_custom_data: ServerCustomData;
    /**
     * params both Business Data API & Conversion API consume
     * @param {Number} value value of the item Eg: 123.45
     * @param {String} currency currency involved in the transaction Eg: usd
     * @param {Array<SignalContent>} contents Array of Content Objects. Use {Content} class to define a content.
     * @param {String} order_id Unique id representing the order
     * @param {String} status Status of the registration in Registration event.
     *                        Use only with CompleteRegistration events for Conversion API.
     *                        Use only with Purchase or UpdateOrder events for Business Data API
     * params only Conversion API consumes
     * @param {String} content_name name of the Content Eg: lettuce
     * @param {String} content_category category of the content Eg: grocery
     * @param {Array<String>} content_ids list of content unique ids involved in the event
     * @param {String} content_type Type of the Content group or Product SKU
     * @param {Number} predicted_ltv Predicted LifeTime Value for the customer involved in the event
     * @param {Number} num_items Number of items involved
     * @param {String} search_string query string used for the Search event
     * @param {String} item_number The item number
     * @param {String} delivery_category The type of delivery for a purchase event
     * @param {Object} custom_properties Custom Properties to be added to the Custom Data
     * params only Business Data API consumes
     * @param {SignalUserData} shipping_contact Shipping contact information. User {UserData} class to define a contact.
     * @param {SignalUserData} billing_contact Billing contact information. User {UserData} class to define a contact.
     * @param {String} external_order_id Unique ID representing the order, universal across multiple categories from the business
     * @param {String} original_order_id Original order id for refund. For Refund event only.
     * @param {String} message Reason for refund. For Refund event only.
     */
    constructor(
      value: number,
      currency: string,
      content_name: string,
      content_category: string,
      content_ids: Array<string>,
      contents: Array<SignalContent>,
      content_type: string,
      order_id: string,
      predicted_ltv: number,
      num_items: number,
      search_string: string,
      status: string,
      item_number: string,
      delivery_category: string,
      custom_properties: Record<string, any>,
      shipping_contact: SignalUserData,
      billing_contact: SignalUserData,
      external_order_id: string,
      original_order_id: string,
      message: string,
    );
    /**
     * Gets the value of the custom data.
     * A numeric value associated with this event. This could be a monetary value or a value in some other metric.
     * Example: 142.54.
     */
    get value(): number;
    /**
     * Sets the value of the custom data.
     * @param value A numeric value associated with this event. This could be a monetary value or a value in some other metric.
     * Example: 142.54.
     */
    set value(value: number);
    /**
     * Sets the value of the custom data.
     * @param {Number} value A numeric value associated with this event. This could be a monetary value or a value in some other metric.
     * Example: 142.54.
     */
    setValue(value: number): CustomData;
    /**
     * Gets the currency for the custom data.
     * The currency for the value specified, if applicable. Currency must be a valid ISO 4217 three digit currency code.
     * Example: 'usd'
     */
    get currency(): string;
    /**
     * Sets the currency for the custom data.
     * @param currency The currency for the value specified, if applicable. Currency must be a valid ISO 4217 three digit currency code.
     * Example: 'usd'
     */
    set currency(currency: string);
    /**
     * Sets the currency for the custom data.
     * @param {String} currency The currency for the value specified, if applicable. Currency must be a valid ISO 4217 three digit currency code.
     * Example: 'usd'
     */
    setCurrency(currency: string): CustomData;
    /**
     * Gets the contents for the custom data.
     * An array of Content objects that contain the product IDs associated with the event plus information about the products. id, quantity, and item_price are available fields.
     * Example: [{'id':'ABC123','quantity' :2,'item_price':5.99}, {'id':'XYZ789','quantity':2, 'item_price':9.99}]
     */
    get contents(): Array<SignalContent>;
    /**
     * Sets the contents for the custom data.
     * @param contents An array of Content objects that contain the product IDs associated with the event plus information about the products. id, quantity, and item_price are available fields.
     * Example: [{'id':'ABC123','quantity' :2,'item_price':5.99}, {'id':'XYZ789','quantity':2, 'item_price':9.99}]
     */
    set contents(contents: Array<SignalContent>);
    /**
     * Sets the contents for the custom data.
     * @param {Array<Content>} contents An array of Content objects that contain the product IDs associated with the event plus information about the products. id, quantity, and item_price are available fields.
     * Example: [{'id':'ABC123','quantity' :2,'item_price':5.99}, {'id':'XYZ789','quantity':2, 'item_price':9.99}]
     */
    setContents(contents: Array<SignalContent>): CustomData;
    /**
     * Gets the order id for the custom data.
     * order_id is the order ID for this transaction as a String.
     * Example: 'order1234'
     */
    get order_id(): string;
    /**
     * Sets the order_id for the custom data.
     * @param order_id The order ID for this transaction as a String.
     * Example: 'order1234'
     */
    set order_id(order_id: string);
    /**
     * Sets the order_id for the custom data.
     * @param {String} order_id The order ID for this transaction as a String.
     * Example: 'order1234'
     */
    setOrderId(order_id: string): CustomData;
    /**
     * Status of the registration in Registration event or Status of the order in Purchase/UpdateOrder event.
     * - Used only with CompleteRegistration events for Conversion API.
     * - Used only with Purchase or UpdateOrder events for Business Data API
     */
    get status(): string;
    /**
     * Sets status of the registration in Registration event or status of the order in Purchase/UpdateOrder event.
     * - Used only with CompleteRegistration events for Conversion API.
     * - Used only with Purchase or UpdateOrder events for Business Data API
     * @param status status, as a String.
     */
    set status(status: string);
    /**
     * Sets status of the registration in Registration event or status of the order in Purchase/UpdateOrder event.
     * - Used only with CompleteRegistration events for Conversion API.
     * - Used only with Purchase or UpdateOrder events for Business Data API
     * @param {String} status status, as a String.
     */
    setStatus(status: string): CustomData;
    /**
     * Gets the content name for the custom data. The name of the page or product associated with the event.
     * The name of the page or product associated with the event.
     * Example: 'lettuce'
     */
    get content_name(): string;
    /**
     * Sets the content name for the custom data.
     * @param content_name The name of the page or product associated with the event.
     * Example: 'lettuce'
     */
    set content_name(content_name: string);
    /**
     * Sets the content name for the custom data.
     * @param content_name The name of the page or product associated with the event.
     * Example: 'lettuce'
     */
    setContentName(content_name: string): CustomData;
    /**
     * Gets the content category for the custom data.
     * The category of the content associated with the event.
     * Example: 'grocery'
     */
    get content_category(): string;
    /**
     * Sets the content_category for the custom data.
     * @param content_category The category of the content associated with the event.
     * Example: 'grocery'
     */
    set content_category(content_category: string);
    /**
     * Sets the content_category for the custom data.
     * @param content_category The category of the content associated with the event.
     * Example: 'grocery'
     */
    setContentCategory(content_category: string): CustomData;
    /**
     * Gets the content_ids for the custom data.
     * The content IDs associated with the event, such as product SKUs for items in an AddToCart, represented as Array of string.
     * If content_type is a product, then your content IDs must be an array with a single string value. Otherwise, this array can contain any number of string values.
     * Example: ['ABC123', 'XYZ789']
     */
    get content_ids(): Array<string>;
    /**
     * Sets the content_ids for the custom data.
     * @param content_ids The content IDs associated with the event, such as product SKUs for items in an AddToCart, represented as Array of string.
     * If content_type is a product, then your content IDs must be an array with a single string value. Otherwise, this array can contain any number of string values.
     * Example: ['ABC123', 'XYZ789']
     */
    set content_ids(content_ids: Array<string>);
    /**
     * Sets the content_ids for the custom data.
     * @param {Array} content_ids The content IDs associated with the event, such as product SKUs for items in an AddToCart, represented as Array of string.
     * If content_type is a product, then your content IDs must be an array with a single string value. Otherwise, this array can contain any number of string values.
     * Example: ['ABC123', 'XYZ789']
     */
    setContentIds(content_ids: Array<string>): CustomData;
    /**
     * Gets the content type for the custom data.
     * A String equal to either product or product_group. Set to product if the keys you send content_ids or contents represent products.
     * Set to product_group if the keys you send in content_ids represent product groups.
     */
    get content_type(): string;
    /**
     * Sets the content type for the custom data.
     * A String equal to either product or product_group. Set to product if the keys you send content_ids or contents represent products.
     * Set to product_group if the keys you send in content_ids represent product groups.
     */
    set content_type(content_type: string);
    /**
     * Sets the content type for the custom data.
     * @param {String} content_type A string equal to either product or product_group. Set to product if the keys you send content_ids or contents represent products.
     * Set to product_group if the keys you send in content_ids represent product groups.
     */
    setContentType(content_type: string): CustomData;
    /**
     * Gets the predicted LifeTimeValue for the (user) in custom data.
     * The predicted lifetime value of a conversion event, as a String.
     * Example: '432.12'
     */
    get predicted_ltv(): number;
    /**
     * Sets the predicted LifeTimeValue for the custom data.
     * @param predicted_ltv The predicted lifetime value of a conversion event, as a String.
     * Example: '432.12'
     */
    set predicted_ltv(predicted_ltv: number);
    /**
     * Sets the predicted LifeTimeValue for the custom data.
     * @param {Number} predicted_ltv The predicted lifetime value of a conversion event, as a String.
     * Example: '432.12'
     */
    setPredictedLtv(predicted_ltv: number): CustomData;
    /**
     * Gets the number of items for the custom data.
     * The number of items that a user tries to buy during checkout. Use only with InitiateCheckout type events.
     * Example: 5
     */
    get num_items(): number;
    /**
     * Sets the number of items for the custom data.
     * @param num_items The number of items that a user tries to buy during checkout. Use only with InitiateCheckout type events.
     * Example: 5
     */
    set num_items(num_items: number);
    /**
     * Sets the number of items for the custom data.
     * @param {Number} num_items The number of items that a user tries to buy during checkout. Use only with InitiateCheckout type events.
     * Example: 5
     */
    setNumItems(num_items: number): CustomData;
    /**
     * Gets the search string for the custom data.
     * A search query made by a user.Use only with Search events.
     * Eg: 'lettuce'
     */
    get search_string(): string;
    /**
     * Sets the search string for the custom data.
     * @param {Number} search_string A search query made by a user.Use only with Search events.
     * Eg: 'lettuce'
     */
    set search_string(search_string: string);
    /**
     * Sets the search string for the custom data.
     * @param search_string A search query made by a user.Use only with Search events.
     * Eg: 'lettuce'
     */
    setSearchString(search_string: string): CustomData;
    /**
     * Gets the item number.
     */
    get item_number(): string;
    /**
     * Sets the item number.
     * @param item_number The item number.
     */
    set item_number(item_number: string);
    /**
     * Sets the item number.
     * @param {String} item_number The item number.
     */
    setItemNumber(item_number: string): CustomData;
    /**
     * Gets the delivery category.
     */
    get delivery_category(): string;
    /**
     * Sets the type of delivery for a purchase event.
     * @param delivery_category The delivery category.
     */
    set delivery_category(delivery_category: string);
    /**
     * Sets the type of delivery for a purchase event.
     * @param {String} delivery_category The delivery category.
     */
    setDeliveryCategory(delivery_category: string): CustomData;
    /**
     * Gets the custom properties to be included in the Custom Data.
     * If our predefined object properties don't suit your needs, you can include your own, custom properties. Custom properties can be used with both standard and custom events, and can help you further define custom audiences.
     * This behavior is the same for Server-Side API and Facebook Pixel.
     * @see {@link https://developers.facebook.com/docs/marketing-api/server-side-api/parameters/custom-data#custom-properties}
     * Eg: '{ 'warehouse_location' : 'washington', 'package_size' : 'L'}'
     */
    get custom_properties(): Record<string, any>;
    /**
     * Sets the custom properties to be included in the Custom Data.
     * If our predefined object properties don't suit your needs, you can include your own, custom properties. Custom properties can be used with both standard and custom events, and can help you further define custom audiences.
     * This behavior is the same for Server-Side API and Facebook Pixel.
     * @see {@link https://developers.facebook.com/docs/marketing-api/server-side-api/parameters/custom-data#custom-properties}
     * @param {Object} custom_properties custom properties property bag to be included in the Custom Data. Eg: '{ 'warehouse_location' : 'washington', 'package_size' : 'L'}'
     */
    set custom_properties(custom_properties: Record<string, any>);
    /**
     * Sets the search string for the custom data.
     * @param custom_properties A custom properties property bag to be included in the Custom Data.
     * If our predefined object properties don't suit your needs, you can include your own, custom properties. Custom properties can be used with both standard and custom events, and can help you further define custom audiences.
     * This behavior is the same for Server-Side API and Facebook Pixel.
     * @see {@link https://developers.facebook.com/docs/marketing-api/server-side-api/parameters/custom-data#custom-properties}
     * Eg: '{ 'warehouse_location' : 'washington', 'package_size' : 'L'}'
     * * @returns {Object} custom_properties property bag.
     */
    setCustomProperties(custom_properties: Record<string, any>): CustomData;
    /**
     * Adds the custom property (key, value) to the custom property bag.
     * @param {string} key The Key for the property to be added.
     * @param {string} value The Value for the property to be added.
     */
    add_custom_property(key: string, value: string): void;
    /**
     * Gets the shipping_contact for Purchase/Update Order event.
     * shipping_contact of an order
     */
    get shipping_contact(): SignalUserData;
    /**
     * Sets the shipping_contact for Purchase/Update Order event.
     * @param shipping_contact shipping contact of an order, use {SignalUserData} to build
     */
    set shipping_contact(shipping_contact: SignalUserData);
    /**
     * Sets the shipping_contact for Purchase/Update Order event.
     * @param {SignalUserData} shipping_contact shipping contact of an order, use {SignalUserData} to build
     */
    setShippingContact(shipping_contact: SignalUserData): CustomData;
    /**
     * Gets the billing_contact for Purchase/Update Order event.
     * billing_contact of an order
     */
    get billing_contact(): SignalUserData;
    /**
     * Sets the billing_contact for Purchase/Update Order event.
     * @param billing_contact billing contact of an order, use {SignalUserData} to build
     */
    set billing_contact(billing_contact: SignalUserData);
    /**
     * Sets the billing_contact for Purchase/Update Order event.
     * @param {SignalUserData} billing_contact billing contact of an order, use {SignalUserData} to build
     */
    setBillingContact(billing_contact: SignalUserData): CustomData;
    /**
     * Gets the unique id of the order.
     * Unique ID representing the order, universal across multiple categories from the business.
     */
    get external_order_id(): string;
    /**
     * Sets the unique id of the order.
     * @param external_order_id Unique ID representing the order, universal across multiple categories from the business.
     */
    set external_order_id(external_order_id: string);
    /**
     * Sets the unique id of the order.
     * @param {String} external_order_id Unique ID representing the order, universal across multiple categories from the business.
     */
    setExternalOrderId(external_order_id: string): CustomData;
    /**
     * Gets the unique id of the original order.
     * Original order id for refund. For Refund event only.
     */
    get original_order_id(): string;
    /**
     * Sets the unique id of the original order.
     * @param original_order_id Original order id for refund. For Refund event only.
     */
    set original_order_id(original_order_id: string);
    /**
     * Sets the unique id of the original order.
     * @param {String} original_order_id Original order id for refund. For Refund event only.
     */
    setOriginalOrderId(original_order_id: string): CustomData;
    /**
     * Gets the unique id of the original order.
     * Reason for refund. For Refund event only.
     */
    get message(): string;
    /**
     * Sets the unique id of the original order.
     * @param message Reason for refund. For Refund event only.
     */
    set message(message: string);
    /**
     * Sets the unique id of the original order.
     * @param {String} message Reason for refund. For Refund event only.
     */
    setMessage(message: string): CustomData;
    /**
     * Gets the constructed custom data for Business Data API
     */
    get business_data_custom_data(): BusinessDataCustomData;
    /**
     * Gets the constructed custom data for Business Data API
     */
    get server_custom_data(): ServerCustomData;
  }
}
declare module 'objects/businessdataapi/event' {
  import UserData from 'objects/businessdataapi/user-data';
  import CustomData from 'objects/businessdataapi/custom-data';
  /**
   * Event for Business Data API
   */
  export default class Event {
    _event_name: string;
    _event_time: number;
    _event_id: string;
    _user_data: UserData;
    _custom_data: CustomData;
    _data_processing_options: Array<string>;
    _data_processing_options_state: number;
    _data_processing_options_country: number;
    /**
     * @param {String} event_name A Facebook pixel Standard Event or Custom Event name.
     * @param {Number} event_time A Unix timestamp in seconds indicating when the actual event occurred.
     * @param {String} event_id This ID can be any string chosen by the advertiser.
     * @param {UserData} user_data A map that contains user data. See UserData Class for options.
     * @param {CustomData} custom_data A map that contains user data. See CustomData Class for options.
     * @param {Array<string>} data_processing_options Processing options you would like to enable for a specific event.
     * @param {Number} data_processing_options_country A country that you want to associate to this data processing option.
     * @param {Number} data_processing_options_state A state that you want to associate with this data processing option.
     */
    constructor(
      event_name: string,
      event_time: number,
      user_data: UserData,
      custom_data: CustomData,
      event_id: string,
      data_processing_options: Array<string>,
      data_processing_options_country: number,
      data_processing_options_state: number,
    );
    /**
     * Gets the Event Name for the current Event.
     */
    get event_name(): string;
    /**
     * Sets the Event Name for the current Event.
     * @param {String} event_name a Facebook pixel Standard Event or Custom Event name.
     */
    set event_name(event_name: string);
    /**
     * Gets the Event Time when the current Event happened.
     */
    get event_time(): number;
    /**
     * Sets the Event Time when the current Event happened.
     * @param {Number} event_time is a Unix timestamp in seconds indicating when the actual event occurred.
     */
    set event_time(event_time: number);
    /**
     * Gets the event_id for the current Event.
     */
    get event_id(): string;
    /**
     * Sets the event Id for the current Event.
     * @param {String} event_id Unique id for the event.
     */
    set event_id(event_id: string);
    /**
     * Gets the user data object for the current Event.
     */
    get user_data(): UserData;
    /**
     * Sets the user data object for the current Event.
     * @param {UserData} user_data user_data is an object that contains user data.
     */
    set user_data(user_data: UserData);
    /**
     * Gets the custom data object for the current Event.
     */
    get custom_data(): CustomData;
    /**
     * Sets the custom data object for the current Event.
     * @param {CustomData} custom_data is an object that includes additional business data about the event.
     */
    set custom_data(custom_data: CustomData);
    /**
     * Gets the data_processing_options for the current event.
     * Processing options you would like to enable for a specific event.
     */
    get data_processing_options(): Array<string>;
    /**
     * Sets the data_processing_options for the current event.
     * @param {Array<string>} data_processing_options represents Data processing options you would like to enable for a specific event, e.g. [] or ['LDU']
     * @see {@link https://developers.facebook.com/docs/marketing-apis/data-processing-options}
     */
    set data_processing_options(data_processing_options: Array<string>);
    /**
     * Gets the data_processing_options_country for the current event.
     * A country that you want to associate to this data processing option.
     * @see {@link https://developers.facebook.com/docs/marketing-apis/data-processing-options}
     */
    get data_processing_options_country(): number;
    /**
     * Sets the data_processing_options_country for the current event.
     * @param {number} data_processing_options_country represents country that you want to associate to this data processing option.
     */
    set data_processing_options_country(
      data_processing_options_country: number,
    );
    /**
     * Gets the data_processing_options_state for the current event.
     * A state that you want to associate with this data processing option.
     */
    get data_processing_options_state(): number;
    /**
     * Sets the data_processing_options_state for the current event.
     * @param {number} data_processing_options_state represents state that you want to associate with this data processing option.
     */
    set data_processing_options_state(data_processing_options_state: number);
    /**
     * Convert to Json object for api call
     */
    toJson(): Record<string, any>;
  }
}
declare module 'objects/signal/event' {
  import SignalUserData from 'objects/signal/user-data';
  import SignalCustomData from 'objects/signal/custom-data';
  import BusinessDataEvent from 'objects/businessdataapi/event';
  import ServerEvent from 'objects/serverside/server-event';
  /**
   * SignalEvent, event data for both Conversion API and Business Data API
   */
  export default class Event {
    _business_data_event: BusinessDataEvent;
    _server_event: ServerEvent;
    /**
     * @param {String} event_name A Facebook pixel Standard Event or Custom Event name.
     * @param {Number} event_time A Unix timestamp in seconds indicating when the actual event occurred.
     * @param {String} event_source_url The browser URL where the event happened.
     * @param {String} event_id This ID can be any string chosen by the advertiser.
     * @param {String} action_source A string that indicates where the event took place.
     * @param {Boolean} opt_out A flag that indicates we should not use this event for ads delivery optimization.
     * @param {SignalUserData} user_data SignalUserData contains user data for both Business Data API and Conversion API
     * @param {SignalCustomData} custom_data SignalCustomData contains custom data for both Business Data API and Conversion API
     * @param {Array<string>} data_processing_options Processing options you would like to enable for a specific event.
     * @param {Number} data_processing_options_country A country that you want to associate to this data processing option.
     * @param {Number} data_processing_options_state A state that you want to associate with this data processing option.
     */
    constructor(
      event_name: string,
      event_time: number,
      event_source_url: string,
      user_data: SignalUserData,
      custom_data: SignalCustomData,
      event_id: string,
      opt_out: boolean,
      action_source: string,
      data_processing_options: Array<string>,
      data_processing_options_country: number,
      data_processing_options_state: number,
    );
    /**
     * Gets the Event Name for the current Event.
     */
    get event_name(): string;
    /**
     * Sets the Event Name for the current Event.
     * @param {String} event_name a Facebook pixel Standard Event or Custom Event name.
     */
    set event_name(event_name: string);
    /**
     * Sets the Event Name for the current Event.
     * @param {String} event_name Facebook pixel Standard Event or Custom Event name.
     */
    setEventName(event_name: string): Event;
    /**
     * Gets the Event Time when the current Event happened.
     */
    get event_time(): number;
    /**
     * Sets the Event Time when the current Event happened.
     * @param {Number} event_time is a Unix timestamp in seconds indicating when the actual event occurred.
     */
    set event_time(event_time: number);
    /**
     * Sets the Event Time when the current Event happened.
     * @param {Number} event_time is a Unix timestamp in seconds indicating when the actual event occurred.
     */
    setEventTime(event_time: number): Event;
    /**
     * Gets the browser url source for the current event.
     */
    get event_source_url(): string;
    /**
     * Sets the browser url source for the current event.
     * @param {String} event_source_url The browser URL where the event happened.
     */
    set event_source_url(event_source_url: string);
    /**
     * Sets the browser url source for the current event.
     * @param {String} event_source_url The browser URL where the event happened.
     */
    setEventSourceUrl(event_source_url: string): Event;
    /**
     * Gets the event_id for the current Event.
     */
    get event_id(): string;
    /**
     * Sets the event Id for the current Event.
     * @param {String} event_id can be any string chosen by the advertiser. This is used with event_name to determine if events are identical for Conversion API.
     */
    set event_id(event_id: string);
    /**
     * Sets the event Id for the current Event.
     * @param {String} event_id can be any string chosen by the advertiser. This is used with event_name to determine if events are identical for Conversion API.
     */
    setEventId(event_id: string): Event;
    /**
     * Gets the action_source for the current event. The Action Source represents where the action took place.
     */
    get action_source(): string;
    /**
     * Sets the action_source for the current event.
     * @param {String} action_source represents where the action took place. One of {'physical_store','app','chat','email','other','phone_call','system_generated','website'}
     */
    set action_source(action_source: string);
    /**
     * Sets the action_source for the current event.
     * @param {String} action_source represents where the action took place. One of {'physical_store','app','chat','email','other','phone_call','system_generated','website'}
     */
    setActionSource(action_source: string): Event;
    /**
     * Gets the opt_out feature for the current event.opt_out is a boolean flag that indicates we should not use this event for ads delivery optimization. If set to true, we only use the event for attribution.
     */
    get opt_out(): boolean;
    /**
     * Sets the opt_out feature for the current event.
     * @param {Boolean} opt_out is a boolean flag that indicates we should not use this event for ads delivery optimization. If set to true, we only use the event for attribution.
     */
    set opt_out(opt_out: boolean);
    /**
     * Sets the opt_out feature for the current event.
     * @param {Boolean} opt_out is a boolean flag that indicates we should not use this event for ads delivery optimization. If set to true, we only use the event for attribution.
     */
    setOptOut(opt_out: boolean): Event;
    /**
     * Gets the user data objects for Business Data API and Conversion API.
     * @param user_data contains user data, use SignalUserData to construct
     */
    get user_data(): SignalUserData;
    /**
     * Sets the user data objects for Business Data API and Conversion API.
     * @param user_data contains user data, use SignalUserData to construct
     */
    set user_data(user_data: SignalUserData);
    /**
     * Sets the user data objects for Business Data API and Conversion API.
     * @param {SignalUserData} user_data contains user data, use SignalUserData to construct
     */
    setUserData(user_data: SignalUserData): Event;
    /**
     * Gets the custom data objects for Business Data API and Conversion API.
     */
    get custom_data(): SignalCustomData;
    /**
     * Sets the custom data objects for Business Data API and Conversion API.
     * @param custom_data contains custom data, use SignalCustomData to construct
     */
    set custom_data(custom_data: SignalCustomData);
    /**
     * Sets the custom data objects for Business Data API and Conversion API.
     * @param {SignalCustomData} custom_data contains custom data, use SignalCustomData to construct
     */
    setCustomData(custom_data: SignalCustomData): Event;
    /**
     * Gets the data_processing_options for the current event.
     * Processing options you would like to enable for a specific event.
     */
    get data_processing_options(): Array<string>;
    /**
     * Sets the data_processing_options for the current event.
     * @param {Array<string>} data_processing_options represents Data processing options you would like to enable for a specific event, e.g. [] or ['LDU']
     */
    set data_processing_options(data_processing_options: Array<string>);
    /**
     * Sets the data_processing_options for the current event.
     * @param {Array<string>} data_processing_options represents Data processing options you would like to enable for a specific event, e.g. [] or ['LDU']
     */
    setDataProcessingOptions(data_processing_options: Array<string>): Event;
    /**
     * Gets the data_processing_options_country for the current event.
     * A country that you want to associate to this data processing option.
     */
    get data_processing_options_country(): number;
    /**
     * Sets the data_processing_options_country for the current event.
     * @param {number} data_processing_options_country represents country that you want to associate to this data processing option.
     */
    set data_processing_options_country(
      data_processing_options_country: number,
    );
    /**
     * Sets the data_processing_options_country for the current event.
     * @param {number} data_processing_options_country represents country that you want to associate to this data processing option.
     */
    setDataProcessingOptionsCountry(
      data_processing_options_country: number,
    ): Event;
    /**
     * Gets the data_processing_options_state for the current event.
     * A state that you want to associate with this data processing option.
     */
    get data_processing_options_state(): number;
    /**
     * Sets the data_processing_options_state for the current event.
     * @param {number} data_processing_options_state represents state that you want to associate with this data processing option.
     */
    set data_processing_options_state(data_processing_options_state: number);
    /**
     * Sets the data_processing_options_state for the current event.
     * @param {number} data_processing_options_state represents state that you want to associate with this data processing option.
     */
    setDataProcessingOptionsState(data_processing_options_state: number): Event;
    /**
     * Gets the constructed custom data for Business Data API
     */
    get business_data_event(): BusinessDataEvent;
    /**
     * Gets the constructed custom data for Business Data API
     */
    get server_event(): ServerEvent;
    /**
     * Convert to Json object for api call
     */
    toJson(): Record<string, any>;
  }
}
declare module 'objects/businessdataapi/event-response' {
  /**
   * EventResponse
   */
  export default class EventResponse {
    _events_received: number;
    _events_dropped: number;
    _message: Array<Record<string, any>>;
    /**
     * @param {Number} events_received
     * @param {Number} events_dropped
     * @param {Array<Object>} message
     */
    constructor(
      events_received: number,
      events_dropped: number,
      message?: Array<Record<string, any>>,
    );
    /**
     * Gets the events received number from the Graph API Response.
     */
    get events_received(): number;
    /**
     * Sets the events received number for the Graph API Response.
     * events_received is represented by integer.
     * @param events_received representing the number of events received for the event Request
     */
    set events_received(events_received: number);
    /**
     * Sets the events received number for the Graph API Response.
     * events_received is represented by integer.
     * @param {Number} events_received representing the number of events received for the event Request
     */
    setEventsReceived(events_received: number): EventResponse;
    /**
     * Gets the events dropped number from the Graph API Response.
     */
    get events_dropped(): number;
    /**
     * Sets the events dropped number for the Graph API Response.
     * events_dropped is represented by integer.
     * @param events_dropped representing the number of events dropped during events processing
     */
    set events_dropped(events_dropped: number);
    /**
     * Sets the events dropped number for the Graph API Response.
     * events_dropped is represented by integer.
     * @param {Number} events_dropped representing the number of events dropped during events processing
     */
    setEventsDropped(events_dropped: number): EventResponse;
    /**
     * Gets the messages from the response received from Graph API.
     * @return messages in the event Response
     */
    get message(): Array<Record<string, any>>;
    /**
     * Sets the messages as array for the response received from Graph API.
     * @param message in the event Response
     */
    set message(message: Array<Record<string, any>>);
    /**
     * Sets the messages as array for the response received from Graph API.
     * @param {Array} message in the event Response
     */
    setMessage(message: Array<Record<string, any>>): EventResponse;
  }
}
declare module 'objects/businessdataapi/event-request' {
  import Event from 'objects/businessdataapi/event';
  import EventResponse from 'objects/businessdataapi/event-response';
  /**
   * EventRequest for Business Data API
   */
  export default class EventRequest {
    _access_token: string;
    _page_id: string;
    _events: Array<Event>;
    _partner_agent: string | null | undefined;
    _api: Record<string, any>;
    /**
     * @param {String} access_token Access Token for the user calling Graph API
     * @param {String} page_id Page Id to which you are sending the events
     * @param {Array<Event>} events Data for the request Payload for a Business Data Event
     * @param {String} partner_agent Platform from which the event is sent e.g. Zapier
     */
    constructor(
      access_token: string,
      page_id: string,
      events?: Array<Event>,
      partner_agent?: string | null | undefined,
    );
    /**
     * Gets the data for the request Payload for a Business Data Event.
     */
    get events(): Array<Event>;
    /**
     * Sets the events for the request Payload for a Business Data Event.
     * @param events for the current event
     */
    set events(events: Array<Event>);
    /**
     * Sets the events for the request Payload for a Business Data Event.
     * @param events for the current event
     */
    setEvents(events: Array<Event>): EventRequest;
    /**
     * Gets the partner_agent for the request
     * Allows you to specify the platform from which the event is sent e.g. Zapier
     */
    get partner_agent(): string;
    /**
     * Sets the partner_agent for the request
     * Allows you to specify the platform from which the event is sent e.g. Zapier
     * @param {String} partner_agent String value for the partner agent
     */
    set partner_agent(partner_agent: string);
    /**
     * Sets the partner_agent for the request
     * Allows you to specify the platform from which the event is sent e.g. Zapier
     * @param {String} partner_agent String value for the partner agent
     */
    setPartnerAgent(partner_agent: string): EventRequest;
    /**
     * Gets the access token for the Graph API request
     */
    get access_token(): string;
    /**
     * Sets the access token for the Graph API request
     * @param access_token string representing the access token that is used to make the Graph API.
     */
    set access_token(access_token: string);
    /**
     * Sets the access token for the Graph API request
     * @param {String} access_token string representing the access token that is used to make the Graph API.
     */
    setAccessToken(access_token: string): EventRequest;
    /**
     * Gets the page id against which we send the events
     */
    get page_id(): string;
    /**
     * Sets the page id against which we send the events
     * @param {String} page_id string value representing the page id to which you are sending the events.
     */
    set page_id(page_id: string);
    /**
     * Sets the page id against which we send the events
     * @param {String} page_id string value representing the page id to which you are sending the events.
     */
    setPageId(page_id: string): EventRequest;
    /**
     * Executes the current event_request data by making a call to the Facebook Graph API.
     */
    execute(): Promise<EventResponse>;
  }
}
declare module 'objects/signal/event-request' {
  import BusinessDataEventRequest from 'objects/businessdataapi/event-request';
  import ServerEventRequest from 'objects/serverside/event-request';
  import HttpServiceInterface from 'objects/serverside/http-service-interface';
  import SignalEvent from 'objects/signal/event';
  /**
   * EventRequest
   */
  export default class EventRequest {
    _business_data_event_request: BusinessDataEventRequest;
    _server_event_request: ServerEventRequest;
    /**
     * @param {String} access_token Access Token for the user calling Graph API
     * @param {String} pixel_id Pixel Id to which you are sending the events
     * @param {String} page_id Page Id to which you are sending the events
     * @param {Array<SignalEvent>} events Data for the request Payload for a Server Side Event
     * @param {?String} partner_agent Platform from which the event is sent e.g. wordpress
     * @param {?String} test_event_code Test Event Code used to verify that your server events are received correctly by Facebook.
     * @param {?String} namespace_id Scope used to resolve extern_id or Third-party ID. Can be another data set or data partner ID.
     * @param {?String} upload_id Unique id used to denote the current set being uploaded.
     * @param {?String} upload_tag Tag string added to track your Offline event uploads.
     * @param {?String} upload_source The origin/source of data for the dataset to be uploaded.
     * @param {Boolean} debug_mode_flag Set to true if you want to enable more logging in SDK
     * @param {?HttpServiceInterface} http_service Override the default http request method by setting an object that implements HttpServiceInterface
     */
    constructor(
      access_token: string,
      pixel_id: string,
      page_id: string,
      events?: Array<SignalEvent>,
      partner_agent?: string | null | undefined,
      test_event_code?: string | null | undefined,
      namespace_id?: string | null | undefined,
      upload_id?: string | null | undefined,
      upload_tag?: string | null | undefined,
      upload_source?: string | null | undefined,
      debug_mode_flag?: boolean,
      http_service?: HttpServiceInterface | null | undefined,
    );
    /**
     * Gets the data for the request Payload for a Server Side Event and Business Data Event.
     */
    get events(): Array<SignalEvent>;
    /**
     * Sets the events for the request Payload for a Server Side Event and Business Data Event.
     * @param events for the current event
     */
    set events(events: Array<SignalEvent>);
    /**
     * Sets the events for the request Payload for a Server Side Event and Business Data Event.
     * @param {Array<SignalEvent>} events for the current event
     */
    setEvents(events: Array<SignalEvent>): EventRequest;
    /**
     * Gets the partner_agent for the request
     * Allows you to specify the platform from which the event is sent e.g. wordpress, Zapier
     */
    get partner_agent(): string;
    /**
     * Sets the partner_agent for the request
     * Allows you to specify the platform from which the event is sent e.g. wordpress, Zapier
     * @param {String} partner_agent String value for the partner agent
     */
    set partner_agent(partner_agent: string);
    /**
     * Sets the partner_agent for the request
     * Allows you to specify the platform from which the event is sent e.g. wordpress
     * @param {String} partner_agent String value for the partner agent
     */
    setPartnerAgent(partner_agent: string): EventRequest;
    /**
     * Gets the test_event_code for the request
     */
    get test_event_code(): string;
    /**
     * Sets the test_event_code for the request
     */
    set test_event_code(test_event_code: string);
    /**
     * Sets the test_event_code for the request
     */
    setTestEventCode(test_event_code: string): EventRequest;
    /**
     * Gets the debug mode flag for the Graph API request
     */
    get debug_mode(): boolean;
    /**
     * Sets the debug mode flag for the Graph API request
     * @param debug_mode boolean value representing whether you want to send the request in debug mode to get detailed logging.
     */
    set debug_mode(debug_mode: boolean);
    /**
     * Sets the debug mode flag for the Graph API request
     * @param {Boolean} debug_mode boolean value representing whether you want to send the request in debug mode to get detailed logging.
     */
    setDebugMode(debug_mode: boolean): EventRequest;
    /**
     * Gets the access token for the Graph API request
     */
    get access_token(): string;
    /**
     * Sets the access token for the Graph API request
     * @param access_token string representing the access token that is used to make the Graph API.
     */
    set access_token(access_token: string);
    /**
     * Sets the access token for the Graph API request
     * @param {String} access_token string representing the access token that is used to make the Graph API.
     */
    setAccessToken(access_token: string): EventRequest;
    /**
     * Gets the pixel against which we send the events
     */
    get pixel_id(): string;
    /**
     * Sets the pixel against which we send the events
     * @param {String} pixel_id string value representing the Pixel's Id to which you are sending the events.
     */
    set pixel_id(pixel_id: string);
    /**
     * Sets the pixel against which we send the events
     * @param {String} pixel_id String value for the pixel_id against which you want to send the events.
     */
    setPixelId(pixel_id: string): EventRequest;
    /**
     * Gets the NamespaceId for the events
     */
    get namespace_id(): string;
    /**
     * Sets the namespace_id for the events
     * @param {String} namespace_id Scope used to resolve extern_id or Third-party ID. Can be another data set or data partner ID.
     */
    set namespace_id(namespace_id: string);
    /**
     * Sets the namespace_id for the events
     * @param {String} namespace_id Scope used to resolve extern_id or Third-party ID. Can be another data set or data partner ID.
     */
    setNamespaceId(namespace_id: string): EventRequest;
    /**
     * Gets the Upload Tag for the current events upload
     */
    get upload_tag(): string;
    /**
     * Sets the upload_tag for the current events upload
     * @param {String} upload_tag Tag string added to Track your Offline event uploads
     */
    set upload_tag(upload_tag: string);
    /**
     * Sets the upload_tag for the current events upload
     * @param {String} upload_tag Tag string added to Track your Offline event uploads
     */
    setUploadTag(upload_tag: string): EventRequest;
    /**
     * Gets the Upload Tag for the current events upload
     */
    get upload_id(): string;
    /**
     * Sets the upload_id for the current events upload
     * @param {String} upload_id Unique id used to denote the current set being uploaded
     */
    set upload_id(upload_id: string);
    /**
     * Sets the upload_id for the current events upload
     * @param {String} upload_id Unique id used to denote the current set being uploaded
     */
    setUploadId(upload_id: string): EventRequest;
    /**
     * Gets the Upload Tag for the current events upload
     */
    get upload_source(): string;
    /**
     * Sets the upload_source for the current events upload
     * @param {String} upload_source origin/source of data for the dataset to be uploaded.
     */
    set upload_source(upload_source: string);
    /**
     * Sets the upload_source for the current events upload
     * @param {String} upload_source origin/source of data for the dataset to be uploaded.
     */
    setUploadSource(upload_source: string): EventRequest;
    /**
     * Gets the http_service object for making the events request
     */
    get http_service(): HttpServiceInterface;
    /**
     * Sets the http_service object for making the events request
     * @param {HttpServiceInterface} http_service
     */
    set http_service(http_service: HttpServiceInterface);
    /**
     * Sets the http_service object for making the events request
     * @param {HttpServiceInterface} http_service
     */
    setHttpService(http_service: HttpServiceInterface): EventRequest;
    /**
     * Executes the current event_request data by making a call to the Facebook Graph API.
     */
    execute(): Promise<Record<string, any>>;
  }
}
declare module 'objects/ad-account-activity' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAccountActivity
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAccountActivity extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): AdAccountActivity;
  }
}
declare module 'objects/ad-account-content-filter-levels-inheritance' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAccountContentFilterLevelsInheritance
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAccountContentFilterLevelsInheritance extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-account-default-destination' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAccountDefaultDestination
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAccountDefaultDestination extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-account-default-objective' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAccountDefaultObjective
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAccountDefaultObjective extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get DefaultObjectiveForUser(): Record<string, any>;
    static get ObjectiveForLevel(): Record<string, any>;
  }
}
declare module 'objects/ad-account-promotable-objects' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAccountPromotableObjects
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAccountPromotableObjects extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-account-recommended-camapaign-budget' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAccountRecommendedCamapaignBudget
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAccountRecommendedCamapaignBudget extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-asset-feed-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAssetFeedSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAssetFeedSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get CallToActionTypes(): Record<string, any>;
  }
}
declare module 'objects/ad-asset-feed-spec-asset-label' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAssetFeedSpecAssetLabel
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAssetFeedSpecAssetLabel extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-asset-feed-spec-body' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAssetFeedSpecBody
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAssetFeedSpecBody extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-asset-feed-spec-caption' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAssetFeedSpecCaption
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAssetFeedSpecCaption extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-asset-feed-spec-description' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAssetFeedSpecDescription
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAssetFeedSpecDescription extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-asset-feed-spec-group-rule' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAssetFeedSpecGroupRule
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAssetFeedSpecGroupRule extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-asset-feed-spec-image' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAssetFeedSpecImage
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAssetFeedSpecImage extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-asset-feed-spec-link-url' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAssetFeedSpecLinkURL
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAssetFeedSpecLinkURL extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-asset-feed-spec-title' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAssetFeedSpecTitle
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAssetFeedSpecTitle extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-asset-feed-spec-video' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAssetFeedSpecVideo
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAssetFeedSpecVideo extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-async-request-set-notification-result' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdAsyncRequestSetNotificationResult
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdAsyncRequestSetNotificationResult extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-bid-adjustments' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdBidAdjustments
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdBidAdjustments extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-campaign-activity' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCampaignActivity
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCampaignActivity extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get BidStrategyNew(): Record<string, any>;
    static get BidStrategyOld(): Record<string, any>;
    static get BillingEventNew(): Record<string, any>;
    static get BillingEventOld(): Record<string, any>;
    static get OptimizationGoalNew(): Record<string, any>;
    static get OptimizationGoalOld(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): AdCampaignActivity;
  }
}
declare module 'objects/ad-campaign-bid-constraint' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCampaignBidConstraint
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCampaignBidConstraint extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-campaign-delivery-stats-unsupported-reasons' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCampaignDeliveryStatsUnsupportedReasons
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCampaignDeliveryStatsUnsupportedReasons extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-campaign-frequency-control-specs' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCampaignFrequencyControlSpecs
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCampaignFrequencyControlSpecs extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-campaign-group-activity' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCampaignGroupActivity
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCampaignGroupActivity extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get ObjectiveNew(): Record<string, any>;
    static get ObjectiveOld(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): AdCampaignGroupActivity;
  }
}
declare module 'objects/ad-campaign-issues-info' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCampaignIssuesInfo
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCampaignIssuesInfo extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-campaign-learning-stage-info' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCampaignLearningStageInfo
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCampaignLearningStageInfo extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-campaign-optimization-event' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCampaignOptimizationEvent
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCampaignOptimizationEvent extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-campaign-paced-bid-info' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCampaignPacedBidInfo
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCampaignPacedBidInfo extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-ad-disclaimer' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeAdDisclaimer
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeAdDisclaimer extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-collection-thumbnail-info' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeCollectionThumbnailInfo
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeCollectionThumbnailInfo extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-interactive-components-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeInteractiveComponentsSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeInteractiveComponentsSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-link-data' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeLinkData
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeLinkData extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get AttachmentStyle(): Record<string, any>;
    static get FormatOption(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-link-data-app-link-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeLinkDataAppLinkSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeLinkDataAppLinkSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-link-data-call-to-action' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeLinkDataCallToAction
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeLinkDataCallToAction extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Type(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-link-data-call-to-action-value' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeLinkDataCallToActionValue
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeLinkDataCallToActionValue extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-link-data-child-attachment' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeLinkDataChildAttachment
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeLinkDataChildAttachment extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-link-data-image-layer-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeLinkDataImageLayerSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeLinkDataImageLayerSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get BlendingMode(): Record<string, any>;
    static get FrameSource(): Record<string, any>;
    static get ImageSource(): Record<string, any>;
    static get LayerType(): Record<string, any>;
    static get OverlayPosition(): Record<string, any>;
    static get OverlayShape(): Record<string, any>;
    static get TextFont(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-link-data-image-overlay-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeLinkDataImageOverlaySpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeLinkDataImageOverlaySpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get CustomTextType(): Record<string, any>;
    static get OverlayTemplate(): Record<string, any>;
    static get Position(): Record<string, any>;
    static get TextFont(): Record<string, any>;
    static get TextType(): Record<string, any>;
    static get ThemeColor(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-link-data-sponsorship-info-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeLinkDataSponsorshipInfoSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeLinkDataSponsorshipInfoSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-link-data-template-video-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeLinkDataTemplateVideoSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeLinkDataTemplateVideoSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-object-story-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeObjectStorySpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeObjectStorySpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-omnichannel-link-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeOmnichannelLinkSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeOmnichannelLinkSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-photo-data' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativePhotoData
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativePhotoData extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-photo-data-media-elements' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativePhotoDataMediaElements
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativePhotoDataMediaElements extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-place-data' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativePlaceData
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativePlaceData extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-platform-customization' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativePlatformCustomization
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativePlatformCustomization extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-portrait-customizations' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativePortraitCustomizations
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativePortraitCustomizations extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-post-click-configuration' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativePostClickConfiguration
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativePostClickConfiguration extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-recommender-settings' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeRecommenderSettings
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeRecommenderSettings extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-static-fallback-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeStaticFallbackSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeStaticFallbackSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-template-url-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeTemplateURLSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeTemplateURLSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-text-data' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeTextData
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeTextData extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-creative-video-data' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCreativeVideoData
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCreativeVideoData extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-customization-rule-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdCustomizationRuleSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdCustomizationRuleSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-dynamic-creative' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdDynamicCreative
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdDynamicCreative extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-entity-target-spend' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdEntityTargetSpend
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdEntityTargetSpend extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-keywords' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdKeywords
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdKeywords extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-monetization-property' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * AdMonetizationProperty
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdMonetizationProperty extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getAdNetworkAnalytics(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    createAdNetworkAnalytic(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<AdMonetizationProperty>;
    getAdNetworkAnalyticsResults(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): AdMonetizationProperty;
  }
}
declare module 'objects/ad-place-page-set-metadata' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdPlacePageSetMetadata
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdPlacePageSetMetadata extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-promoted-object' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdPromotedObject
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdPromotedObject extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get CustomEventType(): Record<string, any>;
  }
}
declare module 'objects/ad-recommendation' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdRecommendation
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdRecommendation extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Confidence(): Record<string, any>;
    static get Importance(): Record<string, any>;
  }
}
declare module 'objects/ad-recommendation-data' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdRecommendationData
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdRecommendationData extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-report-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdReportSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdReportSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): AdReportSpec;
  }
}
declare module 'objects/ad-rule-evaluation-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdRuleEvaluationSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdRuleEvaluationSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get EvaluationType(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): AdRuleEvaluationSpec;
  }
}
declare module 'objects/ad-rule-execution-options' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdRuleExecutionOptions
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdRuleExecutionOptions extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Operator(): Record<string, any>;
  }
}
declare module 'objects/ad-rule-execution-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdRuleExecutionSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdRuleExecutionSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get ExecutionType(): Record<string, any>;
  }
}
declare module 'objects/ad-rule-filters' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdRuleFilters
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdRuleFilters extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Operator(): Record<string, any>;
  }
}
declare module 'objects/ad-rule-history-result' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdRuleHistoryResult
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdRuleHistoryResult extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get ObjectType(): Record<string, any>;
  }
}
declare module 'objects/ad-rule-history-result-action' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdRuleHistoryResultAction
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdRuleHistoryResultAction extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-rule-schedule' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdRuleSchedule
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdRuleSchedule extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-rule-schedule-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdRuleScheduleSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdRuleScheduleSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ad-rule-trigger' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdRuleTrigger
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdRuleTrigger extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Operator(): Record<string, any>;
    static get Type(): Record<string, any>;
  }
}
declare module 'objects/ad-study-objective-id' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdStudyObjectiveID
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdStudyObjectiveID extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/adgroup-activity' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdgroupActivity
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdgroupActivity extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get ObjectiveNew(): Record<string, any>;
    static get ObjectiveOld(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): AdgroupActivity;
  }
}
declare module 'objects/adgroup-issues-info' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdgroupIssuesInfo
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdgroupIssuesInfo extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/adgroup-placement-specific-review-feedback' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdgroupPlacementSpecificReviewFeedback
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdgroupPlacementSpecificReviewFeedback extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/adgroup-relevance-score' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdgroupRelevanceScore
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdgroupRelevanceScore extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/adgroup-review-feedback' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdgroupReviewFeedback
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdgroupReviewFeedback extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/adoptable-pet' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdoptablePet
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdoptablePet extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): AdoptablePet;
  }
}
declare module 'objects/ads-action-stats' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdsActionStats
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdsActionStats extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ads-image-crops' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdsImageCrops
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdsImageCrops extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ads-optimal-delivery-growth-opportunity' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdsOptimalDeliveryGrowthOpportunity
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdsOptimalDeliveryGrowthOpportunity extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ads-pixel-stats' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AdsPixelStats
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AdsPixelStats extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/age-range' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AgeRange
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AgeRange extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/agency-client-declaration' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AgencyClientDeclaration
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AgencyClientDeclaration extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/android-app-link' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AndroidAppLink
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AndroidAppLink extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/async-session' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AsyncSession
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AsyncSession extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): AsyncSession;
  }
}
declare module 'objects/attribution-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AttributionSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AttributionSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/audience-insights-study-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AudienceInsightsStudySpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AudienceInsightsStudySpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/audience-permission-for-actions' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AudiencePermissionForActions
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AudiencePermissionForActions extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/audio-copyright' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * AudioCopyright
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class AudioCopyright extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): AudioCopyright;
  }
}
declare module 'objects/billed-amount-details' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * BilledAmountDetails
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class BilledAmountDetails extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/brand-safety-block-list-usage' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * BrandSafetyBlockListUsage
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class BrandSafetyBlockListUsage extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/cpas-parent-catalog-settings' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CPASParentCatalogSettings
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CPASParentCatalogSettings extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get AttributionWindows(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): CPASParentCatalogSettings;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): CPASParentCatalogSettings;
  }
}
declare module 'objects/campaign-group-brand-configuration' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CampaignGroupBrandConfiguration
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CampaignGroupBrandConfiguration extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/campaign-group-collaborative-ads-partner-info' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CampaignGroupCollaborativeAdsPartnerInfo
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CampaignGroupCollaborativeAdsPartnerInfo extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/canvas-ad-settings' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CanvasAdSettings
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CanvasAdSettings extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/canvas-collection-thumbnail' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CanvasCollectionThumbnail
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CanvasCollectionThumbnail extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/catalog-based-targeting' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CatalogBasedTargeting
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CatalogBasedTargeting extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/catalog-item-app-links' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CatalogItemAppLinks
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CatalogItemAppLinks extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/catalog-item-appeal-status' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CatalogItemAppealStatus
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CatalogItemAppealStatus extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/catalog-sub-vertical-list' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CatalogSubVerticalList
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CatalogSubVerticalList extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/child-event' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ChildEvent
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ChildEvent extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/collaborative-ads-partner-info-list-item' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CollaborativeAdsPartnerInfoListItem
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CollaborativeAdsPartnerInfoListItem extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/commerce-settings' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CommerceSettings
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CommerceSettings extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/connections-targeting' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ConnectionsTargeting
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ConnectionsTargeting extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/contextual-bundling-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ContextualBundlingSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ContextualBundlingSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/conversion-action-query' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ConversionActionQuery
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ConversionActionQuery extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/copyright-reference-container' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CopyrightReferenceContainer
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CopyrightReferenceContainer extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/cover-photo' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CoverPhoto
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CoverPhoto extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/creative-history' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CreativeHistory
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CreativeHistory extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/credit-partition-action-options' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CreditPartitionActionOptions
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CreditPartitionActionOptions extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/currency' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * Currency
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Currency extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/currency-amount' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CurrencyAmount
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CurrencyAmount extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/custom-audience-ad-account' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CustomAudienceAdAccount
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CustomAudienceAdAccount extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/custom-audience-data-source' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CustomAudienceDataSource
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CustomAudienceDataSource extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get SubType(): Record<string, any>;
    static get Type(): Record<string, any>;
  }
}
declare module 'objects/custom-audience-sharing-status' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CustomAudienceSharingStatus
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CustomAudienceSharingStatus extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/custom-audience-status' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * CustomAudienceStatus
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class CustomAudienceStatus extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/day-part' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * DayPart
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class DayPart extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/delivery-check' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * DeliveryCheck
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class DeliveryCheck extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/delivery-check-extra-info' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * DeliveryCheckExtraInfo
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class DeliveryCheckExtraInfo extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/destination-catalog-settings' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * DestinationCatalogSettings
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class DestinationCatalogSettings extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): DestinationCatalogSettings;
  }
}
declare module 'objects/domain' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * Domain
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Domain extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): Domain;
  }
}
declare module 'objects/dynamic-content-set' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * DynamicContentSet
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class DynamicContentSet extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): DynamicContentSet;
  }
}
declare module 'objects/dynamic-post-child-attachment' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * DynamicPostChildAttachment
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class DynamicPostChildAttachment extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/engagement' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * Engagement
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Engagement extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/entity-at-text-range' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * EntityAtTextRange
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class EntityAtTextRange extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Type(): Record<string, any>;
  }
}
declare module 'objects/experience' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * Experience
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Experience extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/fame-export-config' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * FAMEExportConfig
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class FAMEExportConfig extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/flexible-targeting' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * FlexibleTargeting
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class FlexibleTargeting extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/funding-source-details' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * FundingSourceDetails
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class FundingSourceDetails extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/funding-source-details-coupon' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * FundingSourceDetailsCoupon
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class FundingSourceDetailsCoupon extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/id-name' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * IDName
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class IDName extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/instagram-insights-value' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * InstagramInsightsValue
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class InstagramInsightsValue extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/ios-app-link' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * IosAppLink
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class IosAppLink extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/key-value' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * KeyValue
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class KeyValue extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/lead-gen-appointment-booking-info' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * LeadGenAppointmentBookingInfo
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class LeadGenAppointmentBookingInfo extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/lead-gen-conditional-questions-group-choices' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * LeadGenConditionalQuestionsGroupChoices
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class LeadGenConditionalQuestionsGroupChoices extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/lead-gen-conditional-questions-group-questions' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * LeadGenConditionalQuestionsGroupQuestions
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class LeadGenConditionalQuestionsGroupQuestions extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/lead-gen-draft-question' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * LeadGenDraftQuestion
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class LeadGenDraftQuestion extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/lead-gen-post-submission-check-result' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * LeadGenPostSubmissionCheckResult
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class LeadGenPostSubmissionCheckResult extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/lead-gen-question' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * LeadGenQuestion
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class LeadGenQuestion extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/lead-gen-question-option' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * LeadGenQuestionOption
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class LeadGenQuestionOption extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/life-event' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  /**
   * LifeEvent
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class LifeEvent extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    getComments(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    getLikes(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): LifeEvent;
  }
}
declare module 'objects/link' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import Cursor from 'cursor';
  import Comment from 'objects/comment';
  /**
   * Link
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Link extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    createComment(
      fields: Array<string>,
      params?: Record<string, any>,
    ): Promise<Comment>;
    getLikes(
      fields: Array<string>,
      params?: Record<string, any>,
      fetchFirstPage?: boolean,
    ): Cursor | Promise<any>;
    get(fields: Array<string>, params?: Record<string, any>): Link;
  }
}
declare module 'objects/live-video-ad-break-config' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * LiveVideoAdBreakConfig
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class LiveVideoAdBreakConfig extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/live-video-targeting' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * LiveVideoTargeting
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class LiveVideoTargeting extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/location' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * Location
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Location extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/lookalike-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * LookalikeSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class LookalikeSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/mailing-address' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * MailingAddress
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class MailingAddress extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): MailingAddress;
  }
}
declare module 'objects/messenger-destination-page-welcome-message' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * MessengerDestinationPageWelcomeMessage
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class MessengerDestinationPageWelcomeMessage extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): MessengerDestinationPageWelcomeMessage;
  }
}
declare module 'objects/music-video-copyright' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * MusicVideoCopyright
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class MusicVideoCopyright extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): MusicVideoCopyright;
  }
}
declare module 'objects/native-offer-discount' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * NativeOfferDiscount
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class NativeOfferDiscount extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/offsite-pixel' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * OffsitePixel
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class OffsitePixel extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): OffsitePixel;
  }
}
declare module 'objects/open-graph-context' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * OpenGraphContext
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class OpenGraphContext extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): OpenGraphContext;
  }
}
declare module 'objects/outcome-prediction-point' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * OutcomePredictionPoint
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class OutcomePredictionPoint extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/page-admin-note' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PageAdminNote
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PageAdminNote extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): PageAdminNote;
  }
}
declare module 'objects/page-category' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PageCategory
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PageCategory extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/page-change-proposal' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PageChangeProposal
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PageChangeProposal extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/page-parking' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PageParking
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PageParking extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/page-payment-options' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PagePaymentOptions
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PagePaymentOptions extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/page-restaurant-services' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PageRestaurantServices
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PageRestaurantServices extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/page-restaurant-specialties' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PageRestaurantSpecialties
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PageRestaurantSpecialties extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/page-saved-filter' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PageSavedFilter
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PageSavedFilter extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): PageSavedFilter;
  }
}
declare module 'objects/page-start-info' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PageStartInfo
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PageStartInfo extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/page-upcoming-change' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PageUpcomingChange
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PageUpcomingChange extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): PageUpcomingChange;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): PageUpcomingChange;
  }
}
declare module 'objects/payment-pricepoints' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PaymentPricepoints
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PaymentPricepoints extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/payment-subscription' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PaymentSubscription
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PaymentSubscription extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): PaymentSubscription;
  }
}
declare module 'objects/place' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * Place
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Place extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): Place;
  }
}
declare module 'objects/place-topic' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PlaceTopic
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PlaceTopic extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(fields: Array<string>, params?: Record<string, any>): PlaceTopic;
  }
}
declare module 'objects/platform-image-source' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * PlatformImageSource
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class PlatformImageSource extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/privacy' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * Privacy
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Privacy extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/product-catalog-image-settings' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ProductCatalogImageSettings
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductCatalogImageSettings extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/product-catalog-image-settings-operation' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ProductCatalogImageSettingsOperation
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductCatalogImageSettingsOperation extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/product-feed-missing-feed-item-replacement' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ProductFeedMissingFeedItemReplacement
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductFeedMissingFeedItemReplacement extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/product-feed-upload-diagnostics-report' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ProductFeedUploadDiagnosticsReport
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductFeedUploadDiagnosticsReport extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/product-feed-upload-error-report' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ProductFeedUploadErrorReport
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductFeedUploadErrorReport extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/product-item-ar-data' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ProductItemARData
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductItemARData extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get Surfaces(): Record<string, any>;
  }
}
declare module 'objects/product-item-commerce-insights' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ProductItemCommerceInsights
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductItemCommerceInsights extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/product-set-metadata' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ProductSetMetadata
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductSetMetadata extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/product-letiant' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ProductVariant
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ProductVariant extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/raw-custom-audience' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * RawCustomAudience
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class RawCustomAudience extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/reach-frequency-activity' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ReachFrequencyActivity
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ReachFrequencyActivity extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/reach-frequency-ad-format' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ReachFrequencyAdFormat
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ReachFrequencyAdFormat extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/reach-frequency-curve-lower-confidence-range' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ReachFrequencyCurveLowerConfidenceRange
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ReachFrequencyCurveLowerConfidenceRange extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/reach-frequency-curve-upper-confidence-range' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ReachFrequencyCurveUpperConfidenceRange
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ReachFrequencyCurveUpperConfidenceRange extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/reach-frequency-day-part' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ReachFrequencyDayPart
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ReachFrequencyDayPart extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/reach-frequency-estimates-curve' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ReachFrequencyEstimatesCurve
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ReachFrequencyEstimatesCurve extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/reach-frequency-estimates-placement-breakdown' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ReachFrequencyEstimatesPlacementBreakdown
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ReachFrequencyEstimatesPlacementBreakdown extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/reach-frequency-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ReachFrequencySpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ReachFrequencySpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/referral' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  /**
   * Referral
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Referral extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(fields: Array<string>, params?: Record<string, any>): Referral;
    update(fields: Array<string>, params?: Record<string, any>): Referral;
  }
}
declare module 'objects/rev-share-policy' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * RevSharePolicy
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class RevSharePolicy extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/rich-media-element' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * RichMediaElement
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class RichMediaElement extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/saved-message-response' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * SavedMessageResponse
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class SavedMessageResponse extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): SavedMessageResponse;
  }
}
declare module 'objects/security-settings' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * SecuritySettings
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class SecuritySettings extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/split-test-winner' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * SplitTestWinner
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class SplitTestWinner extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/store-catalog-settings' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  import AbstractObject from 'abstract-object';
  /**
   * StoreCatalogSettings
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class StoreCatalogSettings extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    delete(fields: Array<string>, params?: Record<string, any>): AbstractObject;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): StoreCatalogSettings;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): StoreCatalogSettings;
  }
}
declare module 'objects/targeting' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * Targeting
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class Targeting extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    static get DevicePlatforms(): Record<string, any>;
    static get EffectiveDevicePlatforms(): Record<string, any>;
  }
}
declare module 'objects/targeting-dynamic-rule' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TargetingDynamicRule
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TargetingDynamicRule extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/targeting-geo-location' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TargetingGeoLocation
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TargetingGeoLocation extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/targeting-geo-location-city' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TargetingGeoLocationCity
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TargetingGeoLocationCity extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/targeting-geo-location-custom-location' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TargetingGeoLocationCustomLocation
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TargetingGeoLocationCustomLocation extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/targeting-geo-location-electoral-district' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TargetingGeoLocationElectoralDistrict
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TargetingGeoLocationElectoralDistrict extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/targeting-geo-location-geo-entities' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TargetingGeoLocationGeoEntities
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TargetingGeoLocationGeoEntities extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/targeting-geo-location-location-cluster' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TargetingGeoLocationLocationCluster
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TargetingGeoLocationLocationCluster extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/targeting-geo-location-location-expansion' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TargetingGeoLocationLocationExpansion
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TargetingGeoLocationLocationExpansion extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/targeting-geo-location-market' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TargetingGeoLocationMarket
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TargetingGeoLocationMarket extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/targeting-geo-location-place' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TargetingGeoLocationPlace
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TargetingGeoLocationPlace extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/targeting-geo-location-political-district' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TargetingGeoLocationPoliticalDistrict
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TargetingGeoLocationPoliticalDistrict extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/targeting-geo-location-region' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TargetingGeoLocationRegion
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TargetingGeoLocationRegion extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/targeting-geo-location-zip' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TargetingGeoLocationZip
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TargetingGeoLocationZip extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/targeting-product-audience-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TargetingProductAudienceSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TargetingProductAudienceSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/targeting-product-audience-sub-spec' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TargetingProductAudienceSubSpec
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TargetingProductAudienceSubSpec extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/targeting-prospecting-audience' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TargetingProspectingAudience
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TargetingProspectingAudience extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/targeting-relaxation' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TargetingRelaxation
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TargetingRelaxation extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/tracking-and-conversion-with-defaults' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * TrackingAndConversionWithDefaults
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class TrackingAndConversionWithDefaults extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/user-cover-photo' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * UserCoverPhoto
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class UserCoverPhoto extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/user-device' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * UserDevice
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class UserDevice extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/user-lead-gen-disclaimer-response' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * UserLeadGenDisclaimerResponse
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class UserLeadGenDisclaimerResponse extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/user-lead-gen-field-data' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * UserLeadGenFieldData
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class UserLeadGenFieldData extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/user-payment-mobile-pricepoints' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * UserPaymentMobilePricepoints
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class UserPaymentMobilePricepoints extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/value-based-eligible-source' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * ValueBasedEligibleSource
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class ValueBasedEligibleSource extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/video-copyright-condition-group' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * VideoCopyrightConditionGroup
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class VideoCopyrightConditionGroup extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/video-copyright-geo-gate' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * VideoCopyrightGeoGate
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class VideoCopyrightGeoGate extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/video-copyright-segment' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * VideoCopyrightSegment
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class VideoCopyrightSegment extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/video-upload-limits' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * VideoUploadLimits
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class VideoUploadLimits extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/voip-info' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * VoipInfo
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class VoipInfo extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/web-app-link' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * WebAppLink
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class WebAppLink extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/whats-app-business-profile' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * WhatsAppBusinessProfile
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class WhatsAppBusinessProfile extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
    get(
      fields: Array<string>,
      params?: Record<string, any>,
    ): WhatsAppBusinessProfile;
    update(
      fields: Array<string>,
      params?: Record<string, any>,
    ): WhatsAppBusinessProfile;
  }
}
declare module 'objects/windows-app-link' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * WindowsAppLink
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class WindowsAppLink extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/windows-phone-app-link' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * WindowsPhoneAppLink
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class WindowsPhoneAppLink extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'objects/work-user-frontline' {
  import { AbstractCrudObject } from 'abstract-crud-object';
  /**
   * WorkUserFrontline
   * @extends AbstractCrudObject
   * @see {@link https://developers.facebook.com/docs/marketing-api/}
   */
  export default class WorkUserFrontline extends AbstractCrudObject {
    static get Fields(): Record<string, any>;
  }
}
declare module 'facebook-nodejs-business-sdk' {
  /**
   * Copyright (c) 2017-present, Facebook, Inc.
   * All rights reserved.
   *
   * This source code is licensed under the license found in the
   * LICENSE file in the root directory of this source tree.
   */
  export { default as FacebookAdsApi } from 'api';
  export { default as FacebookAdsApiBatch } from 'api-batch';
  export { default as AbstractCrudObject } from 'abstract-crud-object';
  export { default as APIRequest } from 'api-request';
  export { default as APIResponse } from 'api-response';
  export { default as CrashReporter } from 'crash-reporter';
  export { default as Content } from 'objects/serverside/content';
  export { default as CustomData } from 'objects/serverside/custom-data';
  export { default as EventRequest } from 'objects/serverside/event-request';
  export { default as EventResponse } from 'objects/serverside/event-response';
  export { default as ServerEvent } from 'objects/serverside/server-event';
  export { default as UserData } from 'objects/serverside/user-data';
  export { default as DeliveryCategory } from 'objects/serverside/delivery-category';
  export { default as HttpMethod } from 'objects/serverside/http-method';
  export { default as HttpServiceClientConfig } from 'objects/serverside/http-service-client-config';
  export { default as HttpServiceInterface } from 'objects/serverside/http-service-interface';
  export { default as BatchProcessor } from 'objects/serverside/batch-processor';
  export { default as ServerSideUtils } from 'objects/serverside/utils';
  export { default as SignalUserData } from 'objects/signal/user-data';
  export { default as SignalContent } from 'objects/signal/content';
  export { default as SignalCustomData } from 'objects/signal/custom-data';
  export { default as SignalEvent } from 'objects/signal/event';
  export { default as SignalEventRequest } from 'objects/signal/event-request';
  export { default as Ad } from 'objects/ad';
  export { default as AdAccount } from 'objects/ad-account';
  export { default as AdAccountActivity } from 'objects/ad-account-activity';
  export { default as AdAccountAdRulesHistory } from 'objects/ad-account-ad-rules-history';
  export { default as AdAccountAdVolume } from 'objects/ad-account-ad-volume';
  export { default as AdAccountContentFilterLevelsInheritance } from 'objects/ad-account-content-filter-levels-inheritance';
  export { default as AdAccountDefaultDestination } from 'objects/ad-account-default-destination';
  export { default as AdAccountDefaultObjective } from 'objects/ad-account-default-objective';
  export { default as AdAccountDeliveryEstimate } from 'objects/ad-account-delivery-estimate';
  export { default as AdAccountMatchedSearchApplicationsEdgeData } from 'objects/ad-account-matched-search-applications-edge-data';
  export { default as AdAccountMaxBid } from 'objects/ad-account-max-bid';
  export { default as AdAccountPromotableObjects } from 'objects/ad-account-promotable-objects';
  export { default as AdAccountReachEstimate } from 'objects/ad-account-reach-estimate';
  export { default as AdAccountRecommendedCamapaignBudget } from 'objects/ad-account-recommended-camapaign-budget';
  export { default as AdAccountSubscribedApps } from 'objects/ad-account-subscribed-apps';
  export { default as AdAccountTargetingUnified } from 'objects/ad-account-targeting-unified';
  export { default as AdAccountTrackingData } from 'objects/ad-account-tracking-data';
  export { default as AdAccountUser } from 'objects/ad-account-user';
  export { default as AdActivity } from 'objects/ad-activity';
  export { default as AdAssetFeedSpec } from 'objects/ad-asset-feed-spec';
  export { default as AdAssetFeedSpecAssetLabel } from 'objects/ad-asset-feed-spec-asset-label';
  export { default as AdAssetFeedSpecBody } from 'objects/ad-asset-feed-spec-body';
  export { default as AdAssetFeedSpecCaption } from 'objects/ad-asset-feed-spec-caption';
  export { default as AdAssetFeedSpecDescription } from 'objects/ad-asset-feed-spec-description';
  export { default as AdAssetFeedSpecGroupRule } from 'objects/ad-asset-feed-spec-group-rule';
  export { default as AdAssetFeedSpecImage } from 'objects/ad-asset-feed-spec-image';
  export { default as AdAssetFeedSpecLinkURL } from 'objects/ad-asset-feed-spec-link-url';
  export { default as AdAssetFeedSpecTitle } from 'objects/ad-asset-feed-spec-title';
  export { default as AdAssetFeedSpecVideo } from 'objects/ad-asset-feed-spec-video';
  export { default as AdAsyncRequest } from 'objects/ad-async-request';
  export { default as AdAsyncRequestSet } from 'objects/ad-async-request-set';
  export { default as AdAsyncRequestSetNotificationResult } from 'objects/ad-async-request-set-notification-result';
  export { default as AdBidAdjustments } from 'objects/ad-bid-adjustments';
  export { default as AdCampaignActivity } from 'objects/ad-campaign-activity';
  export { default as AdCampaignBidConstraint } from 'objects/ad-campaign-bid-constraint';
  export { default as AdCampaignDeliveryEstimate } from 'objects/ad-campaign-delivery-estimate';
  export { default as AdCampaignDeliveryStatsUnsupportedReasons } from 'objects/ad-campaign-delivery-stats-unsupported-reasons';
  export { default as AdCampaignFrequencyControlSpecs } from 'objects/ad-campaign-frequency-control-specs';
  export { default as AdCampaignGroupActivity } from 'objects/ad-campaign-group-activity';
  export { default as AdCampaignIssuesInfo } from 'objects/ad-campaign-issues-info';
  export { default as AdCampaignLearningStageInfo } from 'objects/ad-campaign-learning-stage-info';
  export { default as AdCampaignOptimizationEvent } from 'objects/ad-campaign-optimization-event';
  export { default as AdCampaignPacedBidInfo } from 'objects/ad-campaign-paced-bid-info';
  export { default as AdCreative } from 'objects/ad-creative';
  export { default as AdCreativeAdDisclaimer } from 'objects/ad-creative-ad-disclaimer';
  export { default as AdCreativeCollectionThumbnailInfo } from 'objects/ad-creative-collection-thumbnail-info';
  export { default as AdCreativeInsights } from 'objects/ad-creative-insights';
  export { default as AdCreativeInteractiveComponentsSpec } from 'objects/ad-creative-interactive-components-spec';
  export { default as AdCreativeLinkData } from 'objects/ad-creative-link-data';
  export { default as AdCreativeLinkDataAppLinkSpec } from 'objects/ad-creative-link-data-app-link-spec';
  export { default as AdCreativeLinkDataCallToAction } from 'objects/ad-creative-link-data-call-to-action';
  export { default as AdCreativeLinkDataCallToActionValue } from 'objects/ad-creative-link-data-call-to-action-value';
  export { default as AdCreativeLinkDataChildAttachment } from 'objects/ad-creative-link-data-child-attachment';
  export { default as AdCreativeLinkDataImageLayerSpec } from 'objects/ad-creative-link-data-image-layer-spec';
  export { default as AdCreativeLinkDataImageOverlaySpec } from 'objects/ad-creative-link-data-image-overlay-spec';
  export { default as AdCreativeLinkDataSponsorshipInfoSpec } from 'objects/ad-creative-link-data-sponsorship-info-spec';
  export { default as AdCreativeLinkDataTemplateVideoSpec } from 'objects/ad-creative-link-data-template-video-spec';
  export { default as AdCreativeObjectStorySpec } from 'objects/ad-creative-object-story-spec';
  export { default as AdCreativeOmnichannelLinkSpec } from 'objects/ad-creative-omnichannel-link-spec';
  export { default as AdCreativePhotoData } from 'objects/ad-creative-photo-data';
  export { default as AdCreativePhotoDataMediaElements } from 'objects/ad-creative-photo-data-media-elements';
  export { default as AdCreativePlaceData } from 'objects/ad-creative-place-data';
  export { default as AdCreativePlatformCustomization } from 'objects/ad-creative-platform-customization';
  export { default as AdCreativePortraitCustomizations } from 'objects/ad-creative-portrait-customizations';
  export { default as AdCreativePostClickConfiguration } from 'objects/ad-creative-post-click-configuration';
  export { default as AdCreativeRecommenderSettings } from 'objects/ad-creative-recommender-settings';
  export { default as AdCreativeStaticFallbackSpec } from 'objects/ad-creative-static-fallback-spec';
  export { default as AdCreativeTemplateURLSpec } from 'objects/ad-creative-template-url-spec';
  export { default as AdCreativeTextData } from 'objects/ad-creative-text-data';
  export { default as AdCreativeVideoData } from 'objects/ad-creative-video-data';
  export { default as AdCustomizationRuleSpec } from 'objects/ad-customization-rule-spec';
  export { default as AdDynamicCreative } from 'objects/ad-dynamic-creative';
  export { default as AdEntityTargetSpend } from 'objects/ad-entity-target-spend';
  export { default as AdImage } from 'objects/ad-image';
  export { default as AdKeywords } from 'objects/ad-keywords';
  export { default as AdLabel } from 'objects/ad-label';
  export { default as AdMonetizationProperty } from 'objects/ad-monetization-property';
  export { default as AdNetworkAnalyticsAsyncQueryResult } from 'objects/ad-network-analytics-async-query-result';
  export { default as AdNetworkAnalyticsSyncQueryResult } from 'objects/ad-network-analytics-sync-query-result';
  export { default as AdPlacePageSet } from 'objects/ad-place-page-set';
  export { default as AdPlacePageSetMetadata } from 'objects/ad-place-page-set-metadata';
  export { default as AdPlacement } from 'objects/ad-placement';
  export { default as AdPreview } from 'objects/ad-preview';
  export { default as AdPromotedObject } from 'objects/ad-promoted-object';
  export { default as AdRecommendation } from 'objects/ad-recommendation';
  export { default as AdRecommendationData } from 'objects/ad-recommendation-data';
  export { default as AdReportRun } from 'objects/ad-report-run';
  export { default as AdReportSpec } from 'objects/ad-report-spec';
  export { default as AdRule } from 'objects/ad-rule';
  export { default as AdRuleEvaluationSpec } from 'objects/ad-rule-evaluation-spec';
  export { default as AdRuleExecutionOptions } from 'objects/ad-rule-execution-options';
  export { default as AdRuleExecutionSpec } from 'objects/ad-rule-execution-spec';
  export { default as AdRuleFilters } from 'objects/ad-rule-filters';
  export { default as AdRuleHistory } from 'objects/ad-rule-history';
  export { default as AdRuleHistoryResult } from 'objects/ad-rule-history-result';
  export { default as AdRuleHistoryResultAction } from 'objects/ad-rule-history-result-action';
  export { default as AdRuleSchedule } from 'objects/ad-rule-schedule';
  export { default as AdRuleScheduleSpec } from 'objects/ad-rule-schedule-spec';
  export { default as AdRuleTrigger } from 'objects/ad-rule-trigger';
  export { default as AdSet } from 'objects/ad-set';
  export { default as AdStudy } from 'objects/ad-study';
  export { default as AdStudyCell } from 'objects/ad-study-cell';
  export { default as AdStudyObjective } from 'objects/ad-study-objective';
  export { default as AdStudyObjectiveID } from 'objects/ad-study-objective-id';
  export { default as AdVideo } from 'objects/ad-video';
  export { default as AdgroupActivity } from 'objects/adgroup-activity';
  export { default as AdgroupIssuesInfo } from 'objects/adgroup-issues-info';
  export { default as AdgroupPlacementSpecificReviewFeedback } from 'objects/adgroup-placement-specific-review-feedback';
  export { default as AdgroupRelevanceScore } from 'objects/adgroup-relevance-score';
  export { default as AdgroupReviewFeedback } from 'objects/adgroup-review-feedback';
  export { default as AdoptablePet } from 'objects/adoptable-pet';
  export { default as AdsActionStats } from 'objects/ads-action-stats';
  export { default as AdsImageCrops } from 'objects/ads-image-crops';
  export { default as AdsInsights } from 'objects/ads-insights';
  export { default as AdsOptimalDeliveryGrowthOpportunity } from 'objects/ads-optimal-delivery-growth-opportunity';
  export { default as AdsPixel } from 'objects/ads-pixel';
  export { default as AdsPixelStats } from 'objects/ads-pixel-stats';
  export { default as AdsPixelStatsResult } from 'objects/ads-pixel-stats-result';
  export { default as AgeRange } from 'objects/age-range';
  export { default as AgencyClientDeclaration } from 'objects/agency-client-declaration';
  export { default as Album } from 'objects/album';
  export { default as AndroidAppLink } from 'objects/android-app-link';
  export { default as AppRequest } from 'objects/app-request';
  export { default as AppRequestFormerRecipient } from 'objects/app-request-former-recipient';
  export { default as Application } from 'objects/application';
  export { default as AssignedUser } from 'objects/assigned-user';
  export { default as AsyncRequest } from 'objects/async-request';
  export { default as AsyncSession } from 'objects/async-session';
  export { default as AtlasCampaign } from 'objects/atlas-campaign';
  export { default as AttributionSpec } from 'objects/attribution-spec';
  export { default as AudienceInsightsStudySpec } from 'objects/audience-insights-study-spec';
  export { default as AudiencePermissionForActions } from 'objects/audience-permission-for-actions';
  export { default as AudioCopyright } from 'objects/audio-copyright';
  export { default as AutomotiveModel } from 'objects/automotive-model';
  export { default as BilledAmountDetails } from 'objects/billed-amount-details';
  export { default as BrandSafetyBlockListUsage } from 'objects/brand-safety-block-list-usage';
  export { default as BroadTargetingCategories } from 'objects/broad-targeting-categories';
  export { default as Business } from 'objects/business';
  export { default as BusinessAdAccountRequest } from 'objects/business-ad-account-request';
  export { default as BusinessAgreement } from 'objects/business-agreement';
  export { default as BusinessApplicationRequest } from 'objects/business-application-request';
  export { default as BusinessAssetGroup } from 'objects/business-asset-group';
  export { default as BusinessAssetSharingAgreement } from 'objects/business-asset-sharing-agreement';
  export { default as BusinessOwnedObjectOnBehalfOfRequest } from 'objects/business-owned-object-on-behalf-of-request';
  export { default as BusinessPageRequest } from 'objects/business-page-request';
  export { default as BusinessRoleRequest } from 'objects/business-role-request';
  export { default as BusinessUnit } from 'objects/business-unit';
  export { default as BusinessUser } from 'objects/business-user';
  export { default as CPASAdvertiserPartnershipRecommendation } from 'objects/cpas-advertiser-partnership-recommendation';
  export { default as CPASCollaborationRequest } from 'objects/cpas-collaboration-request';
  export { default as CPASParentCatalogSettings } from 'objects/cpas-parent-catalog-settings';
  export { default as Campaign } from 'objects/campaign';
  export { default as CampaignGroupBrandConfiguration } from 'objects/campaign-group-brand-configuration';
  export { default as CampaignGroupCollaborativeAdsPartnerInfo } from 'objects/campaign-group-collaborative-ads-partner-info';
  export { default as Canvas } from 'objects/canvas';
  export { default as CanvasAdSettings } from 'objects/canvas-ad-settings';
  export { default as CanvasBodyElement } from 'objects/canvas-body-element';
  export { default as CanvasCollectionThumbnail } from 'objects/canvas-collection-thumbnail';
  export { default as CatalogBasedTargeting } from 'objects/catalog-based-targeting';
  export { default as CatalogItemAppLinks } from 'objects/catalog-item-app-links';
  export { default as CatalogItemAppealStatus } from 'objects/catalog-item-appeal-status';
  export { default as CatalogItemChannelsToIntegrityStatus } from 'objects/catalog-item-channels-to-integrity-status';
  export { default as CatalogSubVerticalList } from 'objects/catalog-sub-vertical-list';
  export { default as CheckBatchRequestStatus } from 'objects/check-batch-request-status';
  export { default as ChildEvent } from 'objects/child-event';
  export { default as CollaborativeAdsPartnerInfoListItem } from 'objects/collaborative-ads-partner-info-list-item';
  export { default as CollaborativeAdsShareSettings } from 'objects/collaborative-ads-share-settings';
  export { default as Comment } from 'objects/comment';
  export { default as CommerceMerchantSettings } from 'objects/commerce-merchant-settings';
  export { default as CommerceMerchantSettingsSetupStatus } from 'objects/commerce-merchant-settings-setup-status';
  export { default as CommerceOrder } from 'objects/commerce-order';
  export { default as CommerceOrderTransactionDetail } from 'objects/commerce-order-transaction-detail';
  export { default as CommercePayout } from 'objects/commerce-payout';
  export { default as CommerceSettings } from 'objects/commerce-settings';
  export { default as ConnectionsTargeting } from 'objects/connections-targeting';
  export { default as ContentDeliveryReport } from 'objects/content-delivery-report';
  export { default as ContextualBundlingSpec } from 'objects/contextual-bundling-spec';
  export { default as ConversionActionQuery } from 'objects/conversion-action-query';
  export { default as CopyrightReferenceContainer } from 'objects/copyright-reference-container';
  export { default as CoverPhoto } from 'objects/cover-photo';
  export { default as CreativeHistory } from 'objects/creative-history';
  export { default as CreditPartitionActionOptions } from 'objects/credit-partition-action-options';
  export { default as Currency } from 'objects/currency';
  export { default as CurrencyAmount } from 'objects/currency-amount';
  export { default as CustomAudience } from 'objects/custom-audience';
  export { default as CustomAudienceAdAccount } from 'objects/custom-audience-ad-account';
  export { default as CustomAudienceDataSource } from 'objects/custom-audience-data-source';
  export { default as CustomAudienceSession } from 'objects/custom-audience-session';
  export { default as CustomAudienceSharingStatus } from 'objects/custom-audience-sharing-status';
  export { default as CustomAudienceStatus } from 'objects/custom-audience-status';
  export { default as CustomAudiencesTOS } from 'objects/custom-audiences-tos';
  export { default as CustomAudiencesharedAccountInfo } from 'objects/custom-audienceshared-account-info';
  export { default as CustomConversion } from 'objects/custom-conversion';
  export { default as CustomConversionStatsResult } from 'objects/custom-conversion-stats-result';
  export { default as CustomUserSettings } from 'objects/custom-user-settings';
  export { default as DACheck } from 'objects/da-check';
  export { default as DayPart } from 'objects/day-part';
  export { default as DeliveryCheck } from 'objects/delivery-check';
  export { default as DeliveryCheckExtraInfo } from 'objects/delivery-check-extra-info';
  export { default as Destination } from 'objects/destination';
  export { default as DestinationCatalogSettings } from 'objects/destination-catalog-settings';
  export { default as Domain } from 'objects/domain';
  export { default as DynamicContentSet } from 'objects/dynamic-content-set';
  export { default as DynamicPostChildAttachment } from 'objects/dynamic-post-child-attachment';
  export { default as DynamicPriceConfigByDate } from 'objects/dynamic-price-config-by-date';
  export { default as Engagement } from 'objects/engagement';
  export { default as EntityAtTextRange } from 'objects/entity-at-text-range';
  export { default as Event } from 'objects/event';
  export { default as EventSourceGroup } from 'objects/event-source-group';
  export { default as Experience } from 'objects/experience';
  export { default as ExtendedCredit } from 'objects/extended-credit';
  export { default as ExtendedCreditAllocationConfig } from 'objects/extended-credit-allocation-config';
  export { default as ExtendedCreditInvoiceGroup } from 'objects/extended-credit-invoice-group';
  export { default as ExternalEventSource } from 'objects/external-event-source';
  export { default as FAMEExportConfig } from 'objects/fame-export-config';
  export { default as FlexibleTargeting } from 'objects/flexible-targeting';
  export { default as Flight } from 'objects/flight';
  export { default as FundingSourceDetails } from 'objects/funding-source-details';
  export { default as FundingSourceDetailsCoupon } from 'objects/funding-source-details-coupon';
  export { default as Group } from 'objects/group';
  export { default as HomeListing } from 'objects/home-listing';
  export { default as Hotel } from 'objects/hotel';
  export { default as HotelRoom } from 'objects/hotel-room';
  export { default as IDName } from 'objects/id-name';
  export { default as IGComment } from 'objects/ig-comment';
  export { default as IGMedia } from 'objects/ig-media';
  export { default as IGUser } from 'objects/ig-user';
  export { default as ImageCopyright } from 'objects/image-copyright';
  export { default as InsightsResult } from 'objects/insights-result';
  export { default as InstagramInsightsResult } from 'objects/instagram-insights-result';
  export { default as InstagramInsightsValue } from 'objects/instagram-insights-value';
  export { default as InstagramUser } from 'objects/instagram-user';
  export { default as InstantArticle } from 'objects/instant-article';
  export { default as InstantArticleInsightsQueryResult } from 'objects/instant-article-insights-query-result';
  export { default as InvoiceCampaign } from 'objects/invoice-campaign';
  export { default as IosAppLink } from 'objects/ios-app-link';
  export { default as KeyValue } from 'objects/key-value';
  export { default as Lead } from 'objects/lead';
  export { default as LeadGenAppointmentBookingInfo } from 'objects/lead-gen-appointment-booking-info';
  export { default as LeadGenConditionalQuestionsGroupChoices } from 'objects/lead-gen-conditional-questions-group-choices';
  export { default as LeadGenConditionalQuestionsGroupQuestions } from 'objects/lead-gen-conditional-questions-group-questions';
  export { default as LeadGenDraftQuestion } from 'objects/lead-gen-draft-question';
  export { default as LeadGenPostSubmissionCheckResult } from 'objects/lead-gen-post-submission-check-result';
  export { default as LeadGenQuestion } from 'objects/lead-gen-question';
  export { default as LeadGenQuestionOption } from 'objects/lead-gen-question-option';
  export { default as LeadgenForm } from 'objects/leadgen-form';
  export { default as LifeEvent } from 'objects/life-event';
  export { default as Link } from 'objects/link';
  export { default as LiveEncoder } from 'objects/live-encoder';
  export { default as LiveVideo } from 'objects/live-video';
  export { default as LiveVideoAdBreakConfig } from 'objects/live-video-ad-break-config';
  export { default as LiveVideoError } from 'objects/live-video-error';
  export { default as LiveVideoInputStream } from 'objects/live-video-input-stream';
  export { default as LiveVideoTargeting } from 'objects/live-video-targeting';
  export { default as Location } from 'objects/location';
  export { default as LookalikeSpec } from 'objects/lookalike-spec';
  export { default as MailingAddress } from 'objects/mailing-address';
  export { default as MeasurementUploadEvent } from 'objects/measurement-upload-event';
  export { default as MediaFingerprint } from 'objects/media-fingerprint';
  export { default as MessagingFeatureReview } from 'objects/messaging-feature-review';
  export { default as MessengerDestinationPageWelcomeMessage } from 'objects/messenger-destination-page-welcome-message';
  export { default as MessengerProfile } from 'objects/messenger-profile';
  export { default as MinimumBudget } from 'objects/minimum-budget';
  export { default as MusicVideoCopyright } from 'objects/music-video-copyright';
  export { default as NativeOffer } from 'objects/native-offer';
  export { default as NativeOfferDiscount } from 'objects/native-offer-discount';
  export { default as NativeOfferView } from 'objects/native-offer-view';
  export { default as NullNode } from 'objects/null-node';
  export { default as OfflineConversionDataSet } from 'objects/offline-conversion-data-set';
  export { default as OffsitePixel } from 'objects/offsite-pixel';
  export { default as OpenGraphContext } from 'objects/open-graph-context';
  export { default as OracleTransaction } from 'objects/oracle-transaction';
  export { default as OutcomePredictionPoint } from 'objects/outcome-prediction-point';
  export { default as Page } from 'objects/page';
  export { default as PageAdminNote } from 'objects/page-admin-note';
  export { default as PageCallToAction } from 'objects/page-call-to-action';
  export { default as PageCategory } from 'objects/page-category';
  export { default as PageChangeProposal } from 'objects/page-change-proposal';
  export { default as PageCommerceEligibility } from 'objects/page-commerce-eligibility';
  export { default as PageParking } from 'objects/page-parking';
  export { default as PagePaymentOptions } from 'objects/page-payment-options';
  export { default as PagePost } from 'objects/page-post';
  export { default as PageRestaurantServices } from 'objects/page-restaurant-services';
  export { default as PageRestaurantSpecialties } from 'objects/page-restaurant-specialties';
  export { default as PageSavedFilter } from 'objects/page-saved-filter';
  export { default as PageSettings } from 'objects/page-settings';
  export { default as PageStartInfo } from 'objects/page-start-info';
  export { default as PageThreadOwner } from 'objects/page-thread-owner';
  export { default as PageUpcomingChange } from 'objects/page-upcoming-change';
  export { default as PageUserMessageThreadLabel } from 'objects/page-user-message-thread-label';
  export { default as PartnerStudy } from 'objects/partner-study';
  export { default as PaymentEnginePayment } from 'objects/payment-engine-payment';
  export { default as PaymentPricepoints } from 'objects/payment-pricepoints';
  export { default as PaymentSubscription } from 'objects/payment-subscription';
  export { default as Permission } from 'objects/permission';
  export { default as Persona } from 'objects/persona';
  export { default as Photo } from 'objects/photo';
  export { default as Place } from 'objects/place';
  export { default as PlaceTopic } from 'objects/place-topic';
  export { default as PlatformImageSource } from 'objects/platform-image-source';
  export { default as PlayableContent } from 'objects/playable-content';
  export { default as Post } from 'objects/post';
  export { default as Privacy } from 'objects/privacy';
  export { default as ProductCatalog } from 'objects/product-catalog';
  export { default as ProductCatalogCategory } from 'objects/product-catalog-category';
  export { default as ProductCatalogHotelRoomsBatch } from 'objects/product-catalog-hotel-rooms-batch';
  export { default as ProductCatalogImageSettings } from 'objects/product-catalog-image-settings';
  export { default as ProductCatalogImageSettingsOperation } from 'objects/product-catalog-image-settings-operation';
  export { default as ProductCatalogPricingVariablesBatch } from 'objects/product-catalog-pricing-letiables-batch';
  export { default as ProductCatalogProductSetsBatch } from 'objects/product-catalog-product-sets-batch';
  export { default as ProductEventStat } from 'objects/product-event-stat';
  export { default as ProductFeed } from 'objects/product-feed';
  export { default as ProductFeedMissingFeedItemReplacement } from 'objects/product-feed-missing-feed-item-replacement';
  export { default as ProductFeedRule } from 'objects/product-feed-rule';
  export { default as ProductFeedRuleSuggestion } from 'objects/product-feed-rule-suggestion';
  export { default as ProductFeedSchedule } from 'objects/product-feed-schedule';
  export { default as ProductFeedUpload } from 'objects/product-feed-upload';
  export { default as ProductFeedUploadDiagnosticsReport } from 'objects/product-feed-upload-diagnostics-report';
  export { default as ProductFeedUploadError } from 'objects/product-feed-upload-error';
  export { default as ProductFeedUploadErrorReport } from 'objects/product-feed-upload-error-report';
  export { default as ProductFeedUploadErrorSample } from 'objects/product-feed-upload-error-sample';
  export { default as ProductGroup } from 'objects/product-group';
  export { default as ProductItem } from 'objects/product-item';
  export { default as ProductItemARData } from 'objects/product-item-ar-data';
  export { default as ProductItemCommerceInsights } from 'objects/product-item-commerce-insights';
  export { default as ProductSet } from 'objects/product-set';
  export { default as ProductSetMetadata } from 'objects/product-set-metadata';
  export { default as ProductVariant } from 'objects/product-letiant';
  export { default as Profile } from 'objects/profile';
  export { default as ProfilePictureSource } from 'objects/profile-picture-source';
  export { default as PublisherBlockList } from 'objects/publisher-block-list';
  export { default as RTBDynamicPost } from 'objects/rtb-dynamic-post';
  export { default as RawCustomAudience } from 'objects/raw-custom-audience';
  export { default as ReachFrequencyActivity } from 'objects/reach-frequency-activity';
  export { default as ReachFrequencyAdFormat } from 'objects/reach-frequency-ad-format';
  export { default as ReachFrequencyCurveLowerConfidenceRange } from 'objects/reach-frequency-curve-lower-confidence-range';
  export { default as ReachFrequencyCurveUpperConfidenceRange } from 'objects/reach-frequency-curve-upper-confidence-range';
  export { default as ReachFrequencyDayPart } from 'objects/reach-frequency-day-part';
  export { default as ReachFrequencyEstimatesCurve } from 'objects/reach-frequency-estimates-curve';
  export { default as ReachFrequencyEstimatesPlacementBreakdown } from 'objects/reach-frequency-estimates-placement-breakdown';
  export { default as ReachFrequencyPrediction } from 'objects/reach-frequency-prediction';
  export { default as ReachFrequencySpec } from 'objects/reach-frequency-spec';
  export { default as Recommendation } from 'objects/recommendation';
  export { default as Referral } from 'objects/referral';
  export { default as RevSharePolicy } from 'objects/rev-share-policy';
  export { default as RichMediaElement } from 'objects/rich-media-element';
  export { default as SavedAudience } from 'objects/saved-audience';
  export { default as SavedMessageResponse } from 'objects/saved-message-response';
  export { default as SecuritySettings } from 'objects/security-settings';
  export { default as SplitTestWinner } from 'objects/split-test-winner';
  export { default as StoreCatalogSettings } from 'objects/store-catalog-settings';
  export { default as SystemUser } from 'objects/system-user';
  export { default as Tab } from 'objects/tab';
  export { default as Targeting } from 'objects/targeting';
  export { default as TargetingDynamicRule } from 'objects/targeting-dynamic-rule';
  export { default as TargetingGeoLocation } from 'objects/targeting-geo-location';
  export { default as TargetingGeoLocationCity } from 'objects/targeting-geo-location-city';
  export { default as TargetingGeoLocationCustomLocation } from 'objects/targeting-geo-location-custom-location';
  export { default as TargetingGeoLocationElectoralDistrict } from 'objects/targeting-geo-location-electoral-district';
  export { default as TargetingGeoLocationGeoEntities } from 'objects/targeting-geo-location-geo-entities';
  export { default as TargetingGeoLocationLocationCluster } from 'objects/targeting-geo-location-location-cluster';
  export { default as TargetingGeoLocationLocationExpansion } from 'objects/targeting-geo-location-location-expansion';
  export { default as TargetingGeoLocationMarket } from 'objects/targeting-geo-location-market';
  export { default as TargetingGeoLocationPlace } from 'objects/targeting-geo-location-place';
  export { default as TargetingGeoLocationPoliticalDistrict } from 'objects/targeting-geo-location-political-district';
  export { default as TargetingGeoLocationRegion } from 'objects/targeting-geo-location-region';
  export { default as TargetingGeoLocationZip } from 'objects/targeting-geo-location-zip';
  export { default as TargetingProductAudienceSpec } from 'objects/targeting-product-audience-spec';
  export { default as TargetingProductAudienceSubSpec } from 'objects/targeting-product-audience-sub-spec';
  export { default as TargetingProspectingAudience } from 'objects/targeting-prospecting-audience';
  export { default as TargetingRelaxation } from 'objects/targeting-relaxation';
  export { default as TargetingSentenceLine } from 'objects/targeting-sentence-line';
  export { default as ThirdPartyMeasurementReportDataset } from 'objects/third-party-measurement-report-dataset';
  export { default as TrackingAndConversionWithDefaults } from 'objects/tracking-and-conversion-with-defaults';
  export { default as URL } from 'objects/url';
  export { default as UnifiedThread } from 'objects/unified-thread';
  export { default as User } from 'objects/user';
  export { default as UserCoverPhoto } from 'objects/user-cover-photo';
  export { default as UserDevice } from 'objects/user-device';
  export { default as UserIDForApp } from 'objects/user-id-for-app';
  export { default as UserIDForPage } from 'objects/user-id-for-page';
  export { default as UserLeadGenDisclaimerResponse } from 'objects/user-lead-gen-disclaimer-response';
  export { default as UserLeadGenFieldData } from 'objects/user-lead-gen-field-data';
  export { default as UserPaymentMobilePricepoints } from 'objects/user-payment-mobile-pricepoints';
  export { default as ValueBasedEligibleSource } from 'objects/value-based-eligible-source';
  export { default as Vehicle } from 'objects/vehicle';
  export { default as VehicleOffer } from 'objects/vehicle-offer';
  export { default as VideoCopyright } from 'objects/video-copyright';
  export { default as VideoCopyrightConditionGroup } from 'objects/video-copyright-condition-group';
  export { default as VideoCopyrightGeoGate } from 'objects/video-copyright-geo-gate';
  export { default as VideoCopyrightRule } from 'objects/video-copyright-rule';
  export { default as VideoCopyrightSegment } from 'objects/video-copyright-segment';
  export { default as VideoList } from 'objects/video-list';
  export { default as VideoPoll } from 'objects/video-poll';
  export { default as VideoThumbnail } from 'objects/video-thumbnail';
  export { default as VideoUploadLimits } from 'objects/video-upload-limits';
  export { default as VoipInfo } from 'objects/voip-info';
  export { default as WebAppLink } from 'objects/web-app-link';
  export { default as WhatsAppBusinessAccount } from 'objects/whats-app-business-account';
  export { default as WhatsAppBusinessProfile } from 'objects/whats-app-business-profile';
  export { default as WindowsAppLink } from 'objects/windows-app-link';
  export { default as WindowsPhoneAppLink } from 'objects/windows-phone-app-link';
  export { default as WorkUserFrontline } from 'objects/work-user-frontline';
}
