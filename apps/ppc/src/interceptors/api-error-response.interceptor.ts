import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  HttpException,
  Logger,
} from '@nestjs/common';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { FacebookError } from '../facebook/facebook.error';

@Injectable()
export class ApiErrorResponseInterceptor implements NestInterceptor {
  private readonly logger = new Logger('NestApplication');

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map((res) => {
        const error = res?.error ?? res?.data?.error ?? null;

        if (error != null) {
          this.logger.error('Events Error: ' + JSON.stringify(res));
          throw error;
        }

        return res;
      }),
      catchError((error) => {
        if (error instanceof FacebookError) {
          return throwError(new HttpException(error.message, error.status));
        }

        return throwError(error);
      }),
    );
  }
}
