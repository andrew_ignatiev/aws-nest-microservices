import * as serverless from 'serverless-http';
import {
  Context,
  Callback,
  Handler,
  APIGatewayProxyEventV2,
  APIGatewayProxyResultV2,
} from 'aws-lambda';
import { createApp } from './app.factory';

let cachedHandler: Handler<APIGatewayProxyEventV2, APIGatewayProxyResultV2>;

async function bootstrap(): Promise<Handler> {
  if (!cachedHandler) {
    const nestApp = await createApp();
    const httpApp = nestApp.getHttpAdapter().getInstance();

    nestApp.enableCors();
    await nestApp.init();

    cachedHandler = serverless(httpApp);
  }

  return cachedHandler;
}

export async function handler(
  event: APIGatewayProxyEventV2,
  context: Context,
  callback: Callback,
) {
  return (await bootstrap())(event, context, callback);
}
