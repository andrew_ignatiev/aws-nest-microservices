export interface ApiControllerInterface {
  data: Record<string, unknown> | Array<Record<string, unknown>>;
  success: true;
  statusCode: number;
}
